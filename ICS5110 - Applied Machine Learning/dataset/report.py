__author__ = 'Dorian Bugeja'
__date__ = '07/01/2016'

import sklearn
import numpy as np;
from sklearn.cross_validation import KFold;






def kfold_generate():
    import numpy as np
    import urllib
    dataset = np.loadtxt('cars/car_num_random.data', delimiter=",")

    # separate the data from the target attributes
    X = dataset[:,0:5]
    y = dataset[:,6]

    kf = KFold(X.shape[0], n_folds=2)
    train_index, test_index = kf._iter_test_indices();
    for train_index, test_index in kf:
        training_set = np.concatenate((X[train_index], np.vstack(y[train_index])), axis=1)
        test_set = np.concatenate((X[test_index], np.vstack(y[test_index])), axis=1)




a = np.array([[1, 2, 3], [3, 4, 4]])
b = np.array([[5, 6]])

print(a.shape)
print(b.T.shape)
#np.concatenate((a, b), axis=0)
np.concatenate((a, b.T), axis=1)
print(len(b.T))






kfold_generate()

"""

X = np.array([[1, 2], [3, 4], [1, 2], [3, 4]])
y = np.array([1, 2, 3, 4])
kf = KFold(4, n_folds=2)


print(kf)
sklearn.cross_validation.KFold(n=4, n_folds=2, shuffle=False,
                               random_state=None)
for train_index, test_index in kf:
   print("TRAIN:", train_index, "TEST:", test_index)
   X_train, X_test = X[train_index], X[test_index]
   y_train, y_test = y[train_index], y[test_index]
"""