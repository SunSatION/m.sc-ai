__author__ = 'Dorian Bugeja'
__date__ = '12/11/2015'

from enum import Enum
import pandas as pd
import numpy as np
import math
import random
import pickle
from sklearn.cross_validation import KFold;
from graphviz import Digraph
import os;

class node(object):
    def __init__(self):
        self.value = None
        self.children = []
        self.desisionAttribute = None;
        self.parent = None;

    def setLabel(self, lab):
        self.value = lab;

    def addChild(self, child):
        self.children.append(child)

    def setDecisionAttr(self, attr):
        self.desisionAttribute = attr

    def setParent(self, node):
        self.parent = node

class Element:
    entropy = 0.

    def __init__(self, df=None):
        self.dtFrame = df

yesClassArray = ["unacc", "acc", "good", "vgood"];

def print_full(x):
    pd.set_option('display.max_rows', len(x))
    print(x)
    pd.reset_option('display.max_rows')

attributeSet = [
    ['buying', 'maint', 'doors', 'persons', 'lugboot', 'safety'],
    ["vhigh", "high", "med", "low"],
    ["vhigh", "high", "med", "low"],
    ["2", "3", "4", "5more"],
    ["2", "4", "more"],
    ["small", "med", "big"],
    ["low", "med", "high"]
]

from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2


def feature_selection():
    for k in range (1,7):
        print(k)
        for test_no in range(1, 11):
            from itertools import compress
            read_csv = pd.read_csv('test_' + str(test_no) + '/training.txt', header=None)
            wholeDataSet =  read_csv.as_matrix();
            X = wholeDataSet[:,0:6]
            y = wholeDataSet[:,6:].ravel()
            trans = SelectKBest(chi2, k);
            trans = trans.fit(X, y)
            X = wholeDataSet[:,trans.get_support()]
            y = wholeDataSet[:,6:].ravel()
            np.savetxt('test_' + str(test_no) + '/training_' + str(k) + '.txt' , np.concatenate((X, np.vstack(y)), axis=1), delimiter=',', fmt=('%d,'*k + '%d'))

            read_csv = pd.read_csv('test_' + str(test_no) + '/testing.txt', header=None)
            wholeDataSet =  read_csv.as_matrix();
            X = wholeDataSet[:,trans.get_support()]
            y = wholeDataSet[:,6:].ravel()
            np.savetxt('test_' + str(test_no) + '/testing_' + str(k) + '.txt' , np.concatenate((X, np.vstack(y)), axis=1), delimiter=',', fmt=('%d,'*k + '%d'))
            print(list(compress(attributeSet[0][0:6], trans.get_support())))
from sklearn.feature_selection import VarianceThreshold

#feature_selection();

#exit()


"""
sel = VarianceThreshold(threshold=(.8 * (1 - .8)))
pd.set_option('display.max_columns', None)

trans = SelectKBest(chi2, k=3);
trans = trans.fit(X, y)
trans.transform(X)
print(trans.get_support())
exit();
"""

def ID3(input_training, target_column, root, yes_class):

    groupedTraining = input_training.groupby(target_column);

    if input_training[target_column].nunique() == 1:
        root.setLabel(( '+' + str(random.randrange(1, 1000))) if groupedTraining.head(1).reset_index().loc[0, target_column] == yes_class else ('-' + str(random.randrange(1, 1000))))
        return root;

    if input_training.shape[1] == 1:
        root.setLabel('Unknown')
        root.setDecisionAttr(groupedTraining.apply(lambda x: x[0].count()).sort_values(ascending=False).index[0])
        return root

    entropyCount = groupedTraining[target_column].count();
    currentEntropy = 0

    for i in range(0, entropyCount.size):
        currentEntropy += (entropyCount.iloc[i] / entropyCount.sum()) * math.log2(
            (entropyCount.iloc[i] / entropyCount.sum()))

    currentEntropy *= -1
    gains = []

    toIterate = np.delete(input_training.columns._data, -1, 0)
    for currentAttribute in toIterate:
        a = []
        for k in range(1, groupedTraining[currentAttribute].nunique().max() + 1):
            a.append(Element(pd.DataFrame(groupedTraining.apply(lambda x: x[x[currentAttribute] == k][currentAttribute].count()))))

        currentSubEntropy = 0;
        for j in range(len(a)):
            subEntropy = 0
            for i in range(0, a[j].dtFrame.size):
                if a[j].dtFrame.iloc[i][0] != 0:
                    subEntropy += (a[j].dtFrame.iloc[i] / a[j].dtFrame.sum()) * (
                    math.log2((a[j].dtFrame.iloc[i] / a[j].dtFrame.sum())))
            a[j].entropy = subEntropy * -1
            currentSubEntropy += ((a[j].dtFrame.sum() / entropyCount.sum()) * a[j].entropy).item()

        gains.append(((currentEntropy - currentSubEntropy), currentAttribute))

    gains = sorted(gains, key=lambda gainSort: gainSort[0], reverse=True)

    root.setLabel(attributeSet[0][gains[0][1]] + "_" + str(random.randrange(0, 1000)))

    for i in range(0, input_training[gains[0][1]].unique().shape[0]):
        newNode = node();
        newNode.setDecisionAttr(attributeSet[gains[0][1]+1][input_training[gains[0][1]].unique()[i].item() - 1])
        newNode.setParent(root)
        #dot.edge(newNode.parent.value, attributeSet[0][gains[0][1]] + str(random.randrange(0, 100)), newNode.desisionAttribute)

        root.addChild(newNode)

        inputTraining2 = input_training[input_training[gains[0][1]] == input_training[gains[0][1]].unique()[i].item()]
        classifier = gains[0][1];

        input_training2 = inputTraining2.drop(inputTraining2[[classifier]], axis=1)
        if input_training2.shape[0] == 0:
            return root
        else:
            leaf = ID3(input_training2, target_column, newNode, yes_class)
            leaf.parent = root;
            dot[yes_class].edge(newNode.parent.value, leaf.value, newNode.desisionAttribute)
    #dot.render('test-output/round-table.gv', view=True)
    return root

from multiprocessing import Pool

dot = [];

for i in range(30):
   dot.append(None)

def split_data_set():
    read_csv = pd.read_csv('cars/car_num_random.data', header=None)
    wholeDataSet =  read_csv.as_matrix();
    X = wholeDataSet[:,0:6]
    y = wholeDataSet[:,6:]
    kf = KFold(X.shape[0], n_folds=10)
    test = 1;
    for train_index, test_index in kf:
        training_set = np.concatenate((X[train_index], np.vstack(y[train_index])), axis=1)
        test_set = np.concatenate((X[test_index], np.vstack(y[test_index])), axis=1)
        os.mkdir('test_' + str(test));
        np.savetxt('test_' + str(test) + '/training.txt' , training_set, delimiter=',', fmt='%d,%d,%d,%d,%d,%d,%d')
        np.savetxt('test_' + str(test) + '/testing.txt' , test_set, delimiter=',', fmt='%d,%d,%d,%d,%d,%d,%d')
        test += 1;


def p_ID3(toCluster):
    test_fold_no = 6
    feature_no = 6
    inputTraining = pd.read_csv('test_' + str(test_fold_no) + '/training_' + str(feature_no) + '.txt', header=None)
    d = node();
    print("Cluster " + str(toCluster) + " started")
    dot[toCluster] = Digraph(comment='Decision Tree for '+ yesClassArray[toCluster - 1])
    tree = ID3(inputTraining, feature_no, d, toCluster)

    with open('test_' + str(test_fold_no) + '/desctree_'+ yesClassArray[toCluster - 1] + '_' + str(feature_no) + '_tree.pickle', 'wb') as f:
        pickle.dump(tree, f)
    print("Cluster: " + str(toCluster) + "done.");
    print(dot[toCluster].source)
    with open('test_' + str(test_fold_no) + '/desctree_'+ yesClassArray[toCluster - 1] + '_' + str(feature_no) + '_drawing.pickle', 'wb') as f:
        pickle.dump(dot[toCluster], f)


#p_ID3(1, 1, 5)
"""
p_ID3(1)
p_ID3(2)
p_ID3(3)
p_ID3(4)
"""

"""
if __name__ == '__main__':
    with Pool(4) as p:
        p.map(p_ID3, range(4, 0, -1))

"""


cache = []

#split_data_set()
#p_ID3(4)



def traverseDecisionTree(tree, currentInstance):
    if ((tree.value[0] == '+') or (tree.value[0] == '-')):
        return tree.value[0]
    else:
        attrToMatch = currentInstance[keys[tree.value[0:tree.value.find('_')]]]
        if len(tree.children) == 0:
            return tree.value
        else:
            for i in range(0, len(tree.children)):
                if (tree.children[i].desisionAttribute == attributeSet[keys[tree.value[0:tree.value.find('_')]] + 1][currentInstance[keys[tree.value[0:tree.value.find('_')]]] - 1]):
                    return traverseDecisionTree(tree.children[i], currentInstance)


keys = {
    "buying": 0, "maint":1, "doors":2, "persons":3, "lugboot":4, "safety":5
}




from sklearn.metrics import *



from scipy import interp
import matplotlib.pyplot as plt

mean_tpr = 0.0
mean_fpr = np.linspace(0, 1, 100)
all_tpr = []

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 8}

test_fold_no = 10
feature_no = 6

for test_no in range(test_fold_no, test_fold_no+1):
    correct = []
    incorrect = []
    unclassified = [];

    y_pred = [];
    y_true = [];
    for i in range(1,5):
        y_true.append([])
        y_pred.append([])
        correct.append(0)
        incorrect.append(0)
        unclassified.append(0)

    with open('test_' + str(test_no) + '/testing.txt', 'r') as testFile:


        for index, yesClass in enumerate(yesClassArray):
            with open('test_' + str(test_no) + '/desctree_'+ yesClassArray[index] + '_' + str(feature_no) + '_tree.pickle', 'rb') as f:
                cache.append(pickle.load(f))

        for line in testFile:
            currentInstance = list(map(int, line.rstrip().split(',')))
            #currentInstance[-1] -= 1;

            for index, yesClass in enumerate(yesClassArray):
                tree = cache[index]
                decisionResult = traverseDecisionTree(tree, currentInstance );
                #print(str(decisionResult) + ' ' + str(index) + ' ' + str(currentInstance[6]))
                if ( decisionResult  == '+' ) and ( currentInstance[6] == 1 + index):
                    y_pred[currentInstance[6]-1].append(1)
                    y_true[currentInstance[6]-1].append(1)
                    correct[currentInstance[6]-1] += 1
                    continue
                    #print('correct')
                    #break

                if ( decisionResult  == '+' ) and ( currentInstance[6] != 1 + index):
                    #print(currentInstance)
                    y_pred[currentInstance[6]-1].append(1)
                    y_true[currentInstance[6]-1].append(0)
                    #incorrect[currentInstance[6]-1] += 1
                    continue
                    #print('incorrect')
                    #break

                if ( decisionResult  == '-' ) and ( currentInstance[6] == 1 + index):
                    incorrect[currentInstance[6]-1] += 1
                    y_pred[currentInstance[6]-1].append(0)
                    y_true[currentInstance[6]-1].append(1)
                    continue
                    #print('incorrect')
                    #break

                if ( decisionResult  == '-' ) and ( currentInstance[6] != 1 + index):
                    y_pred[currentInstance[6]-1].append(0)
                    y_true[currentInstance[6]-1].append(0)
                    #correct[currentInstance[6]-1] += 1
                    continue
                    #print('correct')
                    #break

                if decisionResult == None and ( currentInstance[6] == 1 + index):
                    y_pred[currentInstance[6]-1].append(0)
                    y_true[currentInstance[6]-1].append(1)
                    incorrect[currentInstance[6]-1] += 1
                    unclassified[currentInstance[6]-1] += 1
                    continue
                    #print('incorrect')
                    #break

                if decisionResult == None and ( currentInstance[6] != 1 + index):
                    y_pred[currentInstance[6]-1].append(0)
                    y_true[currentInstance[6]-1].append(0)
                    #correct[currentInstance[6]-1] += 1
                    unclassified[currentInstance[6]-1] += 1
                    continue
                    #print('correct')
                    #break
        print('\hline')
        print("Fold " + str(test_no) + " & Unacc & Acc & Good & VGood & Total \\\\")
        print('\hline')
        print('Correct & ' +  ' & '.join(map(str, correct)) + ' & ' + str(np.sum(correct)) + ' \\\\')
        print('Incorrect & ' +  ' & '.join(map(str, incorrect)) + ' & ' + str(np.sum(incorrect)) + ' \\\\')
        #print(np.sum(correct))
        #print(np.sum(incorrect))
        #print(unclassified)
                    #print(currentInstance)

        print("Fold 1 & Precision & Recall & F1 Score & Accuracy & MSE & AUC \\\\ ")
        print("\\hline")

    for index, yesClass in enumerate(yesClassArray):
        mean_tpr = 0
        print(yesClass + ' & ' + \
              str('{0:.2f}'.format(precision_score(y_true[index], y_pred[index]))) + ' & ' + str('{0:.2f}'.format(recall_score(y_true[index], y_pred[index]))) + ' & ' + \
              str('{0:.2f}'.format(f1_score(y_true[index], y_pred[index]))) + ' & ' + str('{0:.2f}'.format(accuracy_score(y_true[index], y_pred[index]))) + ' & ' + \
                str('{0:.2f}'.format(mean_squared_error(y_true[index], y_pred[index]))) + ' & ' + str('{0:.2f}'.format(roc_auc_score(y_true[index], y_pred[index]))) + ' \\\\ ')
        fpr, tpr, thresholds = roc_curve(y_true[index], y_pred[index], 1)
        mean_tpr += interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0.0
        roc_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, lw=1, label='ROC fold %d class %s (area = %0.2f)' % (test_no, yesClass, roc_auc))
    print('\\\\')


    plt.rc('font', **font)

    plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Random')

    mean_tpr /= 10
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    #plt.plot(mean_fpr, mean_tpr, 'k--',
    #         label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Area over 10 K Folds')
    plt.legend(loc="lower right")
    plt.savefig('test_' + str(test_no) + '/roc.png')



"""

        print('Test Fold ' + str(test_no) + ' Class ' + yesClass)
        print(classification_report(y_true[index], y_pred[index]))
        print('Mean Absolute Error: ' + str(mean_absolute_error(y_true[index], y_pred[index])))
        print('Mean Squared Error: ' + str(mean_squared_error(y_true[index], y_pred[index])))
        print('F1 Score: ' + str(f1_score(y_true[index], y_pred[index])))
        print('Accuracy Score: ' + str(accuracy_score(y_true[index], y_pred[index])))
        print('Precision Score: ' + str(precision_score(y_true[index], y_pred[index])))
        print('Recall Score: ' + str(recall_score(y_true[index], y_pred[index])))
        print('ROC AUC Score: ' + str(roc_auc_score(y_true[index], y_pred[index])))
        print('-----------------')

"""



"""
print(y_pred)
print(y_true)

print(correct)
print(incorrect)
print(unclassified)
"""

    #print(precision, recall, thresholds)





"""
    tree = ID3(inputTraining, 6, -1, d, toCluster)
    with open('Decision Tree v2 for '+ yesClassArray[toCluster - 1] + '_tree.pickle', 'wb') as f:
        pickle.dump(tree, f)
    print("Cluster: " + str(toCluster) + "done.");
    print(dot[toCluster].source)
    #dot[toCluster].render('test-output/round-table' + str(toCluster) + '.gv', view=True)
    with open('Decision Tree v2 for '+ yesClassArray[toCluster - 1] + '_drawing.pickle', 'wb') as f:
        pickle.dump(dot[toCluster], f)
"""








#with open('Decision Tree v2 for acc_tree.pickle', 'rb') as f:
#    tree = pickle.load(f)
#    print(tree)



def updateEdges(root):
    """
    @type param: node
    """
    for i in range(0, len(root.children)):
        dot.edge(root.value, root.children[i].value, root.children[i].value)


#updateEdges(d);


#print(dot.source)





# for i in range()
# uniqueValues = input_training[gains[0][1]].unique();

"""
        updateEdges(root.children[i])
    print()

groupedTraining = inputTraining.groupby(target_column);
entropyCount = groupedTraining[0].count();

currentEntropy = 0

for i in range(0,entropyCount.size):
    currentEntropy += ( entropyCount.iloc[i] / entropyCount.sum() ) * math.log2(( entropyCount.iloc[i] / entropyCount.sum() ))

currentEntropy *= -1

total = inputTraining.size
gains = []
attributeCount = 4
for currentAttribute in range(attributeCount):
    a = []
    for k in range(1, groupedTraining[currentAttribute].nunique().max() + 1):
        a.append(Element(pd.DataFrame(groupedTraining.apply(lambda x: x[x[currentAttribute] == k][0].count()))))
    subEntropy = 0
    currentSubEntropy = 0;
    for j in range(len(a)):
        subEntropy = 0
        for i in range(0,a[j].dtFrame.size):
            if a[j].dtFrame.iloc[i][0] != 0:
                subEntropy += ( a[j].dtFrame.iloc[i] / a[j].dtFrame.sum() ) * ( math.log2(( a[j].dtFrame.iloc[i] / a[j].dtFrame.sum() )))
        a[j].entropy = subEntropy*-1
        currentSubEntropy += ((a[j].dtFrame.sum() / entropyCount.sum()) * a[j].entropy).item()

    gains.append(((currentEntropy - currentSubEntropy), currentAttribute))
gains = sorted(gains, key=lambda gainSort: gainSort[0], reverse=True)


class Ord(Enum):
    Hearts = 1
    Spades = 2
    Diamonds = 3
    Clubs = 4

class PokerClass(Enum):
    Nothinghand = 0
    Onepair = 1
    Twopairs = 2
    Threekind = 3
    Straight = 4
    Flush = 5
    Fullhouse = 6
    Fourkind = 7
    Straightflush = 8
    Royalflush = 9
"""

