import numpy
import matplotlib.pyplot as plt
import scipy
import pickle

from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.tests.helpers import gradientCheck
from pybrain.datasets import ClassificationDataSet
from pybrain.utilities import percentError

# Load training and test data
# x, y, color = numpy.loadtxt(open("DataSet.txt","rb"),delimiter=",",skiprows=3, usecols=(0,1,2), unpack=True)
training_x, training_y, training_color = numpy.loadtxt(open("LearningSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)
test_x, test_y, test_color = numpy.loadtxt(open("TestSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)

for middle_layer_count in range (2, 8):
    print(str(middle_layer_count), ' neurons in hidden layer')


    net = buildNetwork(3, middle_layer_count, 1, bias=True)
    print(net.params)
    ds = SupervisedDataSet(3, 1)
    for x, y, classifier in zip(training_x, training_y, training_color):
        ds.addSample((1, x, y), (classifier))

    ts = SupervisedDataSet(3, 1)
    for x, y, classifier in zip(test_x, test_y, test_color):
        ts.addSample((1, x, y), (classifier))

    trainer = BackpropTrainer(net, ds, learningrate=0.0005)
    model = trainer.trainOnDataset(ds, 3000)

    for mod in net.modules:
        print("Module:", mod.name)
        if mod.paramdim > 0:
            print("--parameters:", mod.params)
        for conn in net.connections[mod]:
            print("-connection to", conn.outmod.name)
            if conn.paramdim > 0:
                 print("- parameters", conn.params)
        if hasattr(net, "recurrentConns"):
            print("Recurrent connections")
            for conn in net.recurrentConns:
                print("-", conn.inmod.name, " to", conn.outmod.name)
                if conn.paramdim > 0:
                    print("- parameters", conn.params)

    output = open('new_neural' + str(middle_layer_count), 'wb')
    pickle.dump(trainer, output)
    pickle.dump(net, output)
    pickle.dump(model, output)

    results = net.activateOnDataset(ts)
    correct = 0;
    incorrect = 0
    for i in range(0, len(results)):
        if ( results[i] > 0.5 and test_color[i] == 1 ) or ( results[i] < 0.5 and test_color[i] == 0 ):
            correct += 1
        else:
            incorrect += 1
    print(correct, incorrect)
    print("================================")

    """
    model = trainer.trainUntilConvergence(verbose=True, continueEpochs=10, validationProportion=0.8)
    output = open('neural' + str(middle_layer_count), 'wb')
    pickle.dump(trainer, output)
    pickle.dump(net, output)
    pickle.dump(model, output)
    """
    """
    input = open('neural' + str(middle_layer_count), 'rb')
    trainer  = pickle.load(input)
    net = pickle.load(input)
    model = pickle.load(input)
    cds = SupervisedDataSet(2,1)
    for x, y, classifier in zip(test_x, test_y, test_color):
        cds.addSample([x, y], [classifier])
    results = net.activateOnDataset(cds)
    correct = 0;
    incorrect = 0
    for i in range(0, len(results)):
        if ( results[i] > 0.8 and test_color[i] == 1 ) or ( results[i] < 0.8 and test_color[i] == 0 ):
            correct += 1
        else:
            incorrect += 1

    print(correct, incorrect)
"""
exit()


"""
[[  11.70730043]
 [ -73.22195288]
 [ 129.32965275]
 [ -78.86035758]
 [  -8.6212644 ]
 [  51.70274222]
 [ -38.08765697]]

"""




training_x, training_y, training_color = numpy.loadtxt(open("LearningSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)
test_x, test_y, test_color = numpy.loadtxt(open("TestSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)
plt.clf()

plt.title("X1 X2 Data points")

plt.scatter(training_x, training_y, marker='o', s=50, alpha=1, c=training_color, cmap='bwr')
plt.scatter(test_x, test_y, marker='^', s=50, alpha=1, c=test_color, cmap='cool', label="Test Set")
plt.legend(handles=[plt.scatter([],[], marker='o', s=50, alpha=1, color='Red', cmap='bwr', label="Positive Training Set"), plt.scatter([],[], marker='o', s=50, alpha=1, color='Blue', cmap='bwr', label="Negative Training Set"),
                    plt.scatter([],[], marker='^', s=50, alpha=1, color='Purple', cmap='bwr', label="Positive Test Set"), plt.scatter([],[], marker='^', s=50, alpha=1, color='Cyan', cmap='bwr', label="Negative Test Set"),
                    ], scatterpoints=1, numpoints=1)
plt.ylabel('X2')
plt.xlabel('X1')

plt.axis([0,7,0,8])
plt.grid(True)

plt.show()

exit()


# Load training and test data
# x, y, color = numpy.loadtxt(open("DataSet.txt","rb"),delimiter=",",skiprows=3, usecols=(0,1,2), unpack=True)
training_x, training_y, training_color = numpy.loadtxt(open("LearningSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)
test_x, test_y, test_color = numpy.loadtxt(open("TestSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)

# Normalise
ZmaxX, ZminX = training_x.max(), training_x.min()
#training_x = (training_x - ZminX) / (ZmaxX - ZminX)
#test_x = (test_x - ZminX) / (ZmaxX - ZminX)
ZmaxY, ZminY = training_y.max(), training_y.min()
#training_y = (training_y - ZminY) / (ZmaxY - ZminY)
#test_y = (test_y - ZminY) / (ZmaxY - ZminY)

# Converged values
d =   [[  12.16824823], # C
       [ -75.46325109], # x1
       [ 132.04780487], # x1^2
       [ -77.87849293], # x1^3
       [  -7.35769238], # y1
       [  46.86088348], # y1^2
       [ -35.01832708], # y1^3
       ]
d = [[  10.68948617],
 [ -66.38841309],
 [ 116.17134177],
 [ -71.07663178],
 [  -8.8928571 ],
 [  52.2009556 ],
 [ -38.42581172]]

# Count incorrect classification
incorrect_classified = 0
for x, y, classifier in zip(test_x, test_y, test_color):

    #y_graph = -(d[0][0] / ( (ZmaxX - ZminX )  ) +  d[1][0]*x  / (ZmaxX - ZminX) +  d[2][0]*(x**2) / (ZmaxX - ZminX) + d[3][0]*(x**3) / (ZmaxX - ZminX) +  d[4][0]*x / (ZmaxY - ZminY) +  d[5][0]*(x**2) /(ZmaxY - ZminY) + d[6][0]*(x**3) / (ZmaxY - ZminY))
    y_graph = -(d[0][0] / ( (ZmaxX - ZminX )  ) +  d[1][0]*x  / (ZmaxX - ZminX) +  d[2][0]*(x**2) / (ZmaxX - ZminX) + d[3][0]*(x**3) / (ZmaxX - ZminX) +  d[4][0]*x / (ZmaxY - ZminY) +  d[5][0]*(x**2) /(ZmaxY - ZminY) + d[6][0]*(x**3) / (ZmaxY - ZminY))
    print(y_graph)
    if (int(y > y_graph) - classifier) != 0:
        incorrect_classified += 1
print("Number of incorrectly classified items: " + str(incorrect_classified))

# Plot decision boundary
x = numpy.linspace(0, 1,300)
y = -(d[0] / ( (ZmaxX - ZminX )  ) +  d[1]*x  / (ZmaxX - ZminX) +  d[2]*(x**2) / (ZmaxX - ZminX) + d[3]*(x**3) / (ZmaxX - ZminX) +  d[4]*x / (ZmaxY - ZminY) +  d[5]*(x**2) /(ZmaxY - ZminY) + d[6]*(x**3) / (ZmaxY - ZminY))

x = ((x * (ZmaxX - ZminX)) + ZminX)
y = ((y * (ZmaxY - ZminY)) + ZminY)

# Plot Dots
plt.scatter(training_x, training_y, marker='^', s=50, alpha=1, c=training_color, cmap='bwr')
plt.scatter(test_x, test_y, marker='o', s=50, alpha=1, c=test_color, cmap='cool')
plt.plot(x,y)
plt.show()
