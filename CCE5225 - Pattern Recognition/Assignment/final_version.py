import numpy
import matplotlib.pyplot as plt
import matplotlib.text as mlines
import math;

def sigmoid(x):
    z = 1.0 / (1.0 + numpy.exp((-1) * x))
    return z


# Shuffle data set and save training and testint set
"""
x, y, color = numpy.loadtxt(open("DataSet.txt","rb"),delimiter=",",skiprows=3, usecols=(0,1,2), unpack=True)
plt.scatter(x, y, marker='o', s=50, alpha=1, c=color, cmap='cool')
plt.show()
numpy.random.seed(17)
numpy.random.shuffle(x)
numpy.random.seed(17)
numpy.random.shuffle(y)
numpy.random.seed(17)
numpy.random.shuffle(color)
numpy.savetxt(open("LearningSet.txt", "wb"), numpy.column_stack((x[:(int)(len(x) * 0.8)], y[:(int)(len(x) * 0.8)], color[:(int)(len(x) * 0.8)])), fmt='%s', delimiter=",", newline="\r\n")
numpy.savetxt(open("TestSet.txt", "wb"), numpy.column_stack((x[(int)(len(x) * 0.8):], y[(int)(len(x) * 0.8):], color[(int)(len(x) * 0.8):])), fmt='%s', delimiter=",", newline="\r\n")
"""

a=16.7656
b=-252.92
c=1269.89
d=-2118.19

#Load split data set
training_x, training_y, training_color = numpy.loadtxt(open("LearningSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)
test_x, test_y, test_color = numpy.loadtxt(open("TestSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)

#training_x, training_y, color = numpy.loadtxt(open("DataSet.txt","rb"),delimiter=",",skiprows=3, usecols=(0,1,2), unpack=True)

# Normalise
ZmaxX, ZminX = training_x.max(), training_x.min()
training_x_norm = (training_x - ZminX) / (ZmaxX - ZminX)
test_x_norm = (test_x - ZminX) / (ZmaxX - ZminX)
ZmaxY, ZminY = training_y.max(), training_y.min()
training_y_norm = (training_y - ZminY) / (ZmaxY - ZminY)
test_y_norm = (test_y - ZminY) / (ZmaxY - ZminY)
ZmaxTX = ZmaxX
ZminTX = ZminX
ZmaxTY = ZmaxY
ZminTY = ZminY

incorrect_classified = 0
rms = 0;
for x, y, classifier in zip(test_x, test_y, test_color):
    y_graph = a * (x ** 3)  + b * (x ** 2) + c * (x) + d
    if (int(y > y_graph) - classifier) != 0:
        print(y - y_graph)
        rms += (y - y_graph)**2
        incorrect_classified += 1
print("Number of incorrectly classified items: " + str(incorrect_classified) +  " from " + str(len(test_x)) )
print("Root mean squared value: " + str(math.sqrt(rms / (incorrect_classified* (ZmaxY - ZminY) ))))


# Plotting time
plt.title("X1 X2 Manual Fitting")
plt.legend(handles=[plt.scatter([],[], marker='o', s=50, alpha=1, color='Red', cmap='bwr', label="Positive Data Set"), plt.scatter([],[], marker='o', s=50, alpha=1, color='Blue', cmap='bwr', label="Negative Data Set")
                    ], scatterpoints=1, numpoints=1)
plt.ylabel('X2')
plt.xlabel('X1')

x = numpy.linspace(4, 6,600)
y = a * (x**3) + b * (x**2) + c * (x) + d
plt.scatter(training_x, training_y, marker='o', s=50, alpha=1, c=training_color, cmap='bwr')
plt.scatter(test_x, test_y, marker='o', s=50, alpha=1, c=test_color, cmap='bwr')
plt.plot(x,y);
plt.grid(True)
plt.show()

incorrect_classified = 0;

# Load training and test data ( remove regularisation )
# x, y, color = numpy.loadtxt(open("DataSet.txt","rb"),delimiter=",",skiprows=3, usecols=(0,1,2), unpack=True)
training_x, training_y, training_color = numpy.loadtxt(open("LearningSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)
test_x, test_y, test_color = numpy.loadtxt(open("TestSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)
plt.clf()

plt.title("X1 X2 Data points")

plt.scatter(training_x, training_y, marker='o', s=50, alpha=1, c=training_color, cmap='bwr')
plt.scatter(test_x, test_y, marker='^', s=50, alpha=1, c=test_color, cmap='cool', label="Test Set")
plt.legend(handles=[plt.scatter([],[], marker='o', s=50, alpha=1, color='Red', cmap='bwr', label="Positive Training Set"), plt.scatter([],[], marker='o', s=50, alpha=1, color='Blue', cmap='bwr', label="Negative Training Set"),
                    plt.scatter([],[], marker='^', s=50, alpha=1, color='Purple', cmap='bwr', label="Positive Test Set"), plt.scatter([],[], marker='^', s=50, alpha=1, color='Cyan', cmap='bwr', label="Negative Test Set"),
                    ], scatterpoints=1, numpoints=1)
plt.ylabel('X2')
plt.xlabel('X1')

x = numpy.linspace(4, 6,600)
y = a * (x**3) + b * (x**2) + c * (x) + d

plt.plot(x,y);
plt.grid(True)

plt.show()

# Normalise
ZmaxX, ZminX = training_x.max(), training_x.min()
training_x_norm = (training_x - ZminX) / (ZmaxX - ZminX)
test_x_norm = (test_x - ZminX) / (ZmaxX - ZminX)
ZmaxY, ZminY = training_y.max(), training_y.min()
training_y_norm = (training_y - ZminY) / (ZmaxY - ZminY)
test_y_norm = (test_y - ZminY) / (ZmaxY - ZminY)
ZmaxTX = ZmaxX
ZminTX = ZminX
ZmaxTY = ZmaxY
ZminTY = ZminY

mo_input = []
for i in range(0,training_x_norm.shape[0]):
    mo_input.append(numpy.array([1, training_x_norm[i], training_x_norm[i]**2, training_x_norm[i]**3, training_y_norm[i], training_y_norm[i]**2, training_y_norm[i]**3]))

mo_input = numpy.array(mo_input)
#mo_input = numpy.vstack(mo_input)
theta = numpy.random.uniform(-10, 10, size=(mo_input.shape[1],1))

# seed = 17, solution values
theta = numpy.array(
[[  01.70730043],
 [ -73.22195288],
 [ 129.32965275],
 [ -78.86035758],
 [  -8.6212644 ],
 [  51.70274222],
 [ -38.08765697]])

current_mo = mo_input
converge_array = []
iter = 0

while True:
    dot_product = numpy.dot(current_mo, theta)
    l_sigmoid_value = []
    for i in dot_product:
        l_sigmoid_value.append(sigmoid(i[0]))
    l_cost_value = []
    for i in range(0, training_color.shape[0]):
        l_cost_value.append( (-training_color[i] * numpy.log10(l_sigmoid_value[i])) - ((1-training_color[i]) * numpy.log10(1 - l_sigmoid_value[i])))

    iter+=1
    converge_array.append(numpy.sum(l_cost_value))

    if (numpy.mean(converge_array[-9:])-numpy.sum(l_cost_value) < 0.0000001 ) and len(converge_array) > 10:
        print("Iteration: " + str(iter))
        print((numpy.mean(converge_array[-9:])) - numpy.sum(l_cost_value))
        print(numpy.sum(l_cost_value))
        print(theta)
        d = theta;
        plt.title('Convergence rate')
        plt.ylabel("Cost Function")
        plt.xlabel("Iterations")
        plt.plot(converge_array, label='Slope')
        plt.show()
        break

    if iter % 4000 == 0:
        print("Iteration: " + str(iter))
        print((numpy.mean(converge_array[-9:])) - numpy.sum(l_cost_value))
        print(numpy.sum(l_cost_value))
        print(theta)

    sum_derivative_cost = 0;
    sigm_minus_required = []

    for j in range(0, theta.shape[0]): # Calculate new difference on sum
        sum_derivative_cost = 0
        for i in range(0, training_color.shape[0]):
            part1 = (l_sigmoid_value[i] - training_color[i]) * current_mo[i][j]
            sum_derivative_cost  = sum_derivative_cost + part1
        sigm_minus_required.append(sum_derivative_cost * 0.005)

    theta = theta - numpy.vstack(numpy.array(sigm_minus_required)); # Update Weights with difference

# Count incorrect classification
rms = 0;
incorrect_classified = 0
for x, y, classifier in zip(test_x_norm, test_y_norm, test_color):
    y_graph = - 1 * ( (( theta[0][0] + theta[1][0]*(x-0)  +  theta[2][0]*((x-0)**2) + theta[3][0]*((x-0)**3)) / (ZmaxX - ZminX)) +  ( ( theta[4][0]*(x-0) +  theta[5][0]*((x-0)**2) + theta[6][0]*((x-0)**3)) / (ZmaxY - ZminY))  )

    if (int(y > y_graph) - classifier) != 0:
        rms += (y - y_graph)**2
        print(y - y_graph)
        incorrect_classified += 1
print("Number of incorrectly classified items: " + str(incorrect_classified) +  " from " + str(len(test_x)) )
print("Root mean squared value: " + str(math.sqrt(rms / incorrect_classified)))


# Plot decision boundary
x = numpy.linspace(0, 1,600)
y = -((theta[0] + ((x**1) * theta[1]) + ((x**2) * theta[2]) + ((x**3) * theta[3])) / (ZmaxX - ZminX) + (((x**1) * theta[4]) + ((x**2) * theta[5]) + ((x**3) * theta[6]))/(ZmaxY - ZminY)  )
print (ZmaxX - ZminX)
print (ZmaxY - ZminY)

# Denormalise graph
x = ((x * (ZmaxX - ZminX)) + ZminX)
y = ((y * (ZmaxY - ZminY)) + ZminY)

# Plot Dots
plt.scatter(training_x, training_y, marker='o', s=50, alpha=1, c=training_color, cmap='bwr')
plt.scatter(test_x, test_y, marker='^', s=50, alpha=1, c=test_color, cmap='cool')
log_line = plt.plot(x,y, label="Class Seperation Logistic Regression")
plt.ylabel('X2')
plt.xlabel('X1')

plt.legend(handles=[plt.scatter([],[], marker='o', s=50, alpha=1, color='Red', cmap='bwr', label="Positive Training Set"), plt.scatter([],[], marker='o', s=50, alpha=1, color='Blue', cmap='bwr', label="Negative Training Set"),
                    plt.scatter([],[], marker='^', s=50, alpha=1, color='Purple', cmap='bwr', label="Positive Test Set"), plt.scatter([],[], marker='^', s=50, alpha=1, color='Cyan', cmap='bwr', label="Negative Test Set"), mlines.Line2D([], [], color='blue',
                          markersize=15, label='Class Seperation Logistic Regression')
                    ], scatterpoints=1, numpoints=1)

plt.show()
