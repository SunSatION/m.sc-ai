import numpy
import matplotlib.pyplot as plt

#x, y, color = numpy.loadtxt(open("DataSet.txt","rb"),delimiter=",",skiprows=3, usecols=(0,1,2), unpack=True)
#numpy.random.seed(17)
#numpy.random.shuffle(x)
#numpy.random.shuffle(y)
#numpy.random.shuffle(color)
#numpy.savetxt(open("LearningSet.txt", "wb"), numpy.column_stack((x[:(int)(len(x) * 0.8)], y[:(int)(len(x) * 0.8)], color[:(int)(len(x) * 0.8)])), fmt='%s', delimiter=",", newline="\r\n")
#numpy.savetxt(open("TestSet.txt", "wb"), numpy.column_stack((x[(int)(len(x) * 0.8):], y[(int)(len(x) * 0.8):], color[(int)(len(x) * 0.8):])), fmt='%s', delimiter=",", newline="\r\n")

training_x, training_y, training_color = numpy.loadtxt(open("LearningSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)

d = [[0],[0],[0],[0],[0],[0],[0]]

#d =   [[  2.58760377],
# [-17.72451214],
# [ 20.79813549],
# [-12.88190899],
# [  2.81385461],
# [ 20.36670398],
# [-17.15333568]]


x = numpy.linspace(0, 1,300)
y =  d[0] + d[1]*x +  d[2]*(x**2) + d[3]*(x**3) +  d[4]*x +  d[5]*(x**2) + d[6]*(x**3)

iter = 0
plt.scatter(training_x, training_y, marker='^', s=50, alpha=1, c=training_color, cmap='rainbow_r')

ZmaxTX, ZminTX = training_x.max(), training_x.min()
training_x = (training_x - ZminTX) / (ZmaxTX - ZminTX)
ZmaxTY, ZminTY = training_y.max(), training_y.min()
training_y = (training_y - ZminTY) / (ZmaxTY - ZminTY)

def sigmoid(x):
    z = 1.0 / (1.0 + numpy.exp((-1) * x))
    return z

numpy.random.seed(20)

mo_input = []
for i in range(0,training_x.shape[0]):
    mo_input.append(numpy.array([1, training_x[i], training_x[i]**2, training_x[i]**3, training_y[i], training_y[i]**2, training_y[i]**3]))

mo_input = numpy.vstack(mo_input)
theta = numpy.random.uniform(0, 1, size=(mo_input.shape[1],1))
current_mo = numpy.vstack(mo_input)
converge_array = []

while True:
    dot_product = numpy.dot(current_mo, theta)
    l_sigmoid_value = []
    for i in dot_product:
        l_sigmoid_value.append(sigmoid(i[0]))
    l_cost_value = []
    for i in range(0, training_color.shape[0]):
        l_cost_value.append( (-training_color[i] * numpy.log(l_sigmoid_value[i])) - ((1-training_color[i]) * numpy.log(1 - l_sigmoid_value[i])))

    iter+=1
    converge_array.append(numpy.sum(l_cost_value))

    if (numpy.mean(converge_array[-9:])-numpy.sum(l_cost_value) < 0.0) and len(converge_array) > 10:
        print((numpy.mean(converge_array[-9:])) - numpy.sum(l_cost_value))
        print(numpy.sum(l_cost_value))
        print(theta)
        d = theta;
        #plt.plot(converge_array)
        #plt.show()
        break

    if iter % 4000 == 0:
        print((numpy.mean(converge_array[-9:])) - numpy.sum(l_cost_value))
        print(numpy.sum(l_cost_value))
        print(theta)

    sum_derivative_cost = 0;
    sigm_minus_required = []

    for j in range(0, theta.shape[0]):
        sum_derivative_cost = 0
        for i in range(0, training_color.shape[0]):
            part1 = (l_sigmoid_value[i] - training_color[i]) * current_mo[i][j]
            sum_derivative_cost  = sum_derivative_cost + part1
        sigm_minus_required.append(sum_derivative_cost * 0.005)

    theta = theta - numpy.vstack(numpy.array(sigm_minus_required));

#################
# Testing Section
#################

test_x, test_y, test_color = numpy.loadtxt(open("TestSet.txt","rb"),delimiter=",", usecols=(0,1,2), unpack=True)

# Normalise
ZmaxX, ZminX = training_x.max(), training_x.min()
training_x = (training_x - ZminX) / (ZmaxX - ZminX)
test_x = (test_x - ZminX) / (ZmaxX - ZminX)
ZmaxY, ZminY = training_y.max(), training_y.min()
training_y = (training_y - ZminY) / (ZmaxY - ZminY)
test_y = (test_y - ZminY) / (ZmaxY - ZminY)

# Count incorrect classification
incorrect_classified = 0
for x, y, classifier in zip(test_x, test_y, test_color):
    y_graph = -(d[0][0] / ( (ZmaxX - ZminX )  ) +  d[1][0]*x  / (ZmaxX - ZminX) +  d[2][0]*(x**2) / (ZmaxX - ZminX) + d[3][0]*(x**3) / (ZmaxX - ZminX) +  d[4][0]*x / (ZmaxY - ZminY) +  d[5][0]*(x**2) /(ZmaxY - ZminY) + d[6][0]*(x**3) / (ZmaxY - ZminY))
    if (int(y > y_graph) - classifier) != 0:
        incorrect_classified += 1
print("Number of incorrectly classified items: " + str(incorrect_classified))

# Plot decision boundary
x = numpy.linspace(0, ZminTX + ( 1 * (ZmaxTX - ZminTX) ), 300)
y = -(d[0] / ( (ZmaxX - ZminX )  ) +  d[1]*x  / (ZmaxX - ZminX) +  d[2]*(x**2) / (ZmaxX - ZminX) + d[3]*(x**3) / (ZmaxX - ZminX) +  d[4]*x / (ZmaxY - ZminY) +  d[5]*(x**2) /(ZmaxY - ZminY) + d[6]*(x**3) / (ZmaxY - ZminY))


# Plot Dots
#plt.scatter(training_x, training_y, marker='^', s=50, alpha=1, c=training_color, cmap='bwr')
plt.scatter(test_x, test_y, marker='o', s=50, alpha=1, c=test_color, cmap='cool')
plt.plot(x,y)
plt.show()

