import numpy

def computeError(c, m, points):
    totalerror = 0;
    for i in range(0, len(points)):
        totalerror += (points[i][1] - ((m * points[i][0] + c)))**2
    return float(totalerror / float(len(points)))

def stepGradient(w0, w1, points, learnrate):
    w1_new = 0
    w0_new = 0
    n = float(len(points))
    for i in range(0, len(points)):
        w0_new += -(2/n) * (points[i][1] - ((w1 * points[i][0]) + w0))
        w1_new += -(2/n) * points[i][0] * ( points[i][1] - ((w1 * points[i][0]) + w0))
    w1_ret = w1 - (learnrate * w1_new)
    w0_ret = w0 - (learnrate * w0_new)

    return [w0_ret, w1_ret]

X = numpy.matrix([[1, 2], [1, 3], [1,6], [1,10], [1,13], [1,17], [1,18], [1,25]])
Y = numpy.transpose(numpy.matrix([0.5, 0.9, 0.7, 1.3, 1.9, 1.4, 2.3, 2.2]));
Xt = numpy.transpose(X);
theta = numpy.linalg.inv(Xt * X) * Xt * Y
print(theta)

points = [(2, 0.5), (3, 0.9), (6, 0.7), (10, 1.3), (17, 1.4), (18, 2.3), (25, 2.2)]


import matplotlib.pyplot as plt
converge = [];

newLine = [-1, -1]
near = 0
err = 100;

while near < 40 :
    oldErr = err;
    err = (computeError(newLine[0], newLine[1], points))
    newLine = stepGradient(newLine[0], newLine[1], points, 0.0005)
    converge.append(err);


    if float(abs(oldErr - err)) < 0.000000000000000001:
        near += 1
    else:
        near = 0

print(newLine)

plt.plot([2, 3, 6, 10, 17, 18, 25], [0.5, 0.9, 0.7, 1.3, 1.4, 2.3, 2.2 ], 'ro')
plt.plot([0,30], [newLine[0], (newLine[1] * 30) + newLine[0]])
plt.axis([0, 30, 0, 3])
plt.show()

plt.plot(converge);
plt.axis([-3, min(200, len(converge)), -3, max(converge) ])
plt.grid();
plt.show();
