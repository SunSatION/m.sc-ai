# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class EbayItemBidItem(scrapy.Item):
    item_id = scrapy.Field() # response.selector.css('#vizItemNum::text').extract_first()
    bidder_name = scrapy.Field() # response.selector.xpath('td[2]/a[1]') + xpath('following-sibling::a[@id="feedBackScoreDiv3"]/text()').extract_first()
    bidder_feedback = scrapy.Field()
    bid_amount = scrapy.Field() # xpath('td[3]/span/text()').extract_first()
    bid_date = scrapy.Field() # xpath('td[4]//span/text()').extract_first()
    extra_details_url = scrapy.Field() # xpath(td[2]/a[1]@href').extract_first()
    automated_bid = scrapy.Field()
    bid_position = scrapy.Field()

class EbaycrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    item_id = scrapy.Field() # response.selector.css('div#descItemNumber::text').extract_first()
    title = scrapy.Field() # //meta[@property="og:title"]/@content').extract_first()
    condition = scrapy.Field() # response.selector.css('div#vi-itm-cond::text').extract_first()
    end_date = scrapy.Field() # response.selector.css('.timeMs').xpath('@timems').extract_first()
    winning_bid = scrapy.Field() # response.selector.css('span#prcIsum_bidPrice::text').extract_first()
    starting_bid = scrapy.Field()
    currency_code = scrapy.Field() # response.selector.css('span#prcIsum_bidPrice::text').extract_first()
    item_location = scrapy.Field() #  response.selector.css('div#itemLocation div.iti-eu-bld-gry::text').extract_first()
    ships_to = scrapy.Field() # response.selector.css('div#shipsTo div.iti-eu-bld-gry::text').extract_first()
    seller_id = scrapy.Field() # response.selector.css('.mbg-nw::text').extract_first()
    item_picture = scrapy.Field() # response.selector.css('img#icImg').xpath('@src').extract_first()
    category = scrapy.Field() # response.selector.css('li.bc-w').extract() // work on
    same_items = scrapy.Field() # response.selector.css('div#merch_html_100011 a[class="mfe-reco-sugg-link"]').xpath('@href').extract()
    people_interested = scrapy.Field() # response.selector.css('div#merch_html_100012 a[class="mfe-reco-sugg-link"]').xpath('@href').extract()
    item_questions = scrapy.Field() # response.selector.css('div.asqContent').extract_first()
    delivery_time = scrapy.Field() # In [44]: response.selector.css('span#delSummary > div.sh-del-frst > div:nth-child(1)::text').extract()
    payments = scrapy.Field()
    accepts_paypal = scrapy.Field() # true if len(response.selector.xpath('//*[@alt="PayPal"]').extract() > 0) else false
    return_time = scrapy.Field() # response.selector.css('s[an#vi-ret-accrd-txt::text').extract_first()
    html_description = scrapy.Field() # response.selector.xpath('//iframe/@src').extract()
    item_description = scrapy.Field() # response.selector.xpath('meta[@property="og:description"]/@content').extract_first()
    no_of_bids = scrapy.Field() # response.selector.css('span#qty-test').extract_first()
    ebay_guarantee = scrapy.Field() # response.selector.css('span#vi-ebp2-no-logo').extract_first()
    keywords = scrapy.Field() # response.selector.xpath('//meta[@name="keywords"]/@content').extract_first()
    image_url = scrapy.Field() # //meta[@property="og:image"]/@content').extract_first()
    item_url = scrapy.Field() # //meta[@property="og:url"]/@content').extract_first()
    no_of_bidders = scrapy.Field() # response.selector.xpath('//*[@id="vizrefresh"]/tr/td/div[1]/div[2]/table[1]/tr/td[1]/span[2]/text()').extract_first()
    duration = scrapy.Field() # response.selector.xpath('//*[@id="vizrefresh"]/tr/td/div[1]/div[2]/table[1]/tr/td[1]/span[12]/text()').extract_first()
    msg = scrapy.Field() #
    category_desc = scrapy.Field()
    seller_starcount = scrapy.Field()
    seller_feedback = scrapy.Field()
    bids = scrapy.Field() # response.selector.css('tr#vizRow1').xpath('following-sibling::*')
    html = scrapy.Field()
    shipping_fee = scrapy.Field()
    price = scrapy.Field();

    def __setitem__(self, key, value):
        if key not in self.fields:
            self.fields[key] = scrapy.Field()
        self._values[key] = value


#response.selector.xpath('//meta[@property="og:image"]/@content').extract_first()').extract_first()

#response.selector.xpath('//meta[@name="keywords"]/@content').extract_first()
