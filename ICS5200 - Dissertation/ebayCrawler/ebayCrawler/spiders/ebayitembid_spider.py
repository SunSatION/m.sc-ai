__author__ = 'Dorian Bugeja'
__date__ = '31/10/2015'

import scrapy

from scrapy.loader import ItemLoader
from ebayCrawler.items import EbayItemBidItem

class EbayItemBidSpider(scrapy.Spider):
    name = "ebayitembid"
    allowed_domains = ["http://offer.ebay.com/"]
    start_urls = ["http://offer.ebay.com/ws/eBayISAPI.dll?ViewBids&item=391303308781&showauto=true"]

    def parse(self, response):
        idx = 0
        allBids = []
        for sel in response.selector.css('tr.tabHeadDesign ').xpath('following-sibling::*[not(@id="viznobrd")]'):
            l = EbayItemBidItem();
            l['item_id'] = response.selector.css('#vizItemNum::text').extract()[1].strip()
            l['bidder_name'] =  sel.xpath('td[2]/a[1]/text()').extract_first()
            l['bidder_name'] = l['bidder_name'].strip() if l['bidder_name'] != None else l['bidder_name']
            l['bidder_feedback'] = sel.xpath('td[2]/a[@id="feedBackScoreDiv3"]/text()').extract_first()
            l['bidder_feedback'] = l['bidder_feedback'].strip() if l['bidder_feedback'] != None else l['bidder_feedback']
            l['bid_amount'] = sel.xpath('td[3]/span/text()').extract_first().strip()
            l['bid_date'] = sel.xpath('td[4]//span/text()').extract()
            l['bid_date'][0] = l['bid_date'][0].strip();
            l['bid_date'][1] = l['bid_date'][1].strip();

            l['extra_details_url'] = sel.xpath('td[2]/a[1]/@href').extract_first()
            l['automated_bid'] = len(sel.xpath('td[@class="newcontentValueFont"][1]').extract())
            l['bid_position'] = idx;
            idx += 1
            yield l
            allBids.append(l)


        sel = response.selector.css('tr.tabHeadDesign ').xpath('following-sibling::*[@id="viznobrd"]')
        l = EbayItemBidItem();
        l['item_id'] = response.selector.css('#vizItemNum::text').extract()[1].strip()

        l['bidder_name'] =  sel.xpath('td[2]/text()').extract_first()
        l['bidder_name'] = l['bidder_name'].strip() if l['bidder_name'] != None else l['bidder_name']

        l['bidder_feedback'] = sel.xpath('td[2]/a[@id="feedBackScoreDiv3"]/text()').extract_first()
        l['bidder_feedback'] = l['bidder_feedback'].strip() if l['bidder_feedback'] != None else l['bidder_feedback']

        l['bid_amount'] = sel.xpath('td[3]/text()').extract_first().strip().strip()
        l['bid_date'] = sel.xpath('td[4]//span/text()').extract()
        l['bid_date'][0] = l['bid_date'][0].strip();
        l['bid_date'][1] = l['bid_date'][1].strip();

        l['extra_details_url'] = sel.xpath('td[2]/a[1]/@href').extract_first()
        l['automated_bid'] = len(sel.xpath('td[@class="newcontentValueFont"][1]').extract())

        l['bid_position'] = idx
        allBids.append(l)
        yield l

