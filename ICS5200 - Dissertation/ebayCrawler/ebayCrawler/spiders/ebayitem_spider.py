__author__ = 'Dorian Bugeja'
__date__ = '31/10/2015'


import scrapy
import urlparse
import ast

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.loader import ItemLoader
from ebayCrawler.items import EbaycrawlerItem
from ebayCrawler.items import EbayItemBidItem


import redis
import socket
import time


r = redis.StrictRedis(host='root.ludoi.com', port=6336, db=0, password='z1x2c3v4b5!!')

def clean_selector_result(result, extract=0, strip=0, extract_first=0, combine=0):
    if result is not None:
        if extract_first == 1:
            result = result.extract_first()
            if strip == 1 and result is not None:
                return result.strip()
            else:
                return result

        if extract == 0:
            return result
        else:
            to_return = '';
            if combine == 1:
                result = result.xpath('.//text()').extract()



                for i in result:
                    stripCurrent = ''
                    if strip == 1:
                        if i is not None:
                            stripCurrent = i.strip()
                        else:
                            stripCurrent = i

                    to_return = to_return + stripCurrent + " "


                return to_return

            result = result.extract();
            if strip == 0:
                return result
            else:
                array_return = []
                for j in result:
                    if ( j is not None ):
                        array_return.append(j.strip())
                    else:
                        array_return.append(j);
                return array_return


def multiple_xpath(selector, xpath_array, extract=0, strip=0, extract_first=0, combine=0):
    for i in xpath_array:
        result = selector.xpath(i)
        if result is not None:
            break
    return clean_selector_result(result, extract, strip, extract_first, combine)


def multiple_css(selector, css_array, extract=0, strip=0, extract_first=0, combine=0):

        for i in css_array:
            result = selector.css(i)

            if result is None or result.extract_first() == '':
                continue

            returned = clean_selector_result(result, extract, strip, extract_first, combine)
            if not(returned == '' or returned is None) :
                return returned



class EbayItemSpider(scrapy.Spider):
    r = None

    name = "ebayitem"
    allowed_domains = ["www.ebay.com", "offer.ebay.com", "vi.vipr.ebaydesc.com", "cgi.ebay.com"]
    handle_httpstatus_list = [404]
    def start_requests(self):
        item_no = None;

        while True:
            # process queue as FIFO, change `blpop` to `brpop` to process as LIFO
            source, data = r.blpop(["mscai:ebayitems_to_visit"])
            o = urlparse.urlparse(ast.literal_eval(data)["url"])

            item_no = urlparse.parse_qs(o.query).get('itm')
            allPaths = o.path.split('/')
            item_no = allPaths[len(allPaths) - 1]
            print('the item number is : ' + item_no)

            if r.hexists('mscai:ebayitems_visited', item_no):
                r.hget('mscai:ebayitems_visited', item_no)
                print('already exists - ' + item_no)
            else:
                print('new entry - ' + item_no)
                r.hset('mscai:ebayitems_visited', item_no, {'computer': socket.gethostname(), 'url' : ast.literal_eval(data)["url"], 'start_time': str(int(time.time())), 'end_time': "0", 'status': 'Started' })
                request = scrapy.Request(ast.literal_eval(data)["url"], callback=self.parse)
                return [request]

                #yield self.make_requests_from_url(ast.literal_eval(data)["url"])




    def parse(self, response):
        try:

            l = EbaycrawlerItem();
            print('crawl started');
            try:
                l["item_id"] = response.selector.css('div#descItemNumber::text').extract_first()
            except Exception as inst:
                print inst

            try:
                l["title"] = response.selector.xpath('//meta[@property="og:title"]/@content').extract_first()
            except Exception as inst:
                print inst
            try:
                l["condition"] = response.selector.css('div#vi-itm-cond::text').extract_first()
            except Exception as inst:
                print inst
            try:
                l["end_date"] = response.selector.css('.timeMs').xpath('@timems').extract_first()
            except Exception as inst:
                print inst
            try:
                l["winning_bid"] = response.selector.css('span#prcIsum_bidPrice::text').extract_first()
            except Exception as inst:
                print inst
            try:
                l["currency_code"] = response.selector.css('span#prcIsum_bidPrice::text').extract_first()
            except Exception as inst:
                print inst
            try:
                l["item_location"] = response.selector.css('div#itemLocation div.iti-eu-bld-gry::text').extract_first()
            except Exception as inst:
                print inst
            try:
                l["ships_to"] = multiple_css(response.selector, ['span.sh-sLoc::text', 'div.sh-sLoc::text', 'div#shipsTo div.iti-eu-bld-gry::text', 'span.sh-gspShipsTo::text'], 1, 1, 1, 0)
            except Exception as inst:
                print inst
            try:
                l["seller_id"] = response.selector.css('.mbg-nw::text').extract_first()
            except Exception as inst:
                print inst
            try:
                l["seller_feedback"] = response.selector.css('div#si-fb::text').extract_first()
            except Exception as inst:
                print inst
            try:
                l["seller_starcount"] = response.selector.css('span.mbg-l a::text').extract_first()
            except Exception as inst:
                print inst

            try:
                l["price"] = response.selector.css('span#prcIsum::text').extract_first()
            except Exception as inst:
                print inst



            try:
                l["item_picture"] = response.selector.css('img#icImg').xpath('@src').extract_first()
            except Exception as inst:
                print inst
            try:
                l["category"] = response.selector.css('li.bc-w a span::text').extract() # work on
            except Exception as inst:
                print inst

            l["same_items"] = []

            for i in response.selector.css('div#merch_html_100011 a[class="mfe-reco-sugg-link"]').xpath('@href').extract():
                o = urlparse.urlparse(i)
                l["same_items"].append(urlparse.parse_qs(o.query).get('itm')[0])

            l["people_interested"] = []
            for i in response.selector.css('div#merch_html_100012 a[class="mfe-reco-sugg-link"]').xpath('@href').extract():
                o = urlparse.urlparse(i)
                l["people_interested"].append(urlparse.parse_qs(o.query).get('itm')[0])

            l["shipping_fee"] = ""
            for i in response.selector.css('span#shSummary').xpath('.//text()').extract():
                if ( len(i.strip()) > 0 ):
                    l["shipping_fee"] = l["shipping_fee"] + i.strip() + " "

            try:
                l["shipping_fee"] = l["shipping_fee"][:l["shipping_fee"].index('|')]
            except:
                pass


            try:
                l["item_questions"] = multiple_css(response.selector, ['div.asqContent'], 1, 1, 0, 1)
            except Exception as inst:
                print inst


            try:
                l["delivery_time"] = multiple_css(response.selector, ['div.sh-del-frst', 'span#delSummary div.sh-del-frst div:nth-child(1)'], 1, 1, 0, 1);
            except Exception as inst:
                print inst

            try:
                l["accepts_paypal"] = 1 if len(response.selector.xpath('//*[@alt="PayPal"]').extract()) > 0 else 0
            except Exception as inst:
                print inst

            try:
                l["return_time"] = response.selector.css('span#vi-ret-accrd-txt::text').extract_first()
            except Exception as inst:
                print inst

            try:
                l["html_description"] = response.selector.xpath('//iframe/@src').extract_first()
            except Exception as inst:
                print inst
            try:
                l["item_description"] = response.selector.xpath('//meta[@property="og:description"]/@content').extract_first()
            except Exception as inst:
                print inst
            try:
                l["no_of_bids"] = response.selector.css('span#qty-test::text').extract_first()
            except Exception as inst:
                print inst
            try:
                l["ebay_guarantee"] = response.selector.css('span#vi-ebp2-no-logo::text').extract_first()
            except Exception as inst:
                print inst
            try:
                l["keywords"] = response.selector.xpath('//meta[@name="keywords"]/@content').extract_first().strip()
            except Exception as inst:
                print inst
            try:
                l["image_url"] = response.selector.xpath('//meta[@property="og:image"]/@content').extract_first()
            except Exception as inst:
                print inst
            try:
                l["item_url"] = response.url
            except Exception as inst:
                print inst

            l["msg"] = "";

            for i in response.selector.css('span#w1-4-_msg').xpath('.//text()').extract():
                if i is not None:
                    l["msg"] = l["msg"] + i.strip() + " "

            l["category_desc"] = {}
            for i in response.selector.css('div.itemAttr td.attrLabels'):
                l["category_desc"][i.xpath('text()').extract_first().strip().replace(':', '')] = i.xpath('following-sibling::td[1]//*//text()[not(parent::p)]').extract_first().strip()

            request = scrapy.Request(l["html_description"], callback=self.parse_html_body)
            request.meta['item'] = l
            yield request
        except:
            while True:
                source, data = r.blpop(["mscai:ebayitems_to_visit"])
                o = urlparse.urlparse(ast.literal_eval(data)["url"])

                item_no = urlparse.parse_qs(o.query).get('itm')
                allPaths = o.path.split('/')
                item_no = allPaths[len(allPaths) - 1]
                print('the item number is : ' + item_no)
                if r.hexists('mscai:ebayitems_visited', item_no):
                    r.hget('mscai:ebayitems_visited', item_no)
                    print('already exists - ' + item_no)
                else:
                    break
            print('new entry - ' + item_no)
            r.hset('mscai:ebayitems_visited', item_no, {'computer': socket.gethostname(), 'url': ast.literal_eval(data)["url"], 'start_time': str(int(time.time())), 'end_time': str(int(time.time())), 'status': 'Started' })
            request = scrapy.Request(ast.literal_eval(data)["url"], callback=self.parse)
            yield request

    def parse_html_body(self, response):
        try:
            item = response.meta['item']
            item["html_description"] = response.body

            request = scrapy.Request('http://offer.ebay.com/ws/eBayISAPI.dll?ViewBids&item=' + str(item['item_id']) + '&showauto=true',  headers={'referer':item["item_url"]}, callback=self.parse_bids)
            request.meta['item'] = item
            yield request
        except:
            while True:
                source, data = r.blpop(["mscai:ebayitems_to_visit"])
                o = urlparse.urlparse(ast.literal_eval(data)["url"])

                item_no = urlparse.parse_qs(o.query).get('itm')
                allPaths = o.path.split('/')
                item_no = allPaths[len(allPaths) - 1]
                print('the item number is 3: ' + item_no)
                if r.hexists('mscai:ebayitems_visited', item_no):
                    r.hget('mscai:ebayitems_visited', item_no)
                    print('already exists - ' + item_no)
                else:
                    break
            print('the item number is 3: ' + item_no)
            r.hset('mscai:ebayitems_visited', item_no, {'computer': socket.gethostname(), 'url': ast.literal_eval(data)["url"], 'start_time': str(int(time.time())), 'end_time': str(int(time.time())), 'status': 'Started' })
            request = scrapy.Request(ast.literal_eval(data)["url"], callback=self.parse)
            yield request


    def multiple_css(self, selector, xpath_array):
        for i in xpath_array:
            result = selector.xpath(i)
            if ( result is not None ):
                return result

    def parse_bids(self, response):
        try:
            item = response.meta['item'];
            idx = 0
            allBids = []




            for sel in response.selector.css('tr.tabHeadDesign ').xpath('following-sibling::*[not(@id="viznobrd")]'):

                try:
                    l = EbayItemBidItem();
                except Exception as inst:
                    print inst
                try:
                    l['bidder_name'] =  sel.xpath('td[2]/a[1]/text()').extract_first()
                except Exception as inst:
                    print inst
                try:
                    l['bidder_name'] = l['bidder_name'].strip() if l['bidder_name'] != None else l['bidder_name']
                except Exception as inst:
                    print inst
                try:
                    l['bidder_feedback'] = sel.xpath('td[2]/a[@id="feedBackScoreDiv3"]/text()').extract_first()
                except Exception as inst:
                    print inst
                try:
                    l['bidder_feedback'] = l['bidder_feedback'].strip() if l['bidder_feedback'] != None else l['bidder_feedback']
                except Exception as inst:
                    print inst

                try:
                    l['bid_amount'] = multiple_xpath(sel, ['td[3]/span/text()', 'td[3]/text()'], extract=1, strip=1, extract_first=1)
                except Exception as inst:
                    print inst

                try:
                    l['bid_date'] = multiple_xpath(sel, ['td[4]//span/text()'], extract=1)
                except Exception as inst:
                    print inst

                try:
                    if ( len(l['bid_date']) > 0 ):
                        l['bid_date'][0] = l['bid_date'][0].strip();
                        l['bid_date'][1] = l['bid_date'][1].strip();
                except Exception as inst:
                    print inst

                try:
                    l['extra_details_url'] = sel.xpath('td[2]/a[1]/@href').extract_first()
                except Exception as inst:
                    print inst

                try:
                    l['automated_bid'] = len(sel.xpath('td[@class="newcontentValueFont"][1]').extract())
                except Exception as inst:
                    print inst

                try:
                    l['bid_position'] = idx;
                except Exception as inst:
                    print inst

                idx += 1

                try:
                    if l['bid_amount'] is not None or l['automated_bid'] != 0:
                        allBids.append(l)
                except Exception as inst:
                    print inst


            sel = response.selector.css('tr.tabHeadDesign ').xpath('following-sibling::*[@id="viznobrd"]')
            l = EbayItemBidItem();
            try:
                l['bidder_name'] =  sel.xpath('td[2]/text()').extract_first()
            except Exception as inst:
                print inst
            try:
                l['bidder_name'] = l['bidder_name'].strip() if l['bidder_name'] != None else l['bidder_name']
            except Exception as inst:
                print inst

            try:
                l['bidder_feedback'] = sel.xpath('td[2]/a[@id="feedBackScoreDiv3"]/text()').extract_first()
            except Exception as inst:
                print inst
            try:
                l['bidder_feedback'] = l['bidder_feedback'].strip() if l['bidder_feedback'] != None else l['bidder_feedback']
            except Exception as inst:
                print inst

            try:
                l['bid_amount'] = multiple_xpath(sel, ['td[3]/span/text()', 'td[3]/text()'], extract=1, strip=1, extract_first=1)
            except Exception as inst:
                print inst

            try:
                l['bid_date'] = multiple_xpath(sel, ['td[4]//span/text()'], extract=1)
            except Exception as inst:
                print inst

            if ( len(l['bid_date']) > 0 ):
                l['bid_date'][0] = l['bid_date'][0].strip();
                l['bid_date'][1] = l['bid_date'][1].strip();

            try:
                l['extra_details_url'] = sel.xpath('td[2]/a[1]/@href').extract_first()
            except Exception as inst:
                print inst

            try:
                l['automated_bid'] = len(sel.xpath('td[@class="newcontentValueFont"][1]').extract())
            except Exception as inst:
                print inst

            try:
                l['bid_position'] = idx
            except Exception as inst:
                print inst

            if l['bid_amount'] is not None or l['automated_bid'] != 0:
                allBids.append(l)

            item['bids'] = allBids

            try:
                item["no_of_bidders"] = response.selector.xpath('//*[@id="vizrefresh"]/tr/td/div[1]/div[2]/table[1]/tr/td[1]/span[2]/text()').extract_first().strip()
            except Exception as inst:
                print inst

            try:
                item["duration"] = response.selector.xpath('//*[@id="vizrefresh"]/tr/td/div[1]/div[2]/table[1]/tr/td[1]/span[last()]/text()').extract_first().strip()
            except Exception as inst:
                print inst



            print('the item number is 4: ' + item['item_id'])
            vitObject = ast.literal_eval(r.hget('mscai:ebayitems_visited', item['item_id']))

            print(item['item_id'] + ' done')

            if ( vitObject != None):
                print('It exists!!!')
                vitObject['end_time'] = int(time.time())
                vitObject['status'] = 'Finished'
                r.hset('mscai:ebayitems_visited', item['item_id'], vitObject)
            else:
                r.hset('mscai:ebayitems_visited', item['item_id'], {'computer': socket.gethostname(), 'url': item['item_url'], 'start_time': str(int(time.time())), 'end_time': int(time.time()), 'status': 'Finished' })

            yield item;

            while True:
                source, data = r.blpop(["mscai:ebayitems_to_visit"])
                o = urlparse.urlparse(ast.literal_eval(data)["url"])

                item_no = urlparse.parse_qs(o.query).get('itm')
                allPaths = o.path.split('/')
                item_no = allPaths[len(allPaths) - 1]
                if r.hexists('mscai:ebayitems_visited', item_no):
                    r.hget('mscai:ebayitems_visited', item_no)
                else:
                    break
            r.hset('mscai:ebayitems_visited', item_no, {'computer': socket.gethostname(), 'url': ast.literal_eval(data)["url"], 'start_time': str(int(time.time())), 'end_time': str(int(time.time())), 'status': 'Started' })
            print(item_no + '  started')
            request = scrapy.Request(ast.literal_eval(data)["url"], callback=self.parse)
            yield request

        except:
            while True:
                source, data = r.blpop(["mscai:ebayitems_to_visit"])
                o = urlparse.urlparse(ast.literal_eval(data)["url"])

                item_no = urlparse.parse_qs(o.query).get('itm')
                allPaths = o.path.split('/')
                item_no = allPaths[len(allPaths) - 1]

                print('the item number is 7: ' + item_no)
                if r.hexists('mscai:ebayitems_visited', item_no):
                    r.hget('mscai:ebayitems_visited', item_no)
                    print('already exists - ' + item_no)
                else:
                    break
            print('new entry - ' + item_no)
            r.hset('mscai:ebayitems_visited', item_no, {'computer': socket.gethostname(), 'url': ast.literal_eval(data)["url"], 'start_time': str(int(time.time())), 'end_time': str(int(time.time())), 'status': 'Started' })
            request = scrapy.Request(ast.literal_eval(data)["url"], callback=self.parse)
            yield request

    def process_exception(self):
        while True:
            source, data = r.blpop(["mscai:ebayitems_to_visit"])
            o = urlparse.urlparse(ast.literal_eval(data)["url"])

            item_no = urlparse.parse_qs(o.query).get('itm')
            allPaths = o.path.split('/')
            item_no = allPaths[len(allPaths) - 1]
            print('the item number is 8: ' + item_no)
            if r.hexists('mscai:ebayitems_visited', item_no):
                r.hget('mscai:ebayitems_visited', item_no)
                print('already exists - ' + item_no)
            else:
                break
        print('new entry - ' + item_no)
        r.hset('mscai:ebayitems_visited', item_no, {'computer': socket.gethostname(), 'url': ast.literal_eval(data)["url"], 'start_time': str(int(time.time())), 'end_time': str(int(time.time())), 'status': 'Started' })
        request = scrapy.Request(ast.literal_eval(data)["url"], callback=self.parse)
        yield request
