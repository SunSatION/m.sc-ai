from enum import Enum
import pandas as pd
import numpy as np
import math
import random
import pickle

from graphviz import Digraph


"""
dot.node('A', 'King Arthur')
dot.node('B', 'Sir Bedevere the Wise')
dot.node('L', 'Sir Lancelot the Brave')

dot.edges(['AB', 'AL'])
dot.edge('B','L', constraint='false', label='Nice')
"""


class node(object):
    def __init__(self):
        self.value = None
        self.children = []
        self.desisionAttribute = None;
        self.parent = None;

    def setLabel(self, lab):
        self.value = lab;

    def addChild(self, child):
        self.children.append(child)

    def setDecisionAttr(self, attr):
        self.desisionAttribute = attr

    def setParent(self, node):
        self.parent = node

class Element:
    entropy = 0.

    def __init__(self, df=None):
        self.dtFrame = df


attributeSet = [
    ['Outlook', 'Temperaute', 'Humidity', 'Wind', 'Decision'],
    ['Sunny', 'Overcase', 'Rain'],
    ['Hot', 'Mild', 'Cool'],
    ['High', 'Normal'],
    ['Weak', 'Strong'],
    ['Yes', 'No']
]


yesClassArray = ['Yes', 'No']
"""
yesClassArray = ["Nothinghand", "Onepair", "Twopairs", "Threekind", "Straight", "Flush", "Fullhouse", "Fourkind", "Straightflush", "Royalflush" ];


attributeSet = [
    ["S1", "C1", "S2", "C2", "S3", "C3", "S4", "C4", "S5", "C5"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
]

"""
inputTraining = pd.read_csv('play_tennis.txt', header=None)

def ID3(input_training, target_column, classifier, root, yes_class):

    groupedTraining = input_training.groupby(target_column);

    if input_training[target_column].nunique() == 1:
        root.setLabel(( '+' + str(random.randrange(1, 1000))) if groupedTraining.head(1).reset_index().loc[0, target_column] == yes_class else ('-' + str(random.randrange(1, 1000))))
        return root;

    if input_training.shape[1] == 1:
        root.setLabel('Unknown')
        root.setDecisionAttr(groupedTraining.apply(lambda x: x[0].count()).sort_values(ascending=False).index[0])
        return root

    entropyCount = groupedTraining[target_column].count();
    currentEntropy = 0

    for i in range(0, entropyCount.size):
        currentEntropy += (entropyCount.iloc[i] / entropyCount.sum()) * math.log2(
            (entropyCount.iloc[i] / entropyCount.sum()))

    currentEntropy *= -1
    gains = []

    toIterate = np.delete(input_training.columns._data, -1, 0)
    for currentAttribute in toIterate:
        a = []
        for k in range(1, groupedTraining[currentAttribute].nunique().max() + 1):
            a.append(Element(pd.DataFrame(groupedTraining.apply(lambda x: x[x[currentAttribute] == k][currentAttribute].count()))))

        currentSubEntropy = 0;
        for j in range(len(a)):
            subEntropy = 0
            for i in range(0, a[j].dtFrame.size):
                if a[j].dtFrame.iloc[i][0] != 0:
                    subEntropy += (a[j].dtFrame.iloc[i] / a[j].dtFrame.sum()) * (
                    math.log2((a[j].dtFrame.iloc[i] / a[j].dtFrame.sum())))
            a[j].entropy = subEntropy * -1
            currentSubEntropy += ((a[j].dtFrame.sum() / entropyCount.sum()) * a[j].entropy).item()

        gains.append(((currentEntropy - currentSubEntropy), currentAttribute))

    gains = sorted(gains, key=lambda gainSort: gainSort[0], reverse=True)

    root.setLabel(attributeSet[0][gains[0][1]] + "_" + str(random.randrange(0, 1000)))

    for i in range(0, input_training[gains[0][1]].unique().shape[0]):
        newNode = node();
        newNode.setDecisionAttr(attributeSet[gains[0][1]+1][input_training[gains[0][1]].unique()[i].item() - 1])
        newNode.setParent(root)
        #dot.edge(newNode.parent.value, attributeSet[0][gains[0][1]] + str(random.randrange(0, 100)), newNode.desisionAttribute)

        root.addChild(newNode)

        inputTraining2 = input_training[input_training[gains[0][1]] == input_training[gains[0][1]].unique()[i].item()]
        classifier = gains[0][1];

        input_training2 = inputTraining2.drop(inputTraining2[[classifier]], axis=1)
        if input_training2.shape[0] == 0:
            return root
        else:
            leaf = ID3(input_training2, target_column, gains[0][1], newNode, yes_class)
            leaf.parent = root;
            dot[yes_class].edge(newNode.parent.value, leaf.value, newNode.desisionAttribute)
    #dot.render('test-output/round-table.gv', view=True)
    return root

from multiprocessing import Pool

dot = [];

for i in range(30):
   dot.append(None)

def p_ID3(toCluster):
    d = node();
    print("Cluster " + str(toCluster) + " started")
    dot[toCluster] = Digraph(comment='Decision Tree for '+ yesClassArray[toCluster - 1])
    tree = ID3(inputTraining, 4, -1, d, toCluster)
    with open('Decision Tree v2 for '+ yesClassArray[toCluster - 1] + '_tree.pickle', 'wb') as f:
        pickle.dump(tree, f)
    print("Cluster: " + str(toCluster) + "done.");
    print(dot[toCluster].source)
    #dot[toCluster].render('test-output/round-table' + str(toCluster) + '.gv', view=True)
    with open('Decision Tree v2 for '+ yesClassArray[toCluster - 1] + '_drawing.pickle', 'wb') as f:
        pickle.dump(dot[toCluster], f)

p_ID3(1)
"""
if __name__ == '__main__':
    with Pool(5) as p:
        p.map(p_ID3, range(10, 0, -1))
"""
"""
with open('Decision Tree for Outlook.pickle', 'rb') as f:
    tree = pickle.load(f)
    print(tree)

"""

def updateEdges(root):
    """
    @type param: node
    """
    for i in range(0, len(root.children)):
        dot.edge(root.value, root.children[i].value, root.children[i].value)


#updateEdges(d);


#print(dot.source)





# for i in range()
# uniqueValues = input_training[gains[0][1]].unique();

"""
        updateEdges(root.children[i])
    print()

groupedTraining = inputTraining.groupby(target_column);
entropyCount = groupedTraining[0].count();

currentEntropy = 0

for i in range(0,entropyCount.size):
    currentEntropy += ( entropyCount.iloc[i] / entropyCount.sum() ) * math.log2(( entropyCount.iloc[i] / entropyCount.sum() ))

currentEntropy *= -1

total = inputTraining.size
gains = []
attributeCount = 4
for currentAttribute in range(attributeCount):
    a = []
    for k in range(1, groupedTraining[currentAttribute].nunique().max() + 1):
        a.append(Element(pd.DataFrame(groupedTraining.apply(lambda x: x[x[currentAttribute] == k][0].count()))))
    subEntropy = 0
    currentSubEntropy = 0;
    for j in range(len(a)):
        subEntropy = 0
        for i in range(0,a[j].dtFrame.size):
            if a[j].dtFrame.iloc[i][0] != 0:
                subEntropy += ( a[j].dtFrame.iloc[i] / a[j].dtFrame.sum() ) * ( math.log2(( a[j].dtFrame.iloc[i] / a[j].dtFrame.sum() )))
        a[j].entropy = subEntropy*-1
        currentSubEntropy += ((a[j].dtFrame.sum() / entropyCount.sum()) * a[j].entropy).item()

    gains.append(((currentEntropy - currentSubEntropy), currentAttribute))
gains = sorted(gains, key=lambda gainSort: gainSort[0], reverse=True)


class Ord(Enum):
    Hearts = 1
    Spades = 2
    Diamonds = 3
    Clubs = 4

class PokerClass(Enum):
    Nothinghand = 0
    Onepair = 1
    Twopairs = 2
    Threekind = 3
    Straight = 4
    Flush = 5
    Fullhouse = 6
    Fourkind = 7
    Straightflush = 8
    Royalflush = 9
"""
