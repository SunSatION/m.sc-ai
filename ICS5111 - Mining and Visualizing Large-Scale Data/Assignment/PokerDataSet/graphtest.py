__author__ = 'Test'


from graphviz import Digraph

dot = Digraph(comment='The Round Table')
dot.node('King Arthur', 'King Arthur')
dot.node('B', 'Sir Bedevere the Wise')
dot.node('L', 'Sir Lancelot the Brave')


dot.edge('King Arthur','L', label='1')
dot.edge('King Arthur','B', label='2')



print(dot.source)
dot.render('test-output/round-table.gv', view=True)

from multiprocessing import Process

def f(name):
    print('hello', name)

if __name__ == '__main__':
    p = Process(target=f, args=('bob',))
    p.start()
    p.join()
