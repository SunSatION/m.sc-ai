__author__ = 'Dorian Bugeja'
__date__ = '25/10/2015'
import pickle
from graphviz import Digraph

#with open('Decision Tree for Flush_drawing.pickle', 'rb') as f:
#    dot = pickle.load(f)

#    print(dot.source)

from multiprocessing import Pool


class node(object):
    def __init__(self):
        self.value = None
        self.children = []
        self.desisionAttribute = None;
        self.parent = None;

    def setLabel(self, lab):
        self.value = lab;

    def addChild(self, child):
        self.children.append(child)

    def setDecisionAttr(self, attr):
        self.desisionAttribute = attr

    def setParent(self, node):
        self.parent = node

class Element:
    entropy = 0.

    def __init__(self, df=None):
        self.dtFrame = df

attributeSet = [
    ["S1", "C1", "S2", "C2", "S3", "C3", "S4", "C4", "S5", "C5"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
    ["Hearts", "Spades", "Diamonds", "Clubs"],
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
]


keys = {
    "S1" : 0,
    "C1" : 1,
    "S2" : 2,
    "C2" : 3,
    "S3" : 4,
    "C3" : 5,
    "S4" : 6,
    "C4" : 7,
    "S5" : 8,
    "C5" : 9
}




def traverseDecisionTree(tree, currentInstance):

    #print(tree);
    if ((tree.value[0] == '+') or (tree.value[0] == '-')):
        return tree.value[0]
    else:
        attrToMatch = currentInstance[keys[tree.value[0:2]]]
        if len(tree.children) == 0:
            return tree.value
        else:
            for i in range(0, len(tree.children)):
                if (tree.children[i].desisionAttribute == attributeSet[keys[tree.value[0:2]] + 1][currentInstance[keys[tree.value[0:2]]] - 1]):
                    return traverseDecisionTree(tree.children[i], currentInstance)


yesClassArray = ["Nothinghand", "Onepair", "Twopairs", "Threekind", "Straight", "Flush", "Fullhouse", "Fourkind", "Straightflush", "Royalflush" ];
yesClassArray.reverse()

cache = []


for index, yesClass in enumerate(yesClassArray):
    with open("Decision Tree v2 for "+ yesClass +"_tree.pickle", 'rb') as f:
        cache.append(pickle.load(f))

correct = 0
incorrect = 0
unclassified = 0;

with open('poker-hand-testing.data', 'r') as testFile:
    for line in testFile:
        currentInstance = list(map(int, line.rstrip().split(',')))
        #currentInstance[-1] -= 1;

        for index, yesClass in enumerate(yesClassArray):
            tree = cache[index]
            decisionResult = traverseDecisionTree(tree, currentInstance );
            #if decisionResult == None:
#                unclassified += 1
#                break
            if ( decisionResult  == '+' ) and ( currentInstance[10] != 9 - index):
                #print(currentInstance)
                incorrect += 1

            if ( decisionResult  == '+' ) and ( currentInstance[10] == 9 - index):
                correct += 1
                break

#            else:
#                correct += 1

            if ( decisionResult  == '-' ) and ( currentInstance[10] == 9 - index):
                incorrect += 1
#            else:
 #               correct += 1

                #print(currentInstance)



print(correct)
print(incorrect)
print(unclassified)

#currentInstance = [1,11,3,12,3,10,4,9,2,13,4]

