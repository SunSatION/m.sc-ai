Technology Quarterly Q2 2013
In this Technology Quarterly
Stuck to the ground by red tape
Concrete heal thyself
A better waterworks
Allo allo
Zapping mosquitoes and corruption
Expanding care
Everlasting light
Ideas coming down the track
Speed is the new stealth
In praise of celestial mechanics
Teaching old microphones new tricks
Harnessing human computation
Reprints
Brain scan Brain scan
Harnessing human computation
Luis von Ahn helped save the internet from spammers His larger quest is to Luis von Ahn helped save the internet from spammers His larger quest is to Luis von Ahn helped save the internet from spammers His larger quest is to
put internet chores to productive use
Jun 1st 2013   From the print edition
Add this article to your reading list by clicking this button
Tweet
ONLY a few weeks into graduate school and aged just 22 Luis von Ahn helped
crack one of the thorniest problems bedevilling the web It was the year 2000
and free webbased email services were booming But spammers were creating
thousands of accounts automatically and using them to blast out messages When
the accounts were shut down they simply created new ones At the same time
sites selling tickets to concerts and sporting events were being besieged by
programs that bombarded them with orders snapping up the best seats for
resale at a higher price Websites needed a way to distinguish between human
visitors and automated ones
Mr von Ahn had just arrived at Carnegie Mellon University in Pittsburgh when
he and his PhD adviser Manuel Blum came up with just such a method The
solution had three requirements it had to be a test that humans could pass
easily and computers could notbut could use computers to determine whether
the response was correct The original idea was to show web users an image
for example of a cat or a roller coaster and ask them to identify it A
correct answer would indicate that the entity at the other end of the internet
connection was indeed human granting access to the webmail service or
ticketing site But it turned out that people were not very good at
identifying images reliably
Technology Quarterly
Stuck to the ground by red tape
Concrete heal thyself
A better waterworks
Allo allo
Zapping mosquitoes and corruption
Expanding care
Everlasting light
Ideas coming down the track
Speed is the new stealth
In praise of celestial mechanics
Teaching old microphones new tricks
Harnessing human computation
Reprints
So the pair came up with another idea displaying a distorted sequence of
letters and asking people to read them and type them into a box This proved
to be a much more reliable test of whether a visitor to a website was human or
not something that is known in computerscience terminology as a Turing
test in honour of Alan Turing a British computer scientist The result was
the CAPTCHA which stands for Completely Automated Public Turing test to tell
Computers and Humans Apart Yahoo and other webmail providers implemented
the system and it immediately made life harder for spammers
Mr von Ahn went on to get his doctorateand a phone call from Bill Gates of
Microsoft offering him a job which he turned down He has since created a
series of internetbased systems that bring many people together to perform
useful work by dividing tasks into tiny pieces often presented as a simple
test or game and aggregating the results A decade ago Mr von Ahn called his
approach human computation the title of his thesis and games with a
purposeprecursors to the modern techniques of crowdsourcing and
gamification
For example he noticed that search engines were bad at finding images
because pictures on web pages are rarely labelled with neat captions So he
created the ESP Game in which two players in different locations are
simultaneously shown the same image in their web browsers and asked to type
words describing what is in it Each round of the game ends when both players
use the same word so the aim is to use the most obvious descriptive terms In
so doing the players tag each image and signal which words best describe it
The technology was acquired by Google in 2005 to help label images for its
search engine
Shall we play a game
Mr von Ahn grew up in Guatemala the son of two doctors He stumbled into
computers indirectly In the mid1980s at the age of eight he wanted to play
video games But instead of giving him a Nintendo console his mother bought a
PC To play games on it he resorted to typing in programs from computer
magazines and working out how to crack the copyprotection schemes on games
sold on floppy discs The young Luis also spent some time at a confectionery
factory owned by his family He was fascinated by the machines that made and
wrapped the sweets and was soon taking some of them apart and reassembling
them His love of engineering endured but not his sweet tooth I got to play
around the whole timebut now I cant stand the taste of mint he says
In Guatemala nearly all students are tested before entering high school The
top 20 nationwide of whom Mr von Ahn was one are sent to a special school
He went on to study mathematics in America at Duke University switching to
computer science for his postgraduate studies because it was more practical
You talk to a mathematician and he tells you that hes one in three people
in the world who understand the problem and its not been solved for 200
years says Mr von Ahn A computer scientist says I solved an open problem
yesterday
In late 2006 Mr von Ahn had just started teaching at Carnegie Mellon when he
received a call from the MacArthur Foundation saying that he was being
awarded one of its coveted genius grants of 500000 Around the same time
he did a backoftheenvelope calculation to get a sense of CAPTCHAs
popularity and realised that about 200m squiggly words were being recognised
and typed into computers every day by internet users around the world At
about ten seconds apiece that amounted to around half a million hours daily
This improved the security of the internet but at the cost of making people
perform a task whose results were immediately discarded Surely the recipient
of a genius grant ought to be able to find a way to make more productive use
of their efforts
Driving home from a meeting in Washington DC in his blue Volkswagen Golf he
was struck by an idea Instead of showing users random letters why not show
them words from scans of old printed texts that automated document
digitisation systems based on optical character recognition could not
understand Such words were by definition incomprehensible to computers but
might be legible to humans They could be shown to people as part of a
modified CAPTCHA test based on two words One the control word is a known
word the other is an illegible word from a scanned document The user reads
and types in the two words and is granted access provided the control word is
correctly identified And when a few users separately provide the same
interpretation of the scanned word it is fed back to the digitisation system
People performing online security checks could thus be put to work digitising
old books and newspapers without even realising that they were doing so Mr
von Ahn called his new idea reCAPTCHA and when the New York Times began to
use the technology to digitise its archive he span it out into a separate
company In 2009 it too was acquired by Google for use in its ambitious book
digitisation project The slogan for reCAPTCHA is Stop spam read books
Mr von Ahn went to work at the internet giant for a year Paradoxically one of
his tasks while at Google was to shut down the ESP Game It had served its
purpose labelling enough images to train an imagerecognition system based on
artificial intelligence which could then perform the task automatically
More than 1 billion people have helped digitise the printed word by using
reCAPTCHA
Other similar projects followed Verbosity for example got players to create
a compendium of commonsense facts such as milk is white which people know
but computers do not But none of Mr von Ahns other projects have come close
to reCAPTCHA when it comes to doing useful work The system now handles 100m
words a day equivalent to 200m books a year If Google were to pay people
Americas minimum wage to read and type in those illegible words it would
cost it around 500m a year
Although still in his early 30s Mr von Ahn has already made a unique
contribution to computer science and artificial intelligence by harnessing
what he calls the combined power of humans and computers to solve problems
that would be impossible for either to solve alone Put simply the idea is
to take something that already happens and try to get something else out of
it he says His work exploits the internets ability to reduce coordination
and transaction costs so that the efforts of hundreds of millions of people
can be aggregated effectively Mr von Ahn estimates that more than 1 billion
people have helped digitise the printed word by using reCAPTCHA
Found in translation
The notion of human computation has spawned its own academic field But did
Mr von Ahn really set out to generate useful results from mundane tasks or
did he stumble on the concept with CAPTCHA and then apply it in other domains
It is a combination of both he says He recently came across a plan
devised when he was 13 and saved by his mother for a power company that would
operate a free gym and generate electricity from people lifting weights
cycling and so forth This was he now realises a precursor of his computer
science work which does the same for mental activity
Mr von Ahn has won many prizes including a presidential award for excellence
in science He has a gaggle of patents to his name His old blue Volkswagen
has been replaced by a blue Porsche And he continues to apply his distinctive
approach to new problems His latest project is a company which he cofounded
last year called Duolingo It helps people learn a foreign language the
game while also providing a translation service the useful work People
are shown a word or phrase which they do their best to translate others then
vote on the best translation Duolingo already has 3m users who use it on
average for 30 minutes a day
Although it is outwardly similar to reCAPTCHA Duolingo marks a further
development of Mr von Ahns model because it also exploits big data The
firm collects huge volumes of data and performs experiments to determine what
works best when learning a new language For example should one teach
adjectives before adverbs Even experts do not know because there has never
been a largescale empirical study Thanks to Duolingo there now is
Among its early findings is that the best way to teach a language varies
according to the students mother tongue When teaching English for example
pronouns such as him her and it are usually introduced early on But
this can confuse Spanish speakers who do not have an equivalent for it The
answer is to delay the introduction of the word it which makes Duolingo
users less likely to give up in frustration Mr von Ahn hopes to apply this
techniqueusing data to improve pedagogyin other disciplines
Education seems like it should be an equaliser but really it is not he
says If you have money you can get a good education if not you dont He
has seen at first hand what access to highquality education can do and wants
to use technology to make it more widely available This is he realises a
far more ambitious aim than blocking spam or scanning books useful as those
are But there is a clear thread running through his work even as he tries to
apply his approach to tackling bigger societal problems not just technical
ones Whether people are in the gym logging on to email or learning a new
language he wants to enable them to do something usefuland harness the
power that they generate
From the print edition Technology Quarterly
Tweet
Submit to reddit
View all comments 3
Want more Subscribe to The Economist and get the weeks most relevant news
and analysis
Print edition
X
Jun 1st 2013
May 25th 2013
May 18th 2013
May 11th 2013
Next in The world this week
X
Politics this week
Next in The world this week
X
Business this week
Next in The world this week
X
KALs cartoon
Next in Leaders
X
British politics and the young British politics and the young
The strange rebirth of liberal England
Young Britons have turned liberal both socially and economically Politicians
need to get on their
Next in Leaders
X
The worlds next great leap forward The worlds next great leap forward
Towards the end of poverty
Nearly 1 billion people have been taken out of extreme poverty in 20 years
The world should aim to
Next in Leaders
X
Dealing with Russia Dealing with Russia
Tougher love needed
When it comes to understanding Vladimir Putin trust Angela Merkels instincts
not Barack
Next in Leaders
X
Colombia and the FARC Colombia and the FARC
The price of peace
An understandable thirst for justice should not block a deal that would save
lives
Next in Leaders
X
Lowemission cars Lowemission cars
Flat batteries
The electric car stalls in the race to be the green wheels of the future That
is not a tragedy
Next in Letters
X
Letters Letters
On Taiwan dams health care New York Malaysia Microsoft being brainy
quotes
Next in Briefing
X
Poverty Poverty
Not always with us
The world has an astonishing chance to take a billion people out of extreme
poverty by 2030
Next in United States
X
Killer drones Killer drones
Out of the shadows
Barack Obamas rules for drones could shape the new global laws of war
Next in United States
X
Nuclear power Nuclear power
Fracked off
Thanks to cheap natural gas Americas nuclear renaissance is on hold
Next in United States
X
Urban revivals Urban revivals
On the waterfront
A oncedilapidated bit of South Boston becomes a hive of innovation
Next in United States
X
Boy Scouts Boy Scouts
Gays welcome
A knotty problem is resolved Merit badges all round
Next in United States
X
Religion in public life Religion in public life
Reading Dawkins in a cabin
Atheists complain about free Bibles in the wilderness
Next in United States
X
Health and safety Health and safety
The menace of mooshine
Saving America from raw milk
Next in United States
X
Agriculture Agriculture
At the trough
An awful farm bill faces opposition
Next in United States
X
Lexington Lexington
George Washington politician
The virtues of the pragmatic founding father are much missed nowadays
Next in The Americas
X
Colombia and the FARC Colombia and the FARC
Digging in for peace
A deal on land marks a welcome breakthrough in peace talks But there is still
much to do and not
Next in The Americas
X
Football in Brazil Football in Brazil
Into extra time
A fraught runup to the dress rehearsal for next years World Cup
Next in The Americas
X
Demography in Latin America Demography in Latin America
Autumn of the patriarchs
Traditional demographic patterns are changing astonishingly fast
Next in The Americas
X
Demography in Chile Demography in Chile
Child bribe
An attempt to raise the birth rate
Next in Asia
X
Indias ungoverned spaces Indias ungoverned spaces
Out of the trees
A murderous attack highlights neglect in Indias outland
Next in Asia
X
Dealing with Pakistans extremists Dealing with Pakistans extremists
The hawk and the dove
The new prime minister and his army chief profess different approaches to
dealing with the Pakistani
Next in Asia
X
Japans constitution Japans constitution
Back to the future
Shinzo Abes plan to rewrite Japans constitution is running into trouble
Next in Asia
X
Japan and SouthEast Asia Japan and SouthEast Asia
Hand in hand
Shinzo Abe has compelling diplomatic as well as economic reasons to push into
SouthEast Asia
Next in Asia
X
Banyan Banyan
Time on whose side
Angry at an unjust defeat Malaysias opposition has reasons to be hopeful
Next in Asia
X
Correction Philippines
Next in China
X
Urbanisation Urbanisation
Some are more equal than others
Chinas need for a new urbanisation policy reaches a critical point
Next in China
X
Ideological debate Ideological debate
Drawing the battle lines
A conservative backlash against a push for reform
Next in China
X
Bashing Japan Bashing Japan
Staged warfare
The government reins in overly dramatic antiJapanese television shows
Next in Middle East and Africa
X
South Africas economy South Africas economy
Muddle through will no longer do
Slow growth and a sliding currency are alarming symptoms of a deeper malaise
Next in Middle East and Africa
X
The African Union at 50 The African Union at 50
Shooting your own feet
Cocking a snook at international justice may impede the continents rise
Next in Middle East and Africa
X
Israel and Palestine Israel and Palestine
Boosting the West Banks economy
Businessmen try making peace through economic cooperation
Next in Middle East and Africa
X
The UN and the World Bank The UN and the World Bank
Rare cooperation
The men heading two world bodies speak more than just the same language
Next in Middle East and Africa
X
Syrias war and diplomacy Syrias war and diplomacy
Argument and arms
Everyone threatens to up the military ante in the runup to a conference
Next in Middle East and Africa
X
Syrias political opposition Syrias political opposition
Disarray
The opposition is as divided as ever
Next in Europe
X
Putins Russia Putins Russia
Repression ahead
Vladimir Putins crackdown on opponents protesters and activist groups may be
a sign of fragility
Next in Europe
X
Swedens riots Swedens riots
A blazing surprise
A Scandinavian idyll is disrupted by arson and unrest
Next in Europe
X
Italys government Italys government
Let off
Some good economic and political news for Enrico Letta
Next in Europe
X
Frances finance minister Frances finance minister
Moscovici vidi vici
Pierre Moscovici wins more friends in Brussels and Berlin than at home
Next in Europe
X
German education and training German education and training
Ein neuer Deal
Germanys vaunted dualeducation system is its latest export hit
Next in Europe
X
Alcohol in Turkey Alcohol in Turkey
Not so good for you
The mildly Islamist government brings in tough alcohol restrictions
Next in Europe
X
Charlemagne Charlemagne
Milda or Europa
Why some countries still want to join the euro
Next in Britain
X
Politics and the young Politics and the young
Generation Boris
Britains youth are not just more liberal than their elders They are also
more liberal than any
Next in Britain
X
The gnome revolt The gnome revolt
Little and large
Next in Britain
X
Tackling terrorism Tackling terrorism
Reaction time
The government is rethinking its antiterror policynot too cleverly
Next in Britain
X
NHS tourism NHS tourism
Freeforall
Doctors fear that health tourism in the NHS is growing
Next in Britain
X
Ethnic minorities Ethnic minorities
Suburban dreams
Sometimes explicitly Indians are following Jews out of London
Next in Britain
X
Policing Scotland Policing Scotland
One force to rule them all
While England argues about police reform Scotland does it
Next in Britain
X
Correction English Democrats
Next in Britain
X
Bagehot Bagehot
Puffing hard in the global race
The Tories have found their theme Sadly it is not changing how they govern
Next in International
X
Citizen journalism Citizen journalism
Foreign correspondents
Amateur journalists create jobs for professional ones
Next in International
X
Cigarettes Cigarettes
No butts
Some countries refuse to kick the habit
Next in International
X
Internet campaigning Internet campaigning
Any requests
Onlinepetitions sites grow richer and more popular
Next in Technology Quarterly
X
Monitor Monitor
Stuck to the ground by red tape
Space technology Dozens of firms want to commercialise space in various ways
Bureaucracy not just
Next in Technology Quarterly
X
Monitor Monitor
Concrete heal thyself
Civil engineering A building material that can perform running repairs on
itself fixing small
Next in Technology Quarterly
X
Monitor Monitor
A better waterworks
Medical technology Artificial kidneys are getting closer to becoming a
clinical reality thanks to
Next in Technology Quarterly
X
Monitor Monitor
Allo allo
Desalination A useful application may have been found for graphene improving
access to fresh water
Next in Technology Quarterly
X
Monitor Monitor
Zapping mosquitoes and corruption
Technology and government How the clever use of mobile phones is helping to
improve government
Next in Technology Quarterly
X
Monitor Monitor
Expanding care
Medical technology A new technique aims to prevent blood loss and save lives
by using a rapidly
Next in Technology Quarterly
X
Difference engine Difference engine
Everlasting light
Lighting technology There is a light that never goes outit just gradually
dims over many years
Next in Technology Quarterly
X
Reinventing the train Reinventing the train
Ideas coming down the track
Transport New train technologies are less visible and spread less quickly
than improvements to cars
Next in Technology Quarterly
X
Hypersonic missiles Hypersonic missiles
Speed is the new stealth
Hypersonic weapons Building vehicles that fly at five times the speed of
sound is amazingly hard
Next in Technology Quarterly
X
Inside story Inside story
In praise of celestial mechanics
Space technology Fixing an unmanned spacecraft that is thousands or millions
of miles away sounds
Next in Technology Quarterly
X
Microphones as sensors Microphones as sensors
Teaching old microphones new tricks
Sensor technology Microphones are designed to capture sound But they turn
out to be able to
Next in Business
X
Online labour exchanges Online labour exchanges
The workforce in the cloud
Talent exchanges on the web are starting to transform the world of work
Next in Business
X
The server market The server market
Shifting sands
Upheaval at the less visible end of the computer industry
Next in Business
X
Mexican housebuilders Mexican housebuilders
Dropping a brick
Changing government policies have plunged housebuilders into a crisis
Next in Business
X
Smithfield Foods Smithfield Foods
Pigs will fly
A Chinese buyer for Americas biggest pork producer
Next in Business
X
Murugappa Group Murugappa Group
The harder path
A rare south Indian business house does things the difficult way
Next in Business
X
Valeant Valeant
Shop til you drop
An acquisitive pharmaceutical company announces its biggest deal yet
Next in Business
X
Schumpeter Schumpeter
Boomerang bosses
When retired chiefs make a comeback their return is often less than triumphant
Next in Finance and economics
X
Chinas shadow banks Chinas shadow banks
The credit kulaks
The growth in wealthmanagement products reflects deeper financial distortions
Next in Finance and economics
X
Banca Etica Banca Etica
Ethical banking in Italy
A bank that takes its name seriously
Next in Finance and economics
X
European monetary policy European monetary policy
The negative option
Would charging banks that leave funds at the central bank help or hurt
Next in Finance and economics
X
Digital money Digital money
Taking a liberty
Life for virtual moneylaunderers is getting harder
Next in Finance and economics
X
Buttonwood Buttonwood
A great migration
Spain needs its young people to create new businesses
Next in Finance and economics
X
Japans equity and bond markets Japans equity and bond markets
Shocking
Volatile bond yields may spell trouble for Abenomics
Next in Finance and economics
X
The austerity debate The austerity debate
Dismal pugilists
Mudslinging between economists is a distraction from the real issues
Next in Finance and economics
X
Crowdfunding in America Crowdfunding in America
End of the peer show
Peertopeer lending needs a new name
Next in Finance and economics
X
Free exchange Free exchange
Macro control micro problems
History shows the limits of macroprudential policy in curbing dangerous risk
taking
Next in Science and technology
X
Martian space flight Martian space flight
Red dreams
Mars has always been ShangriLa for space buffs Two new private missions show
that its lure is as
Next in Science and technology
X
Arctic ecology Arctic ecology
Sacred geese
A diet of goose flesh may help polar bears survive global warming
Next in Books and arts
X
The rise of Spanish The rise of Spanish
Hats off
Spanish has more native speakers than any language other than Mandarin Yet
its success could not
Next in Books and arts
X
The Galleon insidertrading case The Galleon insidertrading case
Ship of knaves
Rajat Gupta had everything Why did he blow it
Next in Books and arts
X
John Hay John Hay
A statesman for all seasons
A valuable reassessment of an underestimated politician and diplomat
Next in Books and arts
X
Capitalism in America Capitalism in America
About turn
Ronald Reagans former budget director on the ills of American capitalism
Next in Books and arts
X
The end of the Soviet Union The end of the Soviet Union
Walking dead
The cruelty of the last years of the Soviet Union
Next in Books and arts
X
The Cannes film festival The Cannes film festival
Subtle story
Abdellatif Kechiches film La vie dAdèle carries off the top prize
Next in Obituary
X
Heinrich Rohrer
Heinrich Rohrer father of nanotechnology died on May 16th aged 79
Next in Economic and financial indicators
X
Output prices and jobs
Next in Economic and financial indicators
X
Trade exchange rates budget balances and interest rates
Next in Economic and financial indicators
X
The Economist commodityprice index
Next in Economic and financial indicators
X
Household debt
Next in Economic and financial indicators
X
Markets
Print edition
X
Jun 1st 2013
Jun 8th 2013
Jun 15th 2013
Jun 22nd 2013
From the print edition Jun 1st 2013
Comment 3
Timekeeper reading list
Email
Reprints  permissions
Print
