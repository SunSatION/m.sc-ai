

# Main Page

From Wikipedia, the free encyclopedia

Jump to: navigation, search

|

Welcome to Wikipedia,

the free encyclopedia that anyone can edit.

5,041,215 articles in English  
  
---  
  
  * Arts
  * Biography
  * Geography
|

  * History
  * Mathematics
  * Science
|

  * Society
  * Technology
  * **All portals**  
|

## From today's featured article  
  
---  
  
1875 Atlantic and Great Western Railroad depot

**Kent** is the largest city in Portage County in the U.S. state of Ohio. It is part of the Akron Metropolitan Statistical Area and the larger Cleveland–Akron–Canton Combined Statistical Area. The population was 28,904 in the 2010 Census and slightly higher in the 2014 estimate. Part of the Connecticut Western Reserve, it was settled in 1805 as a mill town along the Cuyahoga River and later named Franklin Mills. In the 1830s and 1840s, the village was on the route of the Pennsylvania and Ohio Canal. Franklin Mills was an active stop on the Underground Railroad before the Civil War. The city was renamed in 1864 for Marvin Kent, who secured the maintenance yards of the Atlantic and Great Western Railroad _(depot pictured)_ for Franklin Mills. Today Kent is a college town best known as the home of the main campus of Kent State University, founded in 1910, and as the site of the 1970 Kent State shootings. While historically a manufacturing center, the city's largest economic sector is now education. Many Kentites and Kent State alumni have risen to prominence in business, sports, and the arts. (**Full article...**)

  * Recently featured: 

    * Andrew Johnston (singer)
    * "Sisters at Heart"
    * William Wurtenburg

  * **Archive**
  * **By email**
  * **More featured articles...**  
  
## Did you know...  
  
The two summits of Nevado Tres Cruces

  * ... that **the sixth highest mountain** _(pictured)_ in South America is a volcano last active 28,000 years ago that might erupt again?
  * ... that cell biologist **Margaret Reed Lewis** may have been the first person to successfully grow mammalian tissue _in vitro_?
  * ... that David Leitch left his partner Chad Stahelski's _John Wick 2_ in order to direct _**The Coldest City**_?
  * ... that **Ladislaus IV of Hungary** abducted his sister, Elizabeth, from a monastery to give her in marriage to a Czech lord, Zavis of Falkenstein?
  * ... that **Lake Milh** in Iraq, a wetland of international importance, is fed by water from Lake Habbaniyah which comes from the Euphrates?
  * ... that **Jane Fraser** has been promoted to four CEO posts at Citigroup since joining in 2004?
  * ... that _**Rakastava**_ (_The Lover_) was first a song cycle for men's chorus by Jean Sibelius which he transformed to a suite for string orchestra, percussion and triangle?
  * ... that it cost the **son of Turkey's president** $20 million to get _Pretty_?

  * **Recently improved articles**
  * **Start a new article**
  * **Nominate an article**  
  
|  |

## In the news  
  
---  
  
Falcon 9 rocket's first stage on the landing pad

  * After launching a Falcon 9 rocket, SpaceX **successfully lands** the vehicle's first stage _(pictured)_ in Cape Canaveral.
  * Sepp Blatter and Michel Platini, the suspended presidents of FIFA and UEFA respectively, are banned from association football for eight years amid **a corruption case**.
  * More than 70 people are missing after **a landslide** in Shenzhen, China.
  * In the **Spanish general election**, the ruling People's Party, led by Mariano Rajoy, loses one third of its seats, but remains the largest single party.
  * Deep coal mining **ceases** in the United Kingdom with the closure of **Kellingley Colliery**.
  * **Ongoing events**: 

    * Syrian Civil War
    * European migrant crisis

  * **Recent deaths**: 

    * Jimmy Hill
    * Kurt Masur  
  
## On this day...  
  
**December 27**

Ignacy Jan Paderewski

  * 1657 – Citizens of New Netherland presented the **Flushing Remonstrance** to Director-General Peter Stuyvesant, requesting an exemption to his ban on Quaker worship.
  * 1845 – John L. O'Sullivan, in his newspaper the _New York Morning News_, argued that the United States had the right to claim the entire Oregon Country "by the right of our **manifest destiny**".
  * 1918 – A public speech by famed Polish pianist **Ignacy Jan Paderewski** _(pictured)_ in Poznań sparked the Greater Poland Uprising against Germany.
  * 1979 – Soviet war in Afghanistan: Soviet troops stormed Tajbeg Palace outside Kabul and killed Afghan President **Hafizullah Amin** and his 100–150 elite guards.
  * 2008 – In response to rocket attacks from Palestinian armed groups, Israel launched a surprise attack against the Gaza Strip, opening the three-week **Gaza War**.
  * More anniversaries: 

    * December 26
    * **December 27**
    * December 28

  * **Archive**
  * **By email**
  * **List of historical anniversaries**

  * Current date: December 27, 2015 (UTC)
  * Reload this page  
  
|

## Today's featured picture  
  
---  
  
|  |

The **wandering albatross** (_Diomedea exulans_) is a large seabird from the
albatross family which is found in the seas around Antarctica. The first
albatross to be described (though it was long grouped with Tristan and the
Antipodean albatrosses), the wandering albatross is the largest member of its
genus and one of the largest birds in the world.

Photograph: JJ Harrison

  * Recently featured: 

    * Bixby Creek Bridge
    * Dominostein
    * _Paris Street; Rainy Day_

  * **Archive**
  * **More featured pictures...**  
  
---|---  
  
## Other areas of Wikipedia

  * **Community portal** – Bulletin board, projects, resources and activities covering a wide range of Wikipedia areas.
  * **Help desk** – Ask questions about using Wikipedia.
  * **Local embassy** – For Wikipedia-related communication in languages other than English.
  * **Reference desk** – Serving as virtual librarians, Wikipedia volunteers tackle your questions on a wide range of subjects.
  * **Site news** – Announcements, updates, articles and press releases on Wikipedia and the Wikimedia Foundation.
  * **Village pump** – For discussions about Wikipedia itself, including areas for technical issues and policies.

## Wikipedia's sister projects

Wikipedia is hosted by the Wikimedia Foundation, a non-profit organization
that also hosts a range of other projects:

| **Commons**  
Free media repository |  | **MediaWiki**  
Wiki software development |  | **Meta-Wiki**  
Wikimedia project coordination  
---|---|---|---|---|---  
| **Wikibooks**  
Free textbooks and manuals |  | **Wikidata**  
Free knowledge base |  | **Wikinews**  
Free-content news  
| **Wikiquote**  
Collection of quotations |  | **Wikisource**  
Free-content library |  | **Wikispecies**  
Directory of species  
| **Wikiversity**  
Free learning materials and activities |  | **Wikivoyage**  
Free travel guide |  | **Wiktionary**  
Dictionary and thesaurus  
  
## Wikipedia languages

This Wikipedia is written in English. Started in 2001 (2001), it currently
contains 5,041,215 articles. Many other Wikipedias are available; some of the
largest are listed below.

  * More than 1,000,000 articles: 

    * Deutsch
    * Español
    * Français
    * Italiano
    * Nederlands
    * Polski
    * Русский
    * Svenska
    * Tiếng Việt

  * More than 250,000 articles: 

    * العربية
    * Bahasa Indonesia
    * Bahasa Melayu
    * Català
    * Čeština
    * فارسی
    * 한국어
    * Magyar
    * 日本語
    * Norsk bokmål
    * Português
    * Română
    * Srpski / српски
    * Srpskohrvatski / српскохрватски
    * Suomi
    * Türkçe
    * Українська
    * 中文

  * More than 50,000 articles: 

    * Bosanski
    * Български
    * Dansk
    * Eesti
    * Ελληνικά
    * English (simple)
    * Esperanto
    * Euskara
    * Galego
    * עברית
    * Hrvatski
    * Latviešu
    * Lietuvių
    * Norsk nynorsk
    * Slovenčina
    * Slovenščina
    * ไทย

**Complete list of Wikipedias**

Retrieved from
"https://en.wikipedia.org/w/index.php?title=Main_Page&oldid=696846920"

## Navigation menu

### Personal tools

  * Not logged in
  * Talk
  * Contributions
  * Create account
  * Log in

### Namespaces

  * Main Page
  * Talk

###  Variants

### Views

  * Read
  * View source
  * View history

### More

###  Search

### Navigation

  * Main page
  * Contents
  * Featured content
  * Current events
  * Random article
  * Donate to Wikipedia
  * Wikipedia store

### Interaction

  * Help
  * About Wikipedia
  * Community portal
  * Recent changes
  * Contact page

### Tools

  * What links here
  * Related changes
  * Upload file
  * Special pages
  * Permanent link
  * Page information
  * Wikidata item
  * Cite this page

### Print/export

  * Create a book
  * Download as PDF
  * Printable version

### Languages

  * Simple English
  * العربية
  * Bahasa Indonesia
  * Bahasa Melayu
  * Bosanski
  * Български
  * Català
  * Čeština
  * Dansk
  * Deutsch
  * Eesti
  * Ελληνικά
  * Español
  * Esperanto
  * Euskara
  * فارسی
  * Français
  * Galego
  * 한국어
  * עברית
  * Hrvatski
  * Italiano
  * ქართული
  * Latviešu
  * Lietuvių
  * Magyar
  * Nederlands
  * 日本語
  * Norsk bokmål
  * Norsk nynorsk
  * Polski
  * Português
  * Română
  * Русский
  * Slovenčina
  * Slovenščina
  * Српски / srpski
  * Srpskohrvatski / српскохрватски
  * Suomi
  * Svenska
  * ไทย
  * Tiếng Việt
  * Türkçe
  * Українська
  * 中文
  *   * This page was last modified on 26 December 2015, at 10:03.
  * Text is available under the Creative Commons Attribution-ShareAlike License; additional terms may apply. By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization.
  * Privacy policy
  * About Wikipedia
  * Disclaimers
  * Contact Wikipedia
  * Developers
  * Mobile view
  *   * 
  *[EK]: Emirates
  *[e]: Edit this template
  *[v]: View this template
  *[t]: Discuss this template

