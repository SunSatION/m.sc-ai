The Dot-Guessing Game:
A “Fruit Fly” for Human Computation Research
John J. Hor ton
Harvard University
383 Pforzheimer Mail Center
56 Linnaean Street
Cambridge, Massachusetts 02138
john.joseph.hor ton@gmail.com
ABSTRACT
I propose a human computation task that has a number of
properties that make it useful for empirical research. The
task itself is simple: sub jects are asked to guess the number
of dots in an image. The task is useful because: (1) it is sim-
ple to generate examples; (2) it is a CAPTCHA; and (3) it
has an ob jective solution that allows us to ﬁnely grade per-
formances, yet sub jects cannot simply look up the answer.
To demonstrate its value, I conducted two experiments us-
ing the dot-guessing task. Across both experiments, I found
Electronic copy available at: http://ssrn.com/abstract=1600372
trinsically interesting, these organisms possess certain prop-
erties that make them especially attractive for research pur-
poses. I propose a dot-guessing game, where sub jects guess
the number of dots in a computer-generated image, as a po-
tential model task. In order to demonstrate its utility and
generate new HCOMP-relevant research, I used this task
in two experiments conducted on MTurk. The experiments
were designed to explore practical HCOMP/crowdsourcing
issues: the side-eﬀects of certain forms of elicitation, the
potential of using type-revealing contract choices to infer
ability and/or (justiﬁed) conﬁdence, and the relationships
among incentives, eﬀort, and quality.
1.3 Main Experimental Findings
In general, sub jects did remarkably well at the task, con-
Electronic copy available at: http://ssrn.com/abstract=1600372
> par(mar = c(0, 0, 0, 0))
> n = 500
> X = c(runif(n, 0, 1), runif(n, 0, 1))
> plot(X, axes = F, xlab = "", ylab = "")
Figure 1: Example dot-guessing game with generat-
ing R code.
3. EXPERIMENT A
In the ﬁrst experiment, 200 sub jects were recruited to com-
plete a single HIT. They each inspected a single image con-
taining 500 dots and oﬀered a guess. Before proﬀering a
guess, each sub ject ﬁrst answered whether they thought
the number of dots was greater than or less than a num-
ber X randomly drawn from a uniform distribution, X ∼
U [100, 1200].
3.1 Results
Overall, sub jects performed quite well, with the mean best-
ing most individual guesses once very bad outliers were re-
Figure 3: Distributions of log guesses with correct
answers indicated by vertical bars.
view up to seven images, each of which contained a diﬀer-
ent number of dots. Having multiple responses per sub ject
makes a multilevel model appropriate for analysis [4], and
all the regressions include individual-speciﬁc random eﬀects.
Sub jects were not truly randomly assigned to HITs, but
rather were randomly assigned a HIT from the pool of un-
Table 2: Eﬀects of ex post weighting
Image Dots Unadj.
Adj.
Chg.
1
310
344 (0.11)
318 (0.027)
+
2
456
529 (0.16)
485 (0.063)
+
3
854
941 (0.102)
855 (0.002)
+
4
1243
1267 (0.019)
1252 (0.007) +
5
2543
2366 (0.07)
2345 (0.078)
-
6
4000
4839 (0.21)
4648 (0.162) +
7
8200
9683 (0.181)
9473 (0.155) +
Notes: Estimates using optimal weighting function (s = .95, h =
0). The error rate, |est. − truth|/truth, is in parentheses. Esti-
mates do not include the sub jects’ ﬁrst tasks completed.
error rate. Instead, I computed the mean guess (weighted
and unweighted) without using any ﬁrst responses. Table 2
shows that, in general, adjusting the estimate leads to im-
provement in accuracy. In six out of seven cases, the mean
error of the adjusted estimate was lower than the unadjusted
estimate.
6. CONCLUSION
Figure 4: Relative performances (percentiles) on ini-
tial tasks versus error rates on subsequent tasks.
worse than females, although the eﬀect is not signiﬁcant
Eij = 0.09|{z}
·maleij + . . .
with image ﬁxed eﬀects and individual random eﬀects, giv-
ing R2 = 0.66 and N = 949.
0.13
5.
IMPROVING ESTIMATES
To improve accuracy in applications, one would presumably
try to give more weight to the output of better workers.
To demonstrate how this can be done with the dot-guessing
task, I weighted sub jects based on relative performance on
the ﬁrst task completed. For each sub ject who completed
more than one image, I computed their error on the ﬁrst
image and where the performance placed them in the distri-
bution of all performances for that image.
Figure 4 is a scatter plot of sub jects’ relative performances
on their respective initial tasks versus error rates for sub-
sequent tasks. A local kernel density estimate shows that
subsequent performance is fairly uniform up to about the
90th percentile, perhaps with some evidence of mean rever-
sion for very low quantiles, and that after the 90th percentile
performance is much worse, with many sub jects achieving
error rates of 200% or more.
To improve performance, I used a very simple weighting
method with just two parameters. I split the distribution of
errors at s, and then gave all sub jects who performed better
than s a weight of 1, and all who performed worse than s a
weight of h, with h < 1. Using a grid search, I found that the
mean error is minimized when s = .95 and h = 0. Because
the quantiles (and, hence, the parameters) were determined
using a sub ject’s ﬁrst image, using this weighting scheme for
all observations would lead to a mechanical reduction in the
[11] W. Mason and D. J. Watts. Financial incentives and
the ‘performance of crowds’. In Proc. ACM SIGKDD
Workshop on Human Computation (HCOMP), 2009.
[12] D. McFadden. The human side of mechanism design:
A tribute to Leo Hurwicz and Jean-Jacque Laﬀont.
Review of Economic Design, 13(1):77–100, 2009.
[13] R. Schubert, M. Brown, M. Gysler, and H. W.
Brachinger. Financial decision-making: Are women
really more risk-averse? The American Economic
Review, 89(2):381–385, 1999.
[14] V. S. Sheng, F. Provost, and P. G. Ipeirotis. Get
another label?: Improving data quality and data
mining using multiple, noisy labelers. Know ledge
Discovery and Data Minding 2008 (KDD-2008), 2008.
[15] R. Snow, B. O’Connor, D. Jurafsky, and A. Y. Ng.
Cheap and fast—but is it good?: Evaluating
non-expert annotations for natural language tasks.
Proceedings of the Conference on Empirical Methods
in Natural Language Processing (EMNLP 2008), 2008.
[16] L. Von Ahn and L. Dabbish. Labeling images with a
computer game. In Proceedings of the SIGCHI
conference on Human factors in computing systems,
pages 319–326. ACM, 2004.
[17] L. Von Ahn, B. Maurer, C. McMillen, D. Abraham,
and M. Blum. reCAPTCHA: Human-based character
recognition via web security measures. Science,
321(5895):1465, 2008.
[18] H. Wickham. ggplot2: An implementation of the
grammar of graphics. R package version 0.7, URL:
http://CRAN.R-project.org/package=ggplot2, 2008.
information).
6.2 Case for Empiricism
As more aspects of social life moves online, both the size of
collected observational data and the potential to conduct ex-
periments will continue to grow. The transition to a bench
science, where hypotheses can be quickly tested through ex-
perimentation, is bound to have positive eﬀects.
The case for empiricism is particularly strong in designing
mechanisms, as theory oﬀers little guidance about cogni-
tive demands on the mechanism, how contextual factors are
likely to inﬂuence sub jects, how boundedly rational workers
actually play, etc. To make practically useful mechanisms,
it is important to understand how they hold up when the
players are real people, prone to the biases and sub ject to
the limitations of real people [12].
7. ACKNOWLEDGMENTS
Thanks to the the Christakis Lab at Harvard Medical School
and the NSF-IGERT Multidisciplinary Program in Inequal-
ity & Social Policy for generous ﬁnancial support (Grant
No. 0333403). Thanks to Robin Yerkes Horton, Brendan
O’Connor, Carolyn Yerkes, and Richard Zeckhauser for very
helpful comments. All plots were made using ggplot2 [18]
and all multilevel models were ﬁt using lme4[1].
8. REFERENCES
[1] D. Bates and D. Sarkar. lme4: Linear mixed-eﬀects
models using S4 classes. URL http://CRAN.
R-project. org/package= lme4, R package version
0.999375-28, 2008.
[2] B. Biais, D. Hilton, K. Mazurier, and S. Pouget.
Judgemental overconﬁdence, self-monitoring, and
trading performance in an experimental ﬁnancial
market. Review of Economic Studies, pages 287–312,
2005.
[3] D. L. Chen and J. J. Horton. The wages of paycuts:
Evidence from a ﬁeld experiment. Working Paper,
2009.
[4] A. Gelman and J. Hill. Data analysis using regression
and multilevel/hierarchical models. Cambridge
University Press Cambridge, 2007.
[5] J. J. Horton and L. B. Chilton. The labor economics
of paid crowdsourcing. Proceedings of the 11th ACM
Conference on Electronic Commerce 2010
(forthcoming), 2010.
[6] J. J. Horton, D. Rand, and R. J. Zeckhauser. The
online laboratory: Conducting experiments in a real
labor market. SSRN eLibrary, 2010.
[7] B. A. Huberman, D. Romero, and F. Wu.
Crowdsourcing, attention and productivity. Journal of
Information Science (in press), 2009.
[8] M. Kearns, S. Suri, and N. Montfort. An experimental
study of the coloring problem on human sub ject
networks. Science, 313(5788):824, 2006.
[9] A. Kittur, E. H. Chi, and B. Suh. Crowdsourcing user
studies with mechanical turk. Proceedings of
Computer Human Interaction (CHI-2008), 2008.
[10] G. Little, L. B. Chilton, R. Miller, and M. Goldman.
Turkit: Tools for iterative tasks on mechanical turk.
Working Paper, MIT, 2009.
