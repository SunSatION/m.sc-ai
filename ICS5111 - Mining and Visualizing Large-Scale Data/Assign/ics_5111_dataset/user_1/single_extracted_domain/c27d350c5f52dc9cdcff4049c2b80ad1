<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" >
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Cache-Control" content="public" />
<meta http-equiv="Content-Language" content="en" />
<meta name="revised" content="2015-03-25" />
<meta name="author" content="International Studies &amp; Programs at Michigan State University" />
<meta name="description" content="This is the index page for the Asian Studies Center" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta name="keywords" content="msu, asia, asian, studies" />
<meta name="title" content="Asian Studies Center | Michigan State University - Welcome to the Asian Studies Center" />
<title>Asian Studies Center | Michigan State University - Welcome to the Asian Studies Center</title>
<link href="http://asia.isp.msu.edu/" rel="canonical" />
<link href="http://www.isp.msu.edu/includes/css/styles.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/includes/style.css" media="all" rel="stylesheet" type="text/css" />
<link href="/includes/print.css" media="print" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<!--[if IE 6]><style type="text/css"> body { behavior: url(/includes/csshover.htc) }</style><![endif]-->
<!--see /www/isp_common for documentation -->
<!--[if lte IE 6]>
	<script type="text/javascript" src="http://www.isp.msu.edu/isp_common/png/supersleight.js"></script>
<![endif]-->
    </head>
    <body class="front narrow-mode standard-links mobile">
   
        <!--sphider_noindex-->
<ul class="isp_top_ullist" style="height:0; width: 0; overflow: hidden; position:absolute;">
	<li><a href='/global/access.htm' accesskey='0'>Alt-0 for Accessibility and Access Key information page.</a></li>
	<li><a href='#pgcontent' accesskey='2'>Alt-2 to Skip to main content.</a></li>
	<li>Alt-3 to turn all Interactive Accessibility mode items on or back to their default.</li>
</ul>

 
        		  
		                <!-- START HEADER -->
                <div id="isp-header"> 
                    <div id="isp-header-width" class="fixed-header">
                        <div id="isp-header-links">
                            <div id="isp-header-msu">
                                <a href="http://msu.edu"><img src="http://www.isp.msu.edu/isp_common/images/msu-head.png" alt="Michigan State University" title="Michigan State University" id="msuintl" /></a>
                            </div>
                            <div id="isp-header-department">
                                                            <div id="tophead"><a href="http://www.isp.msu.edu"><img src="http://www.isp.msu.edu/isp_common/images/headers/isp-head.png" alt="International Studies and Programs" title="International Studies and Programs" /></a></div>
                                                                                         <div id="subhead"><a href="/"><img src="http://www.isp.msu.edu/isp_common/images/headers/asia.png" alt="Asian Studies Center" title="Asian Studies Center" /></a></div>
                                                        </div>
                           <div class="clear"></div>
                        </div>
                        <!-- menu -->
                    </div>
                                            <div id="menu"
                             class="fixed-header">
                            <ul><li><span class='head' ><a id="mnmenu"  href='/' title='Home page'>Home</a></span></li></ul><ul><li><span class='head' ><a  href='/academics/' title='Academics'>Academics</a></span><ul><li><a  href='/academics/programs.htm' title='Academic Programs'>Academic Programs</a></li><li><a  href='/academics/languages.htm' title='Languages'>Languages</a></li><li><a  href='/academics/undergraduate.htm' title='Undergraduate'>Undergraduate</a></li><li><a  href='/academics/graduate.htm' title='Graduate'>Graduate</a></li><li><a  href='/academics/teaching.htm' title='Teaching'>Teaching</a></li><li><a  href='/announcements/announcement1.htm' title='Fellowships and Scholarships'>Fellowships &amp; Scholarships</a></li><li><a  href='/resources/funding.htm' title='Funding'>Other Student Funding</a></li><li><a  href='/announcements/announcement6.htm' title='Featured Courses'>Featured Courses</a></li></ul></li></ul><ul><li><span class='head' ><a  href='/resources/' title='Resources'>Resources</a></span><ul><li><a  href='/resources/sp_grants.htm' title='Partnership Grants'>Partnership Grants</a></li><li><a  href='/resources/faculty_support.htm' title='Faculty Support'>Faculty Support</a></li><li><a  href='/resources/asia_councils.htm' title='Asia Councils'>Asia Councils</a></li><li><a  href='/resources/videos.htm' title='Videos'>Videos</a></li><li><a  href='/resources/links.htm' title='Links'>Links</a></li></ul></li></ul><ul><li><span class='head' ><a  href='/outreach/' title='Outreach'>Outreach</a></span><ul><li><a  href='/outreach/k12program.htm' title='K12 Programs'>K-12 Programs</a></li><li><a  href='/outreach/workshops.htm' title='Teacher Workshops'>Teacher Workshops</a></li><li><a  href='/outreach/lessons.htm' title='Teacher Donated Lessons'>Teacher Donated Lessons</a></li><li><a  href='/outreach/confucius_institute.htm' title='Confucius Institute'>Confucius Institute</a></li><li><a  href='/outreach/nepal.htm' title='Nepal Study Guide'>Nepal Study Guide</a></li><li><a  href='/outreach/vietnam.htm' title='Vietnam Study Guide'>Vietnam Study Guide</a></li><li><a  href='/outreach/windows.htm' title='Windows on Asia'>Windows on Asia</a></li><li><a  href='/gallery/' title='Photo Gallery'>Photo Gallery</a></li></ul></li></ul><ul><li><span class='head' ><a  href='/programs/' title='Programs'>Programs</a></span><ul><li><a  href='/programs/conferences.htm' title='Conferences'>Conferences</a></li><li><a  href='/programs/ceres.htm' title='Eurasian Studies'>Eurasian Studies</a></li><li><a  href='/programs/central_asia.htm' title='Central Asia'>Central Asia</a></li><li><a  href='/programs/casid.htm' title='International Development'>International Development</a></li><li><a  href='/programs/jcmu.htm' title='Japan Center'>Japan Center</a></li><li><a  href='/programs/ocp.htm' title='China Programs'>China Programs</a></li><li><a  href='/programs/clear.htm' title='Language Education'>Language Education</a></li><li><a  href='/programs/gencen.htm' title='Center for Gender in Global Context'>Center for Gender in Global Context</a></li><li><a  href='/programs/celta.htm' title='Language Teaching'>Language Teaching</a></li><li><a  href='/programs/arabic.htm' title='Arabic Flagship'>Arabic Flagship</a></li></ul></li></ul><ul><li><span class='head' ><a  href='/people/' title='People'>People</a></span><ul><li><a  href='/people/' title='Faculty'>Affiliated Faculty</a></li><li><a  href='/people/staff.htm' title='Staff'>Staff</a></li><li><a  href='/people/alumni_friends.htm' title='Alumni and Friends'>Alumni and Friends</a></li><li><a  href='/people/employment.htm' title='Employment'>Employment</a></li></ul></li></ul><ul><li><span class='head' ><a  href='/archives/' title='FAQs'>Archives</a></span></li></ul><ul><li><span class='head' ><a  href='/global/' title='Contact Us'>Contact</a></span><ul><li><a  href='/supportus.htm' title='Support Us'>Support Us</a></li></ul></li></ul>                            <div class="clear"></div>

                        </div>
                        <!--end Menu -->
                                        <div id="isp-header-search">
                        <div id="isp-header-access-icons">
                            <form method="post" action="/includes/formpost.htm" ><input type='hidden' name='global_postpage' value='/'/><ul id="accessIcons" class='no_tab_a'>
<li><button name='remember_settings' value='narrow-mode,standard' type='submit' title='Click to remember current site settings (cookies must be enabled). Visit Accesibility page to restore defaults'>Set</button></li>

<li><button name='page_width' value='wide-mode' type='submit' title='Click to switch between full browser width and narrow view.'>View</button></li>

<li><a href='mailto:?subject=Look at this link from the Asian Studies Center | International Studies and Programs Website&amp;body=HTTP%3A%2F%2Fasia.isp.msu.edu/' title='Click to e-mail a link to this page to a friend or colleague.'>Email</a></li>

<li><a href='/?pf=t' title='Click for a printer friendly page with content below.'>Print</a></li>

</ul><button style='position:absolute; margin-left: -10000px' accesskey='3' type='submit' name='interactive_links'>i</button>
<button style='position:absolute; margin-left: -10000px' accesskey='5' type='submit' name='bold_links'>i</button>
</form>                        </div>
					                        <div id="isp-header-search-box">
                                                            
	<form name= "ispsearch" id="ispsearch" action="/search.htm" method="get">
		<input type="hidden" name="search" value="1" />
		<input type="hidden" name="which" value="4" />
		<input type="hidden" name="department[]" value="asia" />
		<input type="text" name="query1" id="query1" accesskey="4" class="isp-header-query" title="Enter words to search for then click button to right." />
		<input type="image" src="http://www.isp.msu.edu/isp_common/images/search-small1.png" name="searchsubmit" id="isp-header-submit" value="" alt="Go" title="Click to search for entered words." />
	</form>
                        </div>
                    					</div>
					
                    <div id="isp-header-map">
                    	                        <img src="http://www.isp.msu.edu/isp_common/images/map-asia.png" alt="Small World Map" />
                    	                    </div>
                    <div class="clear"></div>

                </div>
                <div id="isp-header-shadow"></div>
             <!-- END HEADER -->
                                                <div id="body0">
                
                        <div id="page_content">
    <div id="innerContainer">
	<script type="text/javascript" src="/includes/scripts/jquery.js"></script>
	<script type="text/javascript" src="/includes/scripts/wowslider.js"></script>	
		<!-- Banner -->
		<div id="wowslider-container1">
			<div class="ws_images">
				<span><img src="/images/banner/new2015/1_China.jpg" alt="Great Wall, China" title="Great Wall, China" id="one"/></span>
				<span><img src="/images/banner/new2015/2_Japan.jpg" alt="Cherry Blossom, Japan" title="Cherry Blossom, Japan" id="two"/></span>
				<span><img src="/images/banner/new2015/3_Vietnam.jpg" alt="Rice Terraces, Vietnam" title="Rice Terraces, Vietnam" id="three"/></span>
				<span><img src="/images/banner/new2015/4_Nepal.jpg" alt="Mount Everest, Nepal" title="Mount Everest, Nepal" id="four"/></span>
				<span><img src="/images/banner/new2015/5_Myanmar.jpg" alt="Shwedagon Pagoda, Myanmar" title="Shwedagon Pagoda, Myanmar" id="five"/></span>
                <span><img src="/images/banner/new2015/7_Thailand.jpg" alt="Krabi Island, Thailand" title="Krabi Island, Thailand" id="six"/></span>
			</div><a href="#" class="ws_frame"></a>
		</div>
		</div>

		<script type="text/javascript" src="/includes/scripts/slider-script.js"></script>
			
		<div id="leftContent">
			<div class="padding">
			
				<!-- Events -->
				<div class="border shadow custom sidebar" id="events">
					<div id="events-1" class="widget">
						<h5>
							<a href="http://www.isp.msu.edu/media/rss/events/asianstudies/">
								<img src="/images/rss-icon.gif" alt="Subscribe to RSS!" border="0" style="width:16px; height:16px" />
							</a>
							<a href="/events/" style="color:#FFFFFF" title="Click to see more events">Events</a>
						</h5>
						<ul class="events-items-list">

        <li class="events-events-item">
           <div class="dateblock">
               <span class="month">Feb</span>
               <span class="day">15</span>
               <span class="year">2016</span>
           </div>

           <div class='events-list-content'>
                    <span class="events-list-title"><a href="/events/47744/film-premiere-hmong-memory-at-the-crossroads/">Film Premiere: Hmong Memory at the Crossroads</a></span>
                     <span class="events-time"><strong>Time:</strong> 
                    4:00 pm to 6:00 pm                    </span>
                    <span class="events-list-location"><strong>Location:</strong> Kellog Hotel and Conference Center Auditorium</span>
           </div>
           <div class="clear"></div>
        </li>
       <li class="events-events-item">
           <div class="dateblock">
               <span class="month">Mar</span>
               <span class="day">23</span>
               <span class="year">2016</span>
           </div>

           <div class='events-list-content'>
                    <span class="events-list-title"><a href="/events/47559/the-savage-land-and-bernstein-sings-america/">The Savage Land and Bernstein Sings America</a></span>
                     <span class="events-time"><strong>Time:</strong> 
                    7:30 pm to 9:00 pm                    </span>
                    <span class="events-list-location"><strong>Location:</strong> Fairchild Theatre, MSU Auditorium</span>
           </div>
           <div class="clear"></div>
        </li>
       <li class="events-events-item">
           <div class="dateblock">
               <span class="month">Mar</span>
               <span class="day">25</span>
               <span class="year">2016</span>
           </div>

           <div class='events-list-content'>
                    <span class="events-list-title"><a href="/events/47561/the-savage-land-and-bernstein-sings-america/">The Savage Land and Bernstein Sings America</a></span>
                     <span class="events-time"><strong>Time:</strong> 
                    8:00 pm to 10:00 pm                    </span>
                    <span class="events-list-location"><strong>Location:</strong> Fairchild Theatre, MSU Auditorium</span>
           </div>
           <div class="clear"></div>
        </li>
</ul>

 
						<div class="side-widget-more"><a href="/events/">Click for more events</a></div>
					</div>
				</div>
				<br/>

				<!-- News div -->
				<div class="border shadow custom sidebar" id = "news">
					<div id="news-1" class="widget">
						<h5>
							<a href="/news/" style="color:#FFFFFF" title="Click to see more news items">
							<img src="/images/news-icon.gif" style="width:16px;height:16px" alt="news" border="0"/> News
							</a>
						</h5>
							<ul class="news-items-list">
            <li class="news-news-item">
                    <div class="news-list-content">
                <span class="news-list-published">Tuesday, 15 Dec 15</span>
                                    <span class="news-list-headline"><a href="http://www.freep.com/story/opinion/contributors/2015/12/14/trumps-rhetoric-only-gives-into-isis/77109766/">Trump's Rhetoric Only Gives Into ISIS</a></span>
                            </div>
            <div class="clear"></div>
        </li>
            <li class="news-news-item">
                    <div class="news-list-content">
                <span class="news-list-published">Monday, 23 Nov 15</span>
                                    <span class="news-list-headline"><a href="http://msutoday.msu.edu/news/2015/msu-professor-named-honorary-citizen-of-singapore-1/">MSU PROFESSOR NAMED HONORARY CITIZEN OF SINGAPORE</a></span>
                            </div>
            <div class="clear"></div>
        </li>
            <li class="news-news-item">
                    <div class="news-list-content">
                <span class="news-list-published">Monday, 23 Nov 15</span>
                                    <span class="news-list-headline"><a href="http://www.lansingstatejournal.com/story/news/local/2015/11/19/syrian-refugees-lansing-resettle/75921630/?from=global&amp;sessionKey=&amp;autologin=">Syrian refugees controversy gets Lansing's attention</a></span>
                            </div>
            <div class="clear"></div>
        </li>
    </ul>

						<div class="side-widget-more"><a href="/news/">Click for more news</a></div>
					</div>
				</div>
				<br/>
		 
				<!-- Twitter
				<div id="tweets" class="border shadow custom sidebar">
					<li id="twitter-1" class="widget">
						<h5>
						<a href="http://twitter.com/asianstu" style="color:#FFFFFF">
						<img src="/images/twitter_24p.png" style="width:16px;height:16px" alt="news" border="0"/> Twitter
						</a>
					</h5>
											</li>
				</div>-->
				
			</div>
		</div>
		<div id="asianContent" class="shadow">
		<a id="pgcontent" name="pgcontent" tabindex="-1"></a>

<!--/sphider_noindex-->



<h1>Welcome to the Asian Studies Center</h1>



<!-- Real Body of Page is below here (need help changing above? on campus call 4-2142) -->

<br />
                    <p>
<a href="http://us3.campaign-archive2.com/home/?u=575b2ce7c4e7676e15735f3c8&amp;id=379a6493b1" target="_blank"><img style="float:left; margin-right:5px; border: 3px solid #FFFF00;" title="Asian Studies Newsletter" alt="cover of recent newsletter" src="/images/AsianStudiesNewsletter.png" /></a></p>
<br /><br /><br /><br />
<p><b>Please see our latest newsletter. Click on the image to the left to read it in the browser.</b></p>
<br /><br /><br /><br /><br />

					<p>The Asian Studies Center, named a National Undergraduate Resource Center (Title VI NRC) since 2000 by the U.S. Department of Education, directs one of the largest, most diverse programs of education about Asia in the Midwest. Unlike comparable programs, the Center is distinguished by its comprehensive attention to East, Central, South, Southeast, and West Asia in the design of its curriculum, focus of faculty research, and scope of outreach activities. Presently, the Center's nearly two hundred affiliated faculty represent disciplines ranging across the curriculum in teaching undergraduate and graduate students.</p>
                    <p>The Asian Studies Center began as Michigan State University's academic center for developing and coordinating Asia-related programs in 1962. During the Center's first decades, Asian Studies centered on students and faculty working in the East Asia regional area. From the 1960s to the 1980s, the Center's accomplishments received wide recognition and funding from the Social Science Research Council, the Luce Foundation, the Japan Foundation, the Korea Foundation, and other agencies.</p>
                    <p>In recent years, the Center has undergone a major transition as the curriculum, faculty research, exchange programs, and outreach activities have developed to embrace programs in places as diverse as India, Indonesia, Korea, and Nepal. Emblematic of this development is the growth of MSU's overseas linkages to dozens of locations throughout Asia today.</p>
					<p>The Asian Studies Center's shift toward an all-Asia emphasis reflects the university's growing internationalization. Two-thirds of MSU's foreign students and over half of the university's 1200 foreign scholars come from Asia and nearly 2,000 undergraduates are Asian-Pacific Americans. The changing domestic demographics and the increase in students and scholars from Asia have created demands for an Asian Studies curriculum relevant to new needs and experiences reflecting the university's genuinely multicultural quality.</p>        <p id="pdfNote" style="visibility:hidden; height: 0px;">*<a href="http://get.adobe.com/reader/" class="externalSite" target="_blank" title="Go to external site">Adobe Acrobat Reader <span class='noprint'><img class='nw_image' alt='Opens in new window' title='Opens in new window' src='http://www.isp.msu.edu/isp_common/images/nw.gif' width='13' height='10' /></span></a> is required to read PDF documents.</p>		</div>

            <div id="rightContentAnnouncement" class="shadow">
                <div id="rightContentAnnouncementHeader" style="margin-left:10px; margin-top:10px; margin-bottom:5px">
                    <b>PROMOTION:</b>
                </div>
                				<div id="announcements"> 
                	 
                   <div style="width: 50%; float: left;">
                   <div id="leftPromo" class="announcement"  style="background-image: url(/resources/FLAS_promo.png); text-align:left;" onclick ="window.open('/resources/FLAS flyer (2).pdf','_blank')"> 
 					<p style="font-weight:bold; font-size: 200%; color:black; margin-bottom: 20px;" >
         Foreign Language and Area Studies Fellowship FLAS</p>
         <p style="font-weight:bold; font-size: 100%; color:black; margin-bottom: 20px;" >For the study of Asia and Asian Languages
Summer 2016 and Academic Year (AY) 2016 - 2017</p>
                      <p style="font-weight:bold; font-size: 100%; color:black; margin-bottom: 25px;" >
The Asian Studies Center awards graduate and undergraduate fellowships under the Foreign Language and Area Studies (FLAS) Fellowship program of the U.S. Department of Education.<br/> </p>
<p style="font-weight:bold; font-size: 150%; color:blue; margin-bottom: 40px;" > Applications due February 5, 2016</p>

<p style="color: black; font-size: 100%;"><a style="color: black; font-size: 100%;" href="/resources/FLAS flyer (2).pdf" target="_blank">
                        Click here for more information</a></p>  
                   </div>
                   </div>

                    <div style="width: 49%; float: right;">
                    <div id="rightPromo" class="announcement" style="background-image: url(/resources/SCLEE_promo.png); text-align:left;" onclick ="window.open('/resources/SC_LeeFlyer 2016_2.pdf','_blank')">
 					<p style="font-weight:bold; font-size: 200%; color:black; margin-bottom: 20px;" >
                     2016 <br />The S.C. Lee<br /> Scholarship &amp; <br />Paper Competition</p>
                      <p style="font-weight:bold; font-size: 100%; color:black; margin-bottom: 15px;" >
In honor of the late Professor <br />Shao-Chang Lee, an advocate of <br />U.S.-East Asia relations, <br />the S.C. Lee Endowment sponsors students<br /> with outstanding accomplishments<br /> in Asian studies by awarding scholarships and paper prizes.</p>
                      <p style="font-weight:bold; font-size: 180%; color:black; margin-bottom: 5px;" > Application deadline: <br/>
January 29, 2016</p>

                
                  
                       <p style="color: black; font-size: 100%;"><a style="color: black; font-size: 100%;" href="/resources/SC_LeeFlyer 2016_2.pdf" target="_blank">
                        Click here for more information</a></p>    
                    </div>
                    </div>
            </div>  <!-- rightContentAnnouncement -->
<script type="text/javascript">
	<!--
var minHigh = 0;
	leftBox = document.getElementById("leftPromo");
	rightBox = document.getElementById("rightPromo");
	leftHigh = leftBox.offsetHeight;
	rightHigh = rightBox.offsetHeight;
	minHigh = leftHigh > rightHigh ? leftHigh : rightHigh;
	leftBox.style.height =  minHigh + 'px';
	rightBox.style.height = minHigh + 'px';
	//-->
</script>

</div>    
</div><!--end page_content-->
<div style="clear:both;"></div>
<div id="social_sidebar">
	<div style="position:relative">
		<a target="_blank" href="https://www.facebook.com/pages/Asian-Studies-Center/501019605190">
		<img src="/images/fbk_24p.png" alt="Follow us on Facebook!" title="Follow us on Facebook!" />
		</a>
        <!--<a target="_blank" href="https://twitter.com/asianstu">
		<img src="/images/twitter_24p.png" alt="Follow us on Twitter!" title="Follow us on Twitter!" />
		</a>-->
        <a target="_blank" href="mailto:?subject=Look at this link from the Asian Studies Website&amp;body=HTTP%3A%2F%2Fasia.isp.msu.edu/">
		<img src="/images/mail_icon_24p.png" alt="Share as mail!" title="Share as mail!" />
		</a>
        <a target="_blank" href="http://www.isp.msu.edu/media/rss/events/asianstudies/">
		<img src="/images/rss_icon_24p.png" alt="Subscribe to RSS!" title="Subscribe to RSS!" />
		</a>
        <a target="_blank" href="/supportus.htm">
		<img src="/images/dollar_24p.png" alt="Support us!" title="Support us!" />
		</a>

	</div>
</div>
<div id="asiaFooter">  
    <div id="footLeft">
		<a href="http://asia.isp.msu.edu"><img align="left" style="margin-right: 8px;" src="/images/ASN_Logo_green.jpg" alt="Asian Studies Center Logo" class="imgLeft" /></a>
		<p style="margin-bottm:-10px;line-height:normal;">Asian Studies Center<br />
			International Center<br />
			427 N. Shaw Lane, Room 301<br />
			East Lansing, MI 48824<br />
			Tel: 517.353.1680<br />
			Fax: 517.432.2659<br />
		</p>
	</div>
    <div id="footRight">
		<ul id="ftmenu">
			<li><a id="preftmenu" name="preftmenu" href="/global/contact.htm">Contact</a></li>
			<li> &nbsp;| <a href="/global/access.htm">Accessibility</a></li>
			<li> &nbsp;| <a href="/global/priv.htm">Privacy</a></li>
			<li> &nbsp;| <a href="/global/sitemap.htm">Site&nbsp;Map</a></li>
		</ul>
		<p><a href="http://www.msu.edu"><img src="http://www.isp.msu.edu/isp_common/images/isp_footer_aktl.png" alt="Michigan State University: Advancing Knowlege Transforming Lives." /></a></p>
		<p style="margin-top:-10px;line-height:normal;">&copy; 2015 Michigan State University <a href="http://trustees.msu.edu/" title="Go to the Board of Trustees Site">Board of Trustees</a>. East Lansing, MI 48824
		MSU is an affirmative-action, equal opportunity employer</p>


    
	</div>
	<div style="clear:both;"></div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#msuintl').attr('src','http://www.isp.msu.edu/isp_common/images/msu-head.png?294').load(function(){
		$('#query1').css({
			'background-color': 'transparent'
		});
		$('#isp-header-submit').css('background-color','transparent').attr('value','');
	});
	if(window.XMLHttpRequest) { // !ie6
		$('#isp-header-submit').mouseover(function(){
			$(this).attr('src','http://www.isp.msu.edu/isp_common/images/search-small2.png');
		}).mouseout(function(){
			
			$(this).attr('src','http://www.isp.msu.edu/isp_common/images/search-small1.png');
		});
	};
});
	function showBubble(str){;
	document.getElementById(str).style.display='block';
	};
	function hideBubble(str){
;
	document.getElementById(str).style.display='none';
	};
</script> 
                        			
				
</div> <!--end body0 -->


<script type="text/javascript"> function ispaccess() { window.open("http://ispadmin.isp.msu.edu/"); return false;}</script><div ondblclick="ispaccess(); return false;" id="ispaccess" style="position:absolute; top:0px; height:10px; width:100%; display:block;z-index:9999;"></div>
<!-- Access Keys -->
<!--sphider_noindex-->
<ul class="isp_top_ullist" style="height:0; width: 0; overflow: hidden; position:absolute;">
	<li><a href='/' accesskey='1'>Alt-1 for Home Page</a></li>
	<li><a href='#query1'>Alt-4 for Search box</a></li>
	<li><a href='/global/sitemap.htm' accesskey='6'>Alt-6 for Site Map; hierarchical listing of the main pages of the site</a></li>
	<li><a href='#mnmenu' accesskey='7'>Alt-7 to move to the Main Menu.</a></li>
	<li><a href='/global/contact.htm' accesskey='8'>Alt-8 for Contact Us</a></li>
	<li><a href='#preftmenu' accesskey='9'>Alt-9 to move to the Footer Menu.</a></li>
</ul>


<!--/sphider_noindex-->


 
        <script type="text/javascript">
    //<![CDATA[

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-30417231-2']);
    _gaq.push(['_trackPageview']);
    
    (function(){
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();


    //]]>
</script>
        

        
    </body>
</html>
