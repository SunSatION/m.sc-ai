

# Main Page

From Wikipedia, the free encyclopedia

Jump to: navigation, search

|

Welcome to Wikipedia,

the free encyclopedia that anyone can edit.

5,040,639 articles in English  
  
---  
  
  * Arts
  * Biography
  * Geography
|

  * History
  * Mathematics
  * Science
|

  * Society
  * Technology
  * **All portals**  
|

## From today's featured article  
  
---  
  
**Andrew Johnston** (born 1994) is a British singer who rose to fame when he appeared as a boy soprano on the second series of the British television talent show _Britain's Got Talent_ in 2008. He sang "Pie Jesu" from Andrew Lloyd Webber's _Requiem_ in the finals. Although he did not win, he received a contract to record on the SyCo Music label owned by the _Britain's Got Talent_ judge Simon Cowell. Johnston's debut album, _One Voice_, was released in September of the same year, and reached number four on the UK Albums Chart. Johnston was born in Dumfries, Scotland, and grew up in Carlisle. He became head chorister at Carlisle Cathedral, and was bullied at school for his love of classical music. While some journalists have argued that _Britain's Got Talent_ producers exaggerated Johnston's rough background, others have hailed his story as inspirational. In 2009, he graduated from Trinity School. He sings in the National Youth Choir as a baritone and studies full-time at the Royal Northern College of Music. (**Full article...**) 

  * Recently featured: 

    * "Sisters at Heart"
    * William Wurtenburg
    * Tropical Storm Edouard (2002)

  * **Archive**
  * **By email**
  * **More featured articles...**  
  
## Did you know...  
  
_The Saw Mill_, tapestry woven by Else Halling, in the Oslo City Hall

  * ... that **Else Halling** contributed decorations to several public buildings in Oslo, including the Royal Palace, the Akershus Castle, Stortinget, and the City Hall _(tapestry pictured)_?
  * ... that Villanova won the **1985 NCAA Men's Division I Basketball Championship Game** despite taking the fewest shots of any team in a men's Final Four game?
  * ... that _**Cento vergilianus de laudibus Christi**_, a Latin poem by Faltonia Betitia Proba, takes lines from the works of the Roman poet Virgil and rearranges them to be about Jesus?
  * ... that the origin of **clementine cake** may be roughly based upon an orange cake developed by the Sephardi Jews?
  * ... that in 1895 **Sister Krucifiksa** founded a school dedicated to the Sacred Heart, which accepted children regardless of their religious affiliation or ethnic background?
  * ... that Pope Pius IX ensured that the American College would get the property of **a former convent** where he used to serve Mass as a boy?
  * ... that **Thomas Maddock** started the American indoor toilet industry through his invention?
  * ... that although not her team's designated sprinter, Giorgia Bronzini won the **2015 Tour of Chongming Island World Cup** in a bunch sprint?

  * **Recently improved articles**
  * **Start a new article**
  * **Nominate an article**  
  
|  |

## In the news  
  
---  
  
Falcon 9 rocket's first stage on the landing pad

  * After launching a Falcon 9 rocket, SpaceX **successfully lands** the vehicle's first stage _(pictured)_ in Cape Canaveral.
  * Sepp Blatter and Michel Platini, the suspended presidents of FIFA and UEFA respectively, are banned from association football for eight years amid **a corruption case**.
  * More than 70 people are missing after **a landslide** in Shenzhen, China.
  * In the **Spanish general election**, the ruling People's Party, led by Mariano Rajoy, loses one third of its seats, but remains the largest single party.
  * Deep coal mining **ceases** in the United Kingdom with the closure of **Kellingley Colliery**.
  * **Ongoing events**: 

    * Syrian Civil War
    * European migrant crisis

  * **Recent deaths**: 

    * Jimmy Hill
    * Kurt Masur  
  
## On this day...  
  
**December 26**: **Boxing Day** in Commonwealth countries; **St. Stephen's Day** (Western Christianity); **Kwanzaa** begins in Canada and the United States

Pierre and Marie Curie

  * 1606 – The first recorded performance of the play _**King Lear**_, a tragedy by William Shakespeare based on the legendary King Lear of the Britons, was held.
  * 1825 – Imperial Russian Army officers **led about 3,000 soldiers in a protest** against Nicholas I's assumption of the throne after his elder brother Constantine removed himself from the line of succession.
  * 1898 – At the French Academy of Sciences, physicists Pierre and Marie Curie _(both pictured)_ announced the discovery of a new element, naming it **radium**.
  * 1919 – American baseball player **Babe Ruth** was sold by the Boston Red Sox to their rivals, the New York Yankees, starting the 84-year-long **Curse of the Bambino**.
  * 1991 – The Supreme Soviet officially dissolved itself, completing the **dissolution of the Soviet Union**.
  * More anniversaries: 

    * December 25
    * **December 26**
    * December 27

  * **Archive**
  * **By email**
  * **List of historical anniversaries**

  * Current date: December 26, 2015 (UTC)
  * Reload this page  
  
|

## Today's featured picture  
  
---  
  
|  
---  
  
The **Bixby Creek Bridge** is a reinforced concrete open-spandrel arch bridge
in Big Sur, California. Prior to its opening in 1932, local residents were
virtually cut off during winter as the old coast road, running as far as 11
miles (18 km) inland, was often impassable. At its completion, the bridge was,
at 320 feet (98 m), the longest concrete arch span on the California State
Highway System. It is one of the tallest single-span concrete bridges in the
world.

Photograph: David Iliff

  * Recently featured: 

    * Dominostein
    * _Paris Street; Rainy Day_
    * Kingdom of Armenia (antiquity)

  * **Archive**
  * **More featured pictures...**  
  
## Other areas of Wikipedia

  * **Community portal** – Bulletin board, projects, resources and activities covering a wide range of Wikipedia areas.
  * **Help desk** – Ask questions about using Wikipedia.
  * **Local embassy** – For Wikipedia-related communication in languages other than English.
  * **Reference desk** – Serving as virtual librarians, Wikipedia volunteers tackle your questions on a wide range of subjects.
  * **Site news** – Announcements, updates, articles and press releases on Wikipedia and the Wikimedia Foundation.
  * **Village pump** – For discussions about Wikipedia itself, including areas for technical issues and policies.

## Wikipedia's sister projects

Wikipedia is hosted by the Wikimedia Foundation, a non-profit organization
that also hosts a range of other projects:

| **Commons**  
Free media repository |  | **MediaWiki**  
Wiki software development |  | **Meta-Wiki**  
Wikimedia project coordination  
---|---|---|---|---|---  
| **Wikibooks**  
Free textbooks and manuals |  | **Wikidata**  
Free knowledge base |  | **Wikinews**  
Free-content news  
| **Wikiquote**  
Collection of quotations |  | **Wikisource**  
Free-content library |  | **Wikispecies**  
Directory of species  
| **Wikiversity**  
Free learning materials and activities |  | **Wikivoyage**  
Free travel guide |  | **Wiktionary**  
Dictionary and thesaurus  
  
## Wikipedia languages

This Wikipedia is written in English. Started in 2001 (2001), it currently
contains 5,040,639 articles. Many other Wikipedias are available; some of the
largest are listed below.

  * More than 1,000,000 articles: 

    * Deutsch
    * Español
    * Français
    * Italiano
    * Nederlands
    * Polski
    * Русский
    * Svenska
    * Tiếng Việt

  * More than 250,000 articles: 

    * العربية
    * Bahasa Indonesia
    * Bahasa Melayu
    * Català
    * Čeština
    * فارسی
    * 한국어
    * Magyar
    * 日本語
    * Norsk bokmål
    * Português
    * Română
    * Srpski / српски
    * Srpskohrvatski / српскохрватски
    * Suomi
    * Türkçe
    * Українська
    * 中文

  * More than 50,000 articles: 

    * Bosanski
    * Български
    * Dansk
    * Eesti
    * Ελληνικά
    * English (simple)
    * Esperanto
    * Euskara
    * Galego
    * עברית
    * Hrvatski
    * Latviešu
    * Lietuvių
    * Norsk nynorsk
    * Slovenčina
    * Slovenščina
    * ไทย

**Complete list of Wikipedias**

Retrieved from
"https://en.wikipedia.org/w/index.php?title=Main_Page&oldid=696846920"

## Navigation menu

### Personal tools

  * Not logged in
  * Talk
  * Contributions
  * Create account
  * Log in

### Namespaces

  * Main Page
  * Talk

###  Variants

### Views

  * Read
  * View source
  * View history

### More

###  Search

### Navigation

  * Main page
  * Contents
  * Featured content
  * Current events
  * Random article
  * Donate to Wikipedia
  * Wikipedia store

### Interaction

  * Help
  * About Wikipedia
  * Community portal
  * Recent changes
  * Contact page

### Tools

  * What links here
  * Related changes
  * Upload file
  * Special pages
  * Permanent link
  * Page information
  * Wikidata item
  * Cite this page

### Print/export

  * Create a book
  * Download as PDF
  * Printable version

### Languages

  * Simple English
  * العربية
  * Bahasa Indonesia
  * Bahasa Melayu
  * Bosanski
  * Български
  * Català
  * Čeština
  * Dansk
  * Deutsch
  * Eesti
  * Ελληνικά
  * Español
  * Esperanto
  * Euskara
  * فارسی
  * Français
  * Galego
  * 한국어
  * עברית
  * Hrvatski
  * Italiano
  * ქართული
  * Latviešu
  * Lietuvių
  * Magyar
  * Nederlands
  * 日本語
  * Norsk bokmål
  * Norsk nynorsk
  * Polski
  * Português
  * Română
  * Русский
  * Slovenčina
  * Slovenščina
  * Српски / srpski
  * Srpskohrvatski / српскохрватски
  * Suomi
  * Svenska
  * ไทย
  * Tiếng Việt
  * Türkçe
  * Українська
  * 中文
  *   * This page was last modified on 26 December 2015, at 10:03.
  * Text is available under the Creative Commons Attribution-ShareAlike License; additional terms may apply. By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization.
  * Privacy policy
  * About Wikipedia
  * Disclaimers
  * Contact Wikipedia
  * Developers
  * Mobile view
  *   * 

