

# Leonardo da Vinci–Fiumicino Airport

From Wikipedia, the free encyclopedia

(Redirected from Leonardo da Vinci-Fiumicino Airport)

Jump to: navigation, search

Fiumicino – Leonardo da Vinci International Airport  
_Fiumicino – Aeroporto Internazionale Leonardo da Vinci_  
---  
IATA: FCO – ICAO: LIRF  
Summary  
Airport type | Public  
Operator | Aeroporti di Roma SpA  
Serves | Rome, Italy  
Location | Fiumicino  
Hub for |

  * Alitalia
  * Vueling  
Focus city for |

  * Alitalia CityLiner
  * Blue Panorama Airlines
  * easyJet
  * Mistral Air
  * Neos
  * Ryanair  
Elevation AMSL | 13 ft / 4 m  
Coordinates | 41°48′01″N 012°14′20″E﻿ / ﻿41.80028°N 12.23889°E﻿ / 41.80028;
12.23889Coordinates: 41°48′01″N 012°14′20″E﻿ / ﻿41.80028°N 12.23889°E﻿ /
41.80028; 12.23889  
Website | adr.it  
Map  
  
FCO

Location in Italy  
Runways  
| Direction | Length | Surface  
---|---|---  
m | ft  
07/25 | 3,300 | 10,827 | Asphalt  
16R/34L | 3,900 | 12,795 | Asphalt  
16L/34R | 3,900 | 12,795 | Asphalt  
16C/34C | 3,600 | 11,811 | Asphalt  
Statistics (2014)  
| Passengers | 38.506.908  
---|---  
Passenger change 13-14 |  +6.5%  
Aircraft movements | 308,144  
Movements change 13–14 |  +3.3%  
  
Source: Italian AIP at EUROCONTROL[1]  
Statistics from Assaeroporti[2] and AdR[3]  
  
**Fiumicino – Leonardo da Vinci International Airport** (Italian: _Fiumicino – Aeroporto Internazionale Leonardo da Vinci_) (IATA: **FCO**, ICAO: **LIRF**) or **Rome Fiumicino Airport**, also simply known as **Fiumicino Airport**, is a major international airport in Rome, Italy. It is one of the busiest airports in Europe by passenger traffic with 38,5 million passengers served in 2014.[3] It is located in Fiumicino, 18.9 nautical miles (35.0 km; 21.7 mi) southwest of Rome's historic city centre.[1]

The airport serves as the main hub for Alitalia, the largest Italian airline
and Vueling, a Spanish low-cost carrier owned by International Airlines Group.
Based on total passenger numbers, it is the eighth busiest airport in Europe
and was the world's 34th busiest airport in 2013. It covers an area of 29
square kilometres (7,200 acres) and is named after the most recognized
polymath Leonardo da Vinci, who designed the first proto helicopter and a
flying machine with wings in 1480.

## Contents

  * 1 History
    * 1.1 Early years
    * 1.2 Later development
  * 2 Terminals
  * 3 Airlines and destinations
    * 3.1 Scheduled
    * 3.2 Charter
    * 3.3 Cargo
  * 4 Traffic and statistics
  * 5 Ground handling
  * 6 Security services
  * 7 Ground transportation
  * 8 Incidents and accidents
  * 9 References
  * 10 External links

## History[edit]

### Early years[edit]

The airport was officially opened on 15 January 1961, with two runways,
replacing the smaller Rome Ciampino Airport, which remains in service for some
low cost airlines as well as domestic and charter operations. During the
1960s, Alitalia invested heavily in the new airport, building hangars and
maintenance centres; in the same period a third runway was added (16L/34R).
Despite being officially opened in 1961, Leonardo da Vinci–Fiumicino Airport
had actually been in use since 20 August 1960. This was to help relieve air
traffic that was congesting Rome Ciampino Airport during the 1960 Summer
Olympics.[4]

### Later development[edit]

Since 2005 the airport operates a category III B instrument landing system
(ILS). Further improvement work was implemented in 2007 to enable the airport
to handle 30 takeoffs/landings per hour, up from 10, in the event of thick
fog. Four runways presently operate at Leonardo da Vinci airport: 16L/34R and
16R/34L (separated by a distance of 4,000 m (13,000 ft)), 16C/34C (close to
16L/34R), mostly used as a taxiway or as a backup for 16L/34R, and 07/25, used
only westwards for takeoffs owing to the prevailing winds.

In 2010, the new single baggage handling system for more efficient luggage
delivery began operations.

Several projects are planned. These include the construction of an
environmentally-friendly cogeneration system, which would allow the airport to
produce its own energy; construction of Pier C (dedicated to international
flights) with 16 additional loading bridges, to handle the expected growth
from 38 million passengers per year[_when?_] to 55 million by 2018; and the
"Masterplan Fiumicino Nord", involving four new terminals and two new runways
to be built by 2044, when there are estimated to be 100 million passengers per
year.

## Terminals[edit]

The terminals were upgraded during the 1990s and 2000s.[5] In 1991, the
domestic Pier A with 12 gates opened. In 1995, the international Pier B with
10 gates opened. In 1999, the international Satellite C with 14 gates and an
elevated automated people mover, called SkyBridge, connected it with the main
terminal.

In 2000, the new domestic Terminal A opened, and the terminal buildings, then
consisting of Terminal A (with Pier A), Terminal AA, Terminal B (with Pier B)
and Terminal C (with Satellite C), were reorganized. In 2004, the new Cargo
City terminal opened. In 2008, Terminal 5 opened for check-in for American
carriers and El Al. Passengers are then bused to what was then called
Satellite C. The terminal serves 950,000 passengers per year. In 2009, the
terminals were renamed — A was renamed T1, AA was renamed T2, B and C became
T3 and T5 stayed the same.

  * **Terminal 1** (Gates _B1-B13_ and _B14-B30_) is used by Alitalia (short-haul flights) Aer Lingus, Air France, Croatia Airlines,[6] Etihad Regional and KLM.
  * **Terminal 2** (Gates _C1-C7_) is mainly used by easyJet with Blue Air and Wizz Air being the only other tennants.
  * **Terminal 3** (Gates _C8-C16_, _D1-D10_, _H1-H3_, _H6-H19_ and _G1-G14_) is the largest terminal and used by Alitalia (long-haul flights), Ryanair, Vueling and several other companies.
  * **Terminal 5** (Gates _H1-H3_, _H6-H19_ and _G1-G14_) is used by all US, some Asian,[7] and Israeli carriers.

## Airlines and destinations[edit]

Countries served by flights from Leonardo da Vinci–Fiumicino Airport (includes
seasonal and future destinations).

### Scheduled[edit]

Airlines | Destinations | Terminal  
---|---|---  
Aegean Airlines | Athens  
**Seasonal:** Corfu, Heraklion | 3  
Aer Lingus | Dublin | 1  
Aeroflot | Moscow-Sheremetyevo | 3  
Aeroflot  
operated by Rossiya | St Petersburg | 3  
Aerolíneas Argentinas | Buenos Aires-Ezeiza | 3  
Afriqiyah Airways | Tripoli (suspended) | 3  
Air Algérie | Algiers | 3  
airBaltic | Riga | 3  
Air Berlin | Berlin-Tegel, Düsseldorf | 3  
Air Canada | Montréal-Trudeau (resumes 16 June 2016),[8] Toronto-Pearson | 3
[7]  
Air Canada Rouge | Montréal-Trudeau (ends 16 June 2016) | 3  
Air China | Beijing-Capital | 5 [7]  
Air Europa | Madrid | 1  
Air France | Paris-Charles de Gaulle | 1  
Air India | Delhi | 3  
Air Malta | Malta | 3  
Air Moldova | Chișinău | 3  
Air Serbia | Belgrade | 1  
Air Transat | **Seasonal:** Montréal-Trudeau, Toronto-Pearson, Vancouver
(begins 18 June 2016)[9] | 5[7]  
Albastar | **Seasonal:** Lourdes[10] | 1  
Alitalia | Abu Dhabi, Alghero, Algiers, Amsterdam, Athens, Barcelona, Bari,
Beirut, Belgrade, Berlin-Tegel, Bilbao, Bologna, Boston, Brindisi, Brussels,
Bucharest, Budapest, Buenos Aires-Ezeiza, Cagliari, Cairo, Casablanca,
Catania, Copenhagen, Düsseldorf, Florence, Frankfurt, Geneva, Genoa, Istanbul-
Atatürk, Lamezia Terme, London-Heathrow, Madrid, Málaga, Malta, Marseille,
Miami, Milan-Linate, Milan-Malpensa, Montpellier, Moscow-Sheremetyevo, Munich,
Naples, New York-JFK, Nice, Palermo, Paris-Charles de Gaulle, Pisa, Prague,
Reggio Calabria, Rio de Janeiro-Galeão, Santiago de Chile (resumes 1 May
2016),[11] São Paulo-Guarulhos, Seoul-Incheon, Sofia, Tehran-Imam Khomeini,
Tel Aviv-Ben Gurion, Tirana, Tokyo-Narita, Toronto-Pearson, Toulouse, Trieste,
Tunis, Turin, Valencia, Venice-Marco Polo, Verona, Warsaw-Chopin, Zürich  
**Seasonal:** Amman-Queen Alia, Chicago-O'Hare, Heraklion, Kraków, Ibiza, Lampedusa, Los Angeles, Menorca, Mykonos, Palma de Mallorca, Pantelleria, Preveza (begins 4 August 2016), Rhodes, Saint Petersburg, Santorini (begins 4 August 2016),[12] Tbilisi | 1, 3  
Alitalia  
operated by Alitalia CityLiner | Berlin-Tegel, Bologna, Florence, Frankfurt,
Geneva, Genoa, London-City, Milan-Linate, Milan-Malpensa, Montpellier, Munich,
Naples, Nice, Oran, Podgorica, Skopje, Trieste, Turin, Venice-Marco Polo,
Verona, Zürich  
**Seasonal:** Thessaloniki (begins 5 August 2016) | 1, 3  
Alitalia  
operated by Darwin Airline | Naples, Perugia, Pescara, Pisa | 1  
Alitalia  
operated by Mistral Air | Ancona | 1  
American Airlines | Philadelphia  
**Seasonal:** Charlotte, Chicago-O'Hare, New York-JFK | 5  
Asiana Airlines | Seoul-Incheon | 5 [7]  
Austrian Airlines | Vienna | 3  
Belavia | Minsk-National | 3  
Blue Air | Bacău, Bucharest, Iași, Turin | 2  
Blu-express  
operated by Blue Panorama Airlines | Reggio Calabria, Tirana  
**Seasonal:** Heraklion, Ibiza, Lampedusa, Mykonos, Pantelleria, Preveza, Rhodes, Santorini, Skiathos, Zakynthos | 3  
Blue Panorama Airlines | Cancún, Havana, La Romana, Mérida,[13] Santiago de
Cuba | 3  
British Airways | London-Gatwick, London-Heathrow | 3  
Brussels Airlines | Brussels | 3  
Bulgaria Air | Sofia | 3  
Cathay Pacific | Hong Kong | 3  
China Airlines | Delhi, Taipei-Taoyuan | 5 [7]  
China Eastern Airlines | Shanghai-Pudong, Wenzhou (resumes 23 January
2016)[14] | 5 [7]  
China Southern Airlines | Guangzhou, Wuhan[15] | 5  
Croatia Airlines | Dubrovnik, Split, Zagreb | 3  
Czech Airlines | Prague | 3  
Delta Air Lines | Atlanta, New York-JFK  
**Seasonal:** Detroit, Minneapolis/St. Paul (begins 27 May 2016)[16] | 5  
easyJet | Amsterdam, Basel/Mulhouse (ends 28 February 2016),[17] Bristol,
Copenhagen (ends 7 February 2016),[17] Geneva (ends 28 February 2016),[17]
Hamburg (resumes 28 March 2016),[17] London-Gatwick, London-Luton, Lyon,
Marseille (ends 10 January 2016),[17] Milan-Malpensa, Munich (ends 10 January
2016),[17] Nantes (ends 28 February 2016),[17] Nice, Paris-Orly, Prague (ends
7 February 2016),[17] Tel Aviv-Ben Gurion (ends 6 February 2016),[17]
Tenerife-South (ends 6 February 2016),[17] Toulouse, Vienna (ends 6 January
2016)[17] | 2  
easyJet Switzerland | Basel/Mulhouse (begins 29 February 2016),[17] Geneva[18]
| 2  
EgyptAir | Cairo | 3  
El Al | Tel Aviv-Ben Gurion | 5  
Emirates | Dubai-International | 3  
Ethiopian Airlines | Addis Ababa | 3  
Etihad Airways | Abu Dhabi | 3  
Etihad Regional  
operated by Darwin Airline | Geneva | 1  
Eurowings | Düsseldorf, Vienna | 3  
Finnair | Helsinki | 3  
Germanwings | Berlin-Tegel, Cologne/Bonn, Hamburg, Hanover, Stuttgart | 3  
Hainan Airlines | Chongqing,[19] Xi'an[20] | 3  
HOP! | Bordeaux, Lyon, Strasbourg | 1  
Iberia | Madrid | 3  
Iberia Regional  
operated by Air Nostrum | **Seasonal:** Vigo (begins 12 July 2016) | 3  
Iran Air | Tehran-Imam Khomeini | 3  
Israir Airlines | Tel Aviv-Ben Gurion | 5  
Jet2.com | Glasgow, Manchester  
**Seasonal:** Leeds/Bradford, Newcastle | 3  
Jetairfly | Antwerp, Ostend | 3  
KLM | Amsterdam | 1  
Korean Air | Seoul-Incheon | 5[7]  
Kuwait Airways | Kuwait, Paris-Charles de Gaulle | 3  
Lufthansa | Frankfurt, Munich | 3  
Luxair | Luxembourg | 1  
Meridiana | Fuerteventura, Mauritius, Mombasa, Olbia, Tenerife-South  
**Seasonal:** La Romana, Sharm el-Sheikh, Zanzibar | 3  
Middle East Airlines | Beirut | 3  
Monarch Airlines | Birmingham, London-Luton | 3  
Montenegro Airlines | Podgorica | 3  
Neos | **Seasonal:** Boa Vista, Cancún, Fuerteventura, Heraklion, Malé, Kos,
Mombasa, Nosy Be, Sal, Tenerife-South, Zanzibar[21] | 3  
Niki | Vienna | 3  
Norwegian Air Shuttle | Copenhagen, Helsinki, London-Gatwick, Oslo-Gardermoen,
Stockholm-Arlanda  
**Seasonal:** Bergen, Gothenburg | 3  
Pegasus Airlines | Istanbul-Sabiha Gökçen | 3  
Qatar Airways | Doha | 3  
Royal Air Maroc | Casablanca | 3  
Royal Jordanian | Amman-Queen Alia | 3  
Ryanair | Alicante (begins 27 March 2016),[22] Barcelona, Bari, Brindisi,
Brussels, Catania, Comiso, Lamezia Terme, Malta (begins 27 March 2016),[23]
Marseille, Palermo, Seville, Trapani (begins 27 March 2016)[24]  
**Seasonal:** Corfu (begins 28 March 2016),[23] Kos (begins 29 March 2016)[23] | 3  
Saudia | Jeddah, Riyadh | 3  
Scandinavian Airlines | Copenhagen, Stockholm-Arlanda  
**Seasonal:** Oslo-Gardermoen | 3  
Singapore Airlines | Singapore | 3  
SmartWings  
operated by Travel Service Airlines | Prague | 3  
SriLankan Airlines | Colombo | 5 [7]  
SunExpress | **Seasonal:** Izmir | 3  
Swiss International Air Lines | Geneva, Zürich | 3  
TAP Portugal | Lisbon | 3  
TAP Portugal  
operated by Portugália | Porto | 3  
TAROM | Bucharest, Iași | 3  
Thai Airways | Bangkok-Suvarnabhumi | 3  
Transavia | Rotterdam/The Hague | 3  
Tunisair | Tunis | 3  
Turkish Airlines | Istanbul-Atatürk, Istanbul-Sabiha Gökçen | 3  
Ukraine International Airlines | Kiev-Boryspil  
**Seasonal:** Lviv | 3  
United Airlines | **Seasonal:** Chicago-O'Hare, Newark, Washington-Dulles | 5  
Ural Airlines | Yekaterinburg | 3  
Uzbekistan Airways | Tashkent | 3  
Vueling | Alicante, Amsterdam, Athens, Barcelona, Berlin-Tegel, Bilbao,
Brussels, Budapest, Catania, Edinburgh (begins 29 March 2016), Fuerteventura
(begins 28 June 2016),[25] Geneva (begins 27 March 2016),[25] Gran Canaria,
Lanzarote,[26] London-Gatwick, Lyon, Málaga, Manchester (beings 27 June
2016),[25] Marrakech, Marseille, Munich, Nantes, Nice, Palermo, Paris-Orly,
Prague, Santiago de Compostela, Seville, Stuttgart, Valencia, Vienna, Zürich
(begins 27 March 2016)[25]  
**Seasonal:** Cephalonia, Copenhagen (begins 1 June 2016),[25] Corfu, Dubrovnik,[25] Heraklion, Ibiza, Kalamata (begins 24 June 2016),[25] Karpathos, Kiev-Zhuliany (begins 3 July 2016),[25][27] Kos, Lampedusa, Larnaca, Lemnos, Malta, Menorca, Mykonos, Mytilene, Palma de Mallorca, Preveza/Lefkhada, Pula, Rennes, Reykjavík-Keflavík, Rhodes, Riga (begins 25 June 2016),[25] Santorini, Split, Stockholm-Arlanda (begins 23 June 2016),[25] Tallinn (begins 21 June 2016),[25] Tenerife-South (resumes 7 August 2016),[25] Zadar, Zakynthos | 3  
Wizz Air | Budapest, Katowice, Prague, Sofia (ends 26 March 2016), Vilnius,
Warsaw-Chopin | 2  
WOW Air | **Seasonal:** Reykjavík-Keflavík[28] | 3  
  
### Charter[edit]

Airlines | Destinations  
---|---  
Alitalia | **Summer seasonal:** Djerba, Hurghada, Kos, Marsa Alam, Mostar,
Mykonos, Santorini, Shannon, Sharm el-Sheikh  
**Winter seasonal:** Dubai-International, La Romana, Malé, Mauritius, Mombasa, Pointe-à-Pitre, Zanzibar  
ASL Airlines France | Ostend/Bruges, Paris-Orly, Tangier  
Blue Panorama Airlines | Marsa Alam, Sharm el-Sheikh  
**Summer seasonal:** Mersa Matruh  
Japan Airlines | **Summer seasonal:** Tokyo-Haneda  
Malmö Aviation | Billund, Odense  
Meridiana | **Summer seasonal:** Marsa Alam, Sharm el-Sheikh  
Mistral Air | **Summer seasonal:** Enfidha, Heraklion, Marsa Alam, Menorca,
Mostar, Shannon, Sharm el-Sheikh, Tarbes/Lourdes  
Neos | **Summer seasonal:** Mersa Matruh, Tel Aviv-Ben Gurion  
Tunisair | **Summer seasonal:** Djerba, Monastir, Tabarka  
Turkish Airlines | **Summer seasonal:** Izmir  
Ukraine International Airlines | **Summer seasonal:** Lviv[29]  
  
### Cargo[edit]

Airlines | Destinations  
---|---  
FedEx Express  
operated by ASL Airlines Ireland | Ancona, Paris-Charles de Gaulle  
Mistral Air | Brescia, Milan-Linate  
TNT Airways | Liège  
TNT Airways  
operated by Bluebird Cargo | Liège  
  
## Traffic and statistics[edit]

An Alitalia Boeing 777-200ER taxiing at Fiumicino

A British Airways Airbus A321 taxiing at Fiumicino

A Cathay Pacific Airbus A340-300 taxiing at Fiumicino

A Singapore Airlines Boeing 777-200ER taxiing at Fiumicino

A Royal Jordanian Airbus A321 taxiing at Fiumicino

A Delta Air Lines Boeing 767-300ER taxiing at Fiumicino

A KLM Boeing 737-800 taxiing at Fiumicino

A Aeroflot Airbus A320 taxiing at Fiumicino

Busiest routes from/to Rome-Fiumicino Airport in 2014 were the following:[30]

**Busiest domestic routes from/to Rome-Fiumicino (2014)**[30] Rank | Rank  
var.  
(13–14) | Airport | Passengers | Airline(s)  
---|---|---|---|---  
1 |  | Catania, Sicily |  1,861,589 | Alitalia, blu-express, Ryanair, Vueling  
2 |  1 | Palermo, Sicily |  1,481,469 | Alitalia, easyJet, Ryanair, Vueling  
3 |  1 | Milan-Linate, Lombardy |  1,455,244 | Alitalia, easyJet  
4 |  | Cagliari, Sardinia |  781,641 | Alitalia  
5 |  3 | Lamezia Terme, Calabria |  674,471 | Alitalia, Ryanair, Vueling  
6 |  1 | Turin, Piedmont |  619,130 | Alitalia, Vueling  
7 |  | Bari, Apulia |  541,958 | Alitalia, Vueling  
8 |  2 | Venice-Marco Polo, Veneto |  527,642 | Alitalia  
9 |  2 | Brindisi, Apulia |  394,230 | Alitalia, Vueling  
10 |  | Genoa, Liguria |  390,476 | Alitalia, Vueling  
11 |  2 | Milan-Malpensa, Lombardy |  332,226 | Alitalia, easyJet, Meridiana  
12 |  | Reggio Calabria, Calabria |  297,213 | Alitalia, Blu-express, Vueling  
13 |  1 | Naples, Campania |  289,965 | Alitalia  
14 |  1 | Trieste, Friuli-Venezia Giulia |  277,563 | Alitalia  
15 |  1 | Olbia, Sardinia |  275,503 | Meridiana  
16 |  3 | Bologna, Emilia-Romagna |  238,796 | Alitalia  
17 |  2 | Alghero, Sardinia |  231,298 | Alitalia, Livingston  
18 |  | Verona, Veneto |  198,981 | Alitalia  
19 |  2 | Florence, Tuscany |  196,884 | Alitalia  
20 |  | Pisa, Tuscany |  159,867 | Alitalia  
**Busiest European Routes from/to Rome-Fiumicino (2014)**[30] Rank | Rank  
var.  
(13–14) | Airport | Passengers | Airline(s)  
---|---|---|---|---  
1 |  | Paris-Charles de Gaulle, France |  1,134,521 | Air France, Alitalia,
Kuwait Airways  
2 |  2 | Amsterdam, Netherlands |  1,026,909 | Alitalia, KLM, easyJet, Vueling  
3 |  3 | Barcelona, Spain |  988,508 | Alitalia, Vueling  
4 |  2 | Madrid, Spain |  974,320 | Air Europa, Alitalia, Iberia, Vueling  
5 |  2 | London-Heathrow, United Kingdom |  958,525 | Alitalia, British
Airways  
6 |  1 | Paris-Orly, France |  781,202 | easyJet, Vueling  
7 |  5 | Brussels, Belgium |  721,144 | Alitalia, Brussels Airlines, Ryanair,
Vueling  
8 |  1 | Frankfurt am Main, Germany |  704.144 | Alitalia, Lufthansa  
9 |  1 | Athens, Greece |  671,168 | Aegean Airlines, Alitalia, easyJet,
Vueling  
10 |  1 | London-Gatwick, United Kingdom |  658,980 | British Airways,
easyJet, Norwegian Air Shuttle  
11 |  1 | Munich, Germany |  605,218 | Alitalia, Lufthansa, Vueling  
12 |  1 | Istanbul-Atatürk, Turkey |  490,933 | Alitalia, Turkish Airlines  
13 |  1 | Moscow-Sheremetyevo, Russia |  445,522 | Aeroflot, Alitalia  
14 |  1 | Vienna, Austria |  428,884 | Alitalia, Austrian Airlines, easyJet,
Niki  
15 |  1 | Copenhagen, Denmark |  423,198 | Alitalia, easyJet, Norwegian Air
Shuttle, Scandinavian Airlines  
16 |  3 | Zürich, Switzerland |  422,063 | Alitalia, Swiss International Air
Lines  
17 |  | Lisbon, Portugal |  368,461 | TAP Portugal  
18 |  | Geneva, Switzerland |  344,684 | Alitalia, Etihad Regional, easyJet
Switzerland  
19 |  1 | Prague, Czech Republic |  332,617 | Alitalia, Czech Airlines,
easyJet, Smart Wings, Vueling, Wizz Air  
20 |  3 | Berlin-Tegel, Germany |  292,406 | Air Berlin, Alitalia, Vueling  
**Busiest Intercontinental Routes from/to Rome-Fiumicino (2014)**[30] Rank | Rank  
var.  
(13–14) | City | Passengers | Airline(s)  
---|---|---|---|---  
1 |  | New York-John F. Kennedy, New York, United States |  618,941 |
Alitalia, American Airlines, Delta Air Lines  
2 |  | Tel Aviv, Israel |  574,131 | Alitalia, easyJet, El Al, Israir
Airlines, Neos  
3 |  | Dubai-International, United Arab Emirates |  561,170 | Emirates  
4 |  | Toronto-Pearson, Ontario, Canada |  293,256 | Alitalia, Air Canada, Air
Transat  
5 |  | Buenos Aires-Ezeiza, Argentina |  285,724 | Aerolíneas Argentinas,
Alitalia  
6 |  2 | Cairo, Egypt |  254,620 | Alitalia, Egyptair  
7 |  1 | Doha, Qatar |  243,305 | Qatar Airways  
8 |  1 | Tunis, Tunisia |  226,265 | Alitalia, Tunisair  
9 |  1 | São Paulo-Guarulhos, Brazil |  203,784 | Alitalia  
10 |  3 | Tokyo-Narita, Japan |  194,632 | Alitalia, Japan Airlines  
11 |  1 | Casablanca, Morocco |  188,046 | Alitalia, Royal Air Maroc  
12 |  1 | Atlanta, Georgia, United States |  172,958 | Delta Air Lines  
13 |  22 | Abu Dhabi, United Arab Emirates |  172,017 | Alitalia, Etihad
Airways  
14 |  2 | Miami, Florida, United States |  163,522 | Alitalia  
15 |  2 | Philadelphia, Pennsylvania, United States |  160,311 | US Airways  
16 |  1 | Hong Kong |  146,239 | Cathay Pacific  
17 |  | Algiers, Algeria |  144,327 | Air Algerie, Alitalia  
18 |  5 | Beirut, Lebanon |  142,190 | Alitalia, Middle East Airlines  
19 |  5 | Beijing-Capital, China |  138,846 | Air China  
20 |  1 | Chicago-O'Hare, Illinois, United States |  132,458 | Alitalia,
American Airlines  
  
## Ground handling[edit]

| This section **does not cite any sources**. Please help improve this section
by adding citations to reliable sources. Unsourced material may be challenged
and removed. _(January 2014)_  
---|---  
  
Ground handling services were provided by Aeroporti di Roma until 1999, when
it created Aeroporti di Roma Handling (to serve all airlines except for
Alitalia, which continued to be handled by Aeroporti di Roma itself). Alitalia
provided passenger assistance even before 1999. In 2001, Alitalia created
"Alitalia Airport" and started providing ground handling for itself and other
airlines. Air One created EAS and started providing third-party services as
well.[_when?_] Aeroporti di Roma Handling remains the biggest handler in terms
of airlines handled, but Alitalia Airport is the biggest handler in terms of
airplanes handled as Alitalia aircraft account for 50% of the ones at
Fiumicino.[_when?_] There are some other private handlers that provide
passenger assistance, including ARE Group, Globeground Italia and ICTS Italia.

On 2 May 2006, Meridiana's passenger handling staff transferred to Alitalia
Airport and the ramp employees transferred to Alitalia Airport in February
2007 (from Aeroporti di Roma Handling).

In May 2006, Italy's Civil Aviation Authority announced that it took off the
limitation of 3 ramp handlers in Rome Leonardo da Vinci airport. ARE Group and
Aviapartner announced that they would create a company called Aviapartner (51%
Aviapartner; 49% ARE Group) to serve Milan Malpensa and Rome Leonardo da
Vinci. There are fears that luggage mishandling will go up.[_by whom?_] Ground
handling deregulation has brought confusion on who does what and has decreased
service levels, especially on transferring baggage.

In November 2006 Aeroporti di Roma Handling was sold to Flightcare (itself
owned by Spanish company FCC), an Aviance member.

## Security services[edit]

| This section **does not cite any sources**. Please help improve this section
by adding citations to reliable sources. Unsourced material may be challenged
and removed. _(January 2014)_  
---|---  
  
Security Services transferred from the Polizia di Stato to Aeroporti di Roma
in 2000. Aeroporti di Roma created Airport Security (100%-owned) to provide
these services as well as security services to airlines (in competition with
other security companies such as IVRI). Airport Security is supervised by
Polizia di Stato (Italian State Police), Guardia di Finanza (Italian Customs
Police), Ente Nazionale Aviazione Civile (Italy's Civil Aviation Authority)
and Aeroporti di Roma.

## Ground transportation[edit]

Fiumicino Aeroporto railway station

Leonardo da Vinci is about 35 km (22 mi) by car from Rome's historic city
centre. The airport is served by a six-lane motorway and numerous buses and
taxis.

Fiumicino Aeroporto railway station is served by the Leonardo Express train
operated by Trenitalia, available at the airport terminal. It takes 30 minutes
to get to Termini Station in a non-stop trip that is provided twice an hour.
Alternatively, local trains (FL1 line) leave once every 15 minutes, stopping
at all stations. However these trains do not head to Termini station.
Passengers have to change at Trastevere, Ostiense (Metro Piramide) or
Tuscolana.[31] The railway opened in December 1989, with nonstop and several
stop services available.[32]

## Incidents and accidents[edit]

From the 1960s until the 1980s, the airport experienced significant aircraft
hijackings as well as being the scene of two major terrorist attacks and the
port of origin for an aircraft bombing in flight—some engendered by the
Israeli-Palestinian conflict.

  * On 23 November 1964, TWA Flight 800, operated by a Boeing 707, had an engine catch fire during take off. 50 of the 73 passengers and crew on board were killed.
  * On 17 December 1973, during the 1973 Rome airport attacks and hijacking, a Boeing 707-321B operating as Pan American World Airways (Pan Am) Flight 110 was attacked by Palestinian terrorists. 30 passengers were killed when phosphorus bombs were thrown aboard the aircraft as it was preparing for departure.[33] During the same incident a Lufthansa Boeing 737 (D-ABEY)[34] was hijacked and landed at Athens, Damascus and finally in Kuwait. All remaining passengers and crew were then released.[33] One person died in the incident.[34]
  * On 27 December 1985, during the Rome and Vienna airport attacks, terrorists shot and killed 16 people and wounded 99 others at the airport.
  * On 2 April 1986, TWA Flight 840, which was travelling from Fiumicino to Ellinikon International Airport in Athens, Greece, was bombed, ejecting 4 people from the plane to their deaths. The plane landed safely.
  * On 17 October 1988, Uganda Airlines Flight 775 from London Gatwick to Entebbe International Airport via Fiumicino, crashed short of the runway after two missed approaches. Twenty-six of the 45 passengers aboard, as well as all 7 crew members, died.
  * On 2 February 2013, Alitalia Flight 1670, en route from Pisa International Airport to Rome, overran the runway during landing. Sixteen occupants were injured, two of them seriously.[35][36]
  * On 29 September 2013 at 20:10, an Alitalia Airbus A320 flying from Madrid Barajas Airport to Rome Fiumicino airport failed to deploy the landing gear during a storm on landing and the aircraft toppled, skidded off the runway and crashed. 10 passengers suffered minor injuries and all 151 passengers and crew were evacuated and taken to hospital. The crash is still being investigated.[37]
  * On 7 May 2015, during the early hours of the morning, a fire broke out and caused substantial damage to a number of security control cabins and the main commercial area of Terminal 3. The airport reopened shortly after 2pm local time. Hundreds of flights were cancelled due to the fire.[38] The terminal has now reopened with all check-in desks operational, new security screening facilities, and piers D, G and H now accepting passengers.[_citation needed_]
  * On 29 July 2015, a forest fire broke out near the airport, causing its closure for 2 hours.

## References[edit]

  1. ^ _**a**_ _**b**_ "EAD Basic". Ead.eurocontrol.int. Retrieved 25 April 2014.
  2. **^** Associazione Italiana Gestori Aeroportuali[_dead link_]
  3. ^ _**a**_ _**b**_ "Traffic data". Retrieved 2 June 2015.
  4. **^** "Fiumicino: Italy's Fast Growing Airport | Italy". Lifeinitaly.com. Retrieved 25 April 2014.
  5. **^** "Expansion projects at Fiumicino". Airport-technology.com. 15 June 2011. Retrieved 25 April 2014.
  6. **^** http://www.anna.aero/2015/04/29/new-airline-routes-launched-21-april-27-april-2015/
  7. ^ _**a**_ _**b**_ _**c**_ _**d**_ _**e**_ _**f**_ _**g**_ _**h**_ _**i**_ http://www.adr.it/documents/10157/554493/Allocazione+Terminal+per+Vettori_24luglio.pdf
  8. **^** http://airlineroute.net/2015/11/13/ac-yulfco-jun16/
  9. **^** "airtransat to Launch Vancouver - Rome Service from June 2016". Airlineroute.net. September 3, 2015. Retrieved September 3, 2015.
  10. **^** "Spain's AlbaStar to offer scheduled Italy-Lourdes flights". _ch-aviation_. Retrieved 2 June 2015.
  11. **^** "Alitalia Resumes Chile Service from May 2016". Airlineroute.net. 27 October 2015. Retrieved 27 October 2015.
  12. **^** L, J (25 November 2015). "Alitalia Adds Rome – Thira Service in August 2016". Airline Route. Retrieved 25 November 2015.
  13. **^** "Blue Panorama Merida Service Changes from Dec 2015". Airlineroute.net. 16 July 2015. Retrieved 16 July 2015.
  14. **^** http://airlineroute.net/2015/12/05/mu-wnzfco-jan16/
  15. **^** http://airlineroute.net/2015/08/27/cz-fcochc-w15/
  16. **^** "DELTA Adds Minneapolis - Rome Service from May 2016". Airlineroute.net. 5 October 2015. Retrieved 5 October 2015.
  17. ^ _**a**_ _**b**_ _**c**_ _**d**_ _**e**_ _**f**_ _**g**_ _**h**_ _**i**_ _**j**_ _**k**_ _**l**_ http://forum.airliners.de/topic/33610-aktuelle-streckeneinstellungen-reduktionen/?p=721122
  18. **^** "Flight Timetables". easyJet.
  19. **^** "New direct flight to link China's Chongqing and Rome". Retrieved 2 June 2015.
  20. **^** "Hainan Airlines Adds Xi'An - Rome Service from Dec 2015". Airlineroute.net. 4 November 2015. Retrieved 4 November 2015.
  21. **^** http://www.neosair.it/
  22. **^** https://m.ryanair.com/app/index.html#trap
  23. ^ _**a**_ _**b**_ _**c**_ http://airlineroute.net/2015/09/20/fr-fco-s16/
  24. **^** https://ryanair.com/gb/en/
  25. ^ _**a**_ _**b**_ _**c**_ _**d**_ _**e**_ _**f**_ _**g**_ _**h**_ _**i**_ _**j**_ _**k**_ _**l**_ "New Routes from Rome S16" (in Italian). 5 October 2015. Retrieved 6 October 2015.
  26. **^** http://www.vueling.com/en
  27. **^** "Испанский лоу-кост Vueling откроет рейсы Киев-Рим". avianews.com by Aviation Today. avianews.com. 6 October 2015. Retrieved 6 October 2015.
  28. **^** "WOW Adds Three New Destinations". _Iceland Review_. Retrieved 2 June 2015.
  29. **^** "Timetable". Ukraine International Airlines. Retrieved 8 May 2013.
  30. ^ _**a**_ _**b**_ _**c**_ _**d**_ "ENAC: Italy's Traffic Statistics 2014" (PDF) (in Italian). 2 April 2015. Retrieved 7 April 2015.
  31. **^** [1][_dead link_]
  32. **^** _Flight International_. 23 May 1987. 5.
  33. ^ _**a**_ _**b**_ Ramsden, J. M., ed. (27 December 1973). "Rome hijacking" (PDF). _FLIGHT International_ (IPC Transport Press Ltd) **104** (3380): p.1010. Retrieved 11 February 2015 – via flightglobal.com/pdfarchive. ... ran on to the apron and two phosphorus bombs were thrown into the front and rear entrances of a Pan American 707 Celestial Clipper, with 170 passengers on board CS1 maint: Extra text (link)
  34. ^ _**a**_ _**b**_ "Hijacking description: Monday 17 December 1973". _aviation-safety.net_. Flight Safety Foundation. 11 February 2015. Retrieved 11 February 2015.
  35. **^** Posted by foxcrawl at 2:31 am. "Carpatair ATR-72 plane overruns runway on landing in Rome". Foxcrawl. Retrieved 6 February 2013.
  36. **^** Squires, Nick (4 February 2013). "Alitalia paints over crashed plane's markings". _Telegraph_. Retrieved 13 February 2013.
  37. **^** Matt Blake (30 September 2013). "Alitalia plane carrying 151 passengers crash lands in Rome after its landing gear fails to open in a storm | Mail Online". London: Dailymail.co.uk. Retrieved 17 January 2014.
  38. **^** BBC News (7 May 2015). "Chaos at Rome Fiumicino airport after terminal fire". BBC News. Retrieved 7 May 2015.

## External links[edit]

Media related to Fiumicino Airport at Wikimedia Commons

  * Leonardo da Vinci international airport (English/Italian)
  * Current weather for LIRF at NOAA/NWS
  * Accident history for FCO at Aviation Safety Network
|

  * Rome portal
  * Italy portal
  * Aviation portal  
  
---  
|

  * v
  * t
  * e

Airports in Italy  
  
---  
  
---  
Major international |

  * Milan–Malpensa
  * **Rome–Fiumicino**  
  
Minor international |

  * Alghero
  * Ancona
  * Bari
  * Bergamo
  * Bologna
  * Brindisi
  * Cagliari
  * Catania
  * Comiso
  * Cuneo
  * Elba
  * Florence
  * Genoa
  * Lamezia Terme
  * Milan–Linate
  * Naples
  * Olbia
  * Palermo
  * Parma
  * Perugia
  * Pescara
  * Pisa
  * Rimini
  * Rome–Ciampino
  * Trapani
  * Treviso
  * Trieste
  * Turin
  * Venice–Marco Polo
  * Verona  
  
Domestic |

  * Brescia
  * Crotone
  * Foggia
  * Lampedusa
  * Pantelleria
  * Reggio Calabria  
  
Unscheduled |

  * Albenga
  * Aosta
  * Bolzano
  * Forlì
  * Oristano
  * Salerno
  * Tortolì  
  
Military |

  * Amendola
  * Aviano
  * Decimomannu
  * Ghedi
  * Gioia del Colle
  * Grosseto
  * Istrana
  * Piacenza–San Damiano
  * Pratica di Mare
  * Rivolto
  * Sigonella
  * Vicenza
  * Villafranca  
  
Retrieved from "https://en.wikipedia.org/w/index.php?title=Leonardo_da_Vinci–F
iumicino_Airport&oldid=696738853"

Categories:

  * Airports established in 1961
  * Airports in Rome
  * Fiumicino
  * Buildings and structures in Lazio

Hidden categories:

  * All articles with dead external links
  * Articles with dead external links from April 2014
  * CS1 Italian-language sources (it)
  * CS1 maint: Extra text
  * Use dmy dates from May 2014
  * Coordinates on Wikidata
  * Articles containing Italian-language text
  * Vague or ambiguous time from January 2014
  * Articles needing additional references from January 2014
  * All articles needing additional references
  * Articles with specifically marked weasel-worded phrases from January 2014
  * All articles with unsourced statements
  * Articles with unsourced statements from July 2015

## Navigation menu

### Personal tools

  * Not logged in
  * Talk
  * Contributions
  * Create account
  * Log in

### Namespaces

  * Article
  * Talk

###  Variants

### Views

  * Read
  * Edit
  * View history

### More

###  Search

### Navigation

  * Main page
  * Contents
  * Featured content
  * Current events
  * Random article
  * Donate to Wikipedia
  * Wikipedia store

### Interaction

  * Help
  * About Wikipedia
  * Community portal
  * Recent changes
  * Contact page

### Tools

  * What links here
  * Related changes
  * Upload file
  * Special pages
  * Permanent link
  * Page information
  * Wikidata item
  * Cite this page

### Print/export

  * Create a book
  * Download as PDF
  * Printable version

### Languages

  * Afrikaans
  * العربية
  * Беларуская
  * Български
  * Català
  * Čeština
  * Dansk
  * Deutsch
  * Eesti
  * Ελληνικά
  * Español
  * Euskara
  * فارسی
  * Français
  * 한국어
  * हिन्दी
  * Bahasa Indonesia
  * Italiano
  * עברית
  * Basa Jawa
  * Latina
  * Lietuvių
  * Magyar
  * Македонски
  * मराठी
  * Bahasa Melayu
  * Nederlands
  * 日本語
  * Norsk bokmål
  * Polski
  * Português
  * Română
  * Русский
  * Scots
  * Slovenčina
  * Suomi
  * Svenska
  * ไทย
  * Тоҷикӣ
  * Türkçe
  * Українська
  * Tiếng Việt
  * 中文
  * Edit links

  * This page was last modified on 25 December 2015, at 11:27.
  * Text is available under the Creative Commons Attribution-ShareAlike License; additional terms may apply. By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization.
  * Privacy policy
  * About Wikipedia
  * Disclaimers
  * Contact Wikipedia
  * Developers
  * Mobile view
  *   * 
  *[t]: Discuss this template
  *[v]: View this template
  *[e]: Edit this template

