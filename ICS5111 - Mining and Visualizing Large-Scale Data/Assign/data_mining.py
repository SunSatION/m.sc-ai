import sys
import html2text
from urllib.parse import urlparse
import pdfminer
import hashlib
import re
import shutil
from os import listdir
from os.path import isfile, join


#goes through the files given and obtains all URLS. The URLs are cleaned of any unwanted URLS.
#Next, a batch file is created with wget instructions to obtain a copy of the sites pertaining to the URLs extracted.
def create_wget_batch_file(current_user):
    #adding URLs to a list provided that they are not already in it
    def addToList(self, url_to_add):
        if url_to_add not in self:
            self.append(url_to_add)

    import xml.etree.ElementTree as ET
    list_of_urls = []

#what is this for??
#    current_user = 1

    tree = ET.parse(".\\ics_5111_dataset\\user_" + str(current_user) + "\\User Activity\\history_user_1." + str(current_user) + "_0.rdf")
    root = tree.getroot()
    for desc in root.findall("{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Description"):
        for link in desc.findall("{http://tempuri.org/}uri"):
            url = link.attrib.get("{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource")
            if not any(x in url for x in ["google.com", "file:/"]):
                addToList(list_of_urls, url)

    f = open('ics_5111_dataset\\user_' + str(current_user) + '\\export.bat', 'w+')
    for i in list_of_urls:
        f.write("wget --adjust-extension --span-hosts --convert-links --backup-converted --page-requisites -e robots=off -p -P \".\\ics_5111_dataset\\user_" + str(current_user) + "\\Extracted\" \"" + i + "\"\r\n")


def get_list_of_url(current_user):
    #adding URLs to a list provided that they are not already in it
    def addToList(self, url_to_add):
        if url_to_add not in self:
            self.append(url_to_add)

    import xml.etree.ElementTree as ET
    list_of_urls = []
    #opens the user activies as a tree. Isn't this already done in "create_wget_batch_file(current_user):"??
    tree = ET.parse(".\\ics_5111_dataset\\user_" + str(current_user) + "\\User Activity\\history_user_1." + str(current_user) + "_0.rdf")
    root = tree.getroot()
    for desc in root.findall("{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Description"):
        for link in desc.findall("{http://tempuri.org/}uri"):
            url = link.attrib.get("{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource")
            if not any(x in url for x in ["google.com", "file:/"]):
                addToList(list_of_urls, url)

    return list_of_urls

#is this for the functions below or for all the functions? This provides OS interaction right?
import os;

#calculates the text-to-hrml ratio
def get_html_to_text_ratio(threshold):
    for i in url_list:#where is url_list passed to the function?? :\
        hashed_url = hashlib.md5(i.encode('utf8')).hexdigest()
        if os.path.isfile(folder_loc + 'single_extracted_\\' + hashed_url):#where is folder_loc defined?
            ratio = os.stat( folder_loc + 'single_extracted_html2text\\' + hashed_url).st_size \
                    /  (os.stat(folder_loc +  'single_extracted_html2text\\' + hashed_url).st_size + os.stat( folder_loc + 'single_extracted_\\' + hashed_url).st_size )
            if ( ratio > 0.2 ) and ratio != 1:
                print(i + ' - ' + hashed_url + ' - ' + str(ratio))
        else:
            pass
            #print (i + ' - ' + hashed_url + "- file does not exist")

#is this not used any more?
"""
def create_wget_batch_file_domain(current_user):

    list_of_urls = get_list_of_url(current_user)
    f = open('ics_5111_dataset\\user_' + str(current_user) + '\\export_domain.bat', 'w+')
    for i in list_of_urls:
        f.write("wget --adjust-extension --span-hosts --convert-links --backup-converted --page-requisites -e robots=off -p -P \".\\ics_5111_dataset\\user_" + str(current_user) + "\\Extracted\" \"" + urlparse(i).netloc + "\"\r\n")
"""
#whats the difference between this and "create_wget_batch_file(current_user):"?
def create_wget_batch_file_domain(current_user):
    list_of_urls = get_list_of_url(current_user)
    f = open('ics_5111_dataset\\user_' + str(current_user) + '\\export_domain.bat', 'w+')
    for i in list_of_urls:
        f.write("wget -e robots=off --user-agent=\"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36\" \"" + urlparse(i).netloc + "\" -O \"" + ".\\Single_Extracted_Domain_\\" + hashlib.md5(i.encode('utf8')).hexdigest() + "\" --append-output=domain.log \r\n")

#what's this? Just hashing?
def gen_hashed_filename(current_user):
    f = open('ics_5111_dataset\\user_' + str(current_user) + '\\export_filename.bat', 'w+')
    for i in url_list :
        f.write("wget -e robots=off --user-agent=\"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36\" \"" + i + "\" -O \"" + ".\\Single_Extracted_\\" + hashlib.md5(i.encode('utf-8')).hexdigest() + "\" --append-output=single.log\r\n")

from sklearn.feature_extraction.text import TfidfVectorizer;
from sklearn.metrics.pairwise import linear_kernel
import scipy
import numpy


def convert_to_text(current_user):
    files = {'g':'h'}#what does this mean?
    h = html2text.HTML2Text()
    h.ignore_links = True;
    h.ignore_images = True;
    from pdfminer.pdfparser import PDFParser, PDFDocument
    from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
    from pdfminer.converter import PDFPageAggregator
    from pdfminer.layout import LAParams, LTTextBox, LTTextLine

    with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\datastrct.txt", 'w', encoding="utf-8") as dstruct:
        for i in url_list :#where is url_list passed?
            for j in ['', 'domain_']:#huh?
                files[hashlib.md5(i.encode('utf8')).hexdigest()] = {'url': i}
                print(hashlib.md5(i.encode('utf8')).hexdigest())
                if i.endswith('.pdf') and j == '':
                    print(i);
                    print('PDF in Action - ' + hashlib.md5(i.encode('utf8')).hexdigest())
                    try:

                        fp = open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_" + j + "\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'rb')
                        parser = PDFParser(fp)
                        doc = PDFDocument()
                        parser.set_document(doc)
                        doc.set_parser(parser)
                        doc.initialize('')
                        rsrcmgr = PDFResourceManager()
                        laparams = LAParams()#what's this?
                        device = PDFPageAggregator(rsrcmgr, laparams=laparams)
                        interpreter = PDFPageInterpreter(rsrcmgr, device)
                        # Process each page contained in the document.
                        with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_" + j + "html2text\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'w', encoding="utf-8") as write_file:
                            dx = 0
                            for page in doc.get_pages():
                                interpreter.process_page(page)
                                layout = device.get_result()
                                for lt_obj in layout:
                                    if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
                                        write_file.write(lt_obj.get_text())
                                        dx = dx + len(lt_obj.get_text())
                        if j == '':
                            dstruct.write(hashlib.md5(i.encode('utf8')).hexdigest() + ',' + str(dx) +  ',' + i + '\n')
                        else :
                            dstruct.write(hashlib.md5(i.encode('utf8')).hexdigest() + ',' + str(dx) +  ',' + urlparse(i).netloc + '\n')
                    except Exception as e:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        print(exc_type, exc_tb.tb_lineno)
                        print("Exception!!")
                else:
                    try:
                        with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_" + j + "\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'r', encoding="utf-8") as content_file:
                            content = content_file.read()
                    except UnicodeDecodeError:
                        with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_" + j + "\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'r', encoding="latin-1") as content_file:
                            content = content_file.read()
                    except Exception as e:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        print(exc_type, exc_tb.tb_lineno)
                        print("Exception!!")

                    if j == '':
                        dstruct.write(hashlib.md5(i.encode('utf8')).hexdigest() + ',' + str(len(content)) +  ',' + i + '\n')
                    else :
                        dstruct.write(hashlib.md5(i.encode('utf8')).hexdigest() + ',' + str(len(content)) +  ',' + urlparse(i).netloc + '\n')

                    with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_" + j + "html2text\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'w', encoding="utf-8") as write_file:
                        write_file.write(h.handle(content))



def create_dir(current_user):
    import os as io;
    try:
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_domain_html2text\\");
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_domain_html2text_cleaned\\");
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_html2text\\");
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_html2text_cleaned\\");
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_\\");
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_domain_\\");
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\final\\");
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\final_stem\\");
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\final_stopwords\\");
        io.mkdir(".\\ics_5111_dataset\\user_" + str(current_user) + "\\final_stopwords_stem\\");
    except Exception as d:
        pass

#what does this do?
def clean_expand(current_user):
    for i in url_list:#where does this come from?? :\
          for j in ['', 'domain_']:
            with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_" + j + "html2text\\" + hashlib.md5(i.encode('utf8')).hexdigest(), "r", encoding="utf-8") as f:
                with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_" + j + "html2text_cleaned\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'w', encoding="utf-8") as write_file:
                    for line in f:
                        cleanedLine = line.strip()
                        if cleanedLine.startswith('#'):
                            #print(cleanedLine)
                            newString = cleanedLine[cleanedLine.find(' ')+1:]
                            cleanedLine2 = ''
                            for l in range(0, max(6-cleanedLine.count('#'),1)):
                                cleanedLine2 += newString + ' '
                            cleanedLine = cleanedLine2;
                        cleanedLine = re.sub(r'\[\d+\]+', '', cleanedLine);
                        cleanedLine = re.sub(r'([^\s\w\r\n\.\,\?\!]|_)+', ' ', cleanedLine).strip()
                        cleanedLine = re.sub(r'  ', ' ', cleanedLine);
                        cleanedLine = cleanedLine.lower();
                        write_file.writelines(cleanedLine + '\n')

#clean_expand(current_user)

def remove_main_page_content(current_user):
    import difflib
    for i in url_list:

        if urlparse(i).path == '/':
            shutil.copy2(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_html2text_cleaned\\" + hashlib.md5(i.encode('utf8')).hexdigest(), ".\\ics_5111_dataset\\user_" + str(current_user) + "\\final\\" + hashlib.md5(i.encode('utf8')).hexdigest())
            continue
        print(i)
        with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_domain_html2text_cleaned\\" + hashlib.md5(i.encode('utf8')).hexdigest(), "r", encoding="utf-8") as toCompare:
            with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\single_extracted_html2text_cleaned\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'r', encoding="utf-8") as original:
                with open(".\\ics_5111_dataset\\user_" + str(current_user) + "\\final\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'w', encoding="utf-8") as final:
                    for x in difflib.ndiff(original.readlines(), toCompare.readlines()):
                        if x.startswith('-') and not(x == "- t discuss this template\n" or x == "- v view this template\n" or x == "- e edit this template\n")  :
                            final.write(x.strip()[2:] + '\n')
                        if x.strip() == '':
                            final.write('\n')

def parse_to_paragraph():
    from nltk.tokenize import TextTilingTokenizer

    for idx, i in enumerate(url_list):
        with open(folder_loc + "final_stopwords\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'r', encoding="utf8") as content_file:
            print(hashlib.md5(i.encode('utf8')).hexdigest())
            print(i)
            if not(os.path.exists(folder_loc + "final_stopwords\\" + hashlib.md5(i.encode('utf8')).hexdigest() + '_p')):
                os.mkdir(folder_loc + "final_stopwords\\" + hashlib.md5(i.encode('utf8')).hexdigest() + '_p')
            else:
                continue
            content = content_file.read()
            if len(content) > 700 :
                TT = {};
                try:
                    tt = TextTilingTokenizer(k=10, w=20, stopwords=stopwords.words('english'))
                    tk = tt.tokenize(content)
                    for single_paragraph in range(0,len(tk)):
                        with open(folder_loc + "final_stopwords\\" + hashlib.md5(i.encode('utf8')).hexdigest() + "_p\\" + str(single_paragraph) + ".txt", 'w', encoding="utf8") as write_file:
                            write_file.writelines(tk[single_paragraph])
                except (RuntimeError, TypeError, NameError):
                    print('pass1 failed')
                    try:
                        tt = TextTilingTokenizer(k=8, w=16, stopwords=stopwords.words('english'))
                        tk = tt.tokenize(content)
                        for single_paragraph in range(0,len(tk)):
                            with open(folder_loc + "final_stopwords\\" + hashlib.md5(i.encode('utf8')).hexdigest() + "_p\\" + str(single_paragraph) + ".txt", 'w', encoding="utf8") as write_file:
                                write_file.writelines(tk[single_paragraph])
                    except (RuntimeError, TypeError, NameError):
                        print('pass2 failed')
                        try:
                            tt = TextTilingTokenizer(k=5, w=10, stopwords=stopwords.words('english'))
                            tk = tt.tokenize(content)
                            for single_paragraph in range(0,len(tk)):
                                with open(folder_loc + "final_stopwords\\" + hashlib.md5(i.encode('utf8')).hexdigest() + "_p\\" + str(single_paragraph) + ".txt", 'w', encoding="utf8") as write_file:
                                    write_file.writelines(tk[single_paragraph])
                        except (RuntimeError, TypeError, NameError):
                            print('pass3 failed')
                            try:
                                tt = TextTilingTokenizer(k=3, w=8, stopwords=stopwords.words('english'))
                                tk = tt.tokenize(content)
                                for single_paragraph in range(0,len(tk)):
                                    with open(folder_loc + "final_stopwords\\" + hashlib.md5(i.encode('utf8')).hexdigest() + "_p\\" + str(single_paragraph) + ".txt", 'w', encoding="utf8") as write_file:
                                        write_file.writelines(tk[single_paragraph])
                            except (RuntimeError, TypeError, NameError):
                                print('pass4 failed')

paragraph_list = [];

from nltk.stem.porter import *
from nltk.corpus import stopwords

def apply_stemming_stopwords():
    stemmer = PorterStemmer()
    for i in url_list:
        with open(folder_loc + "final\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'r', encoding="utf8") as content_file:
            current_file = content_file.read()
            singles = [];
            with open(folder_loc + "final_stem\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'w', encoding="utf8") as write_file:
                for x in current_file.splitlines():
                    singles = [stemmer.stem(word) for word in x.split(' ')]
                    write_file.write(' '.join(singles) + '\n')
            with open(folder_loc + "final_stopwords\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'w', encoding="utf8") as write_file:
                for x in current_file.splitlines():
                    filtered_words = [word for word in x.split(' ') if word not in stopwords.words('english')]
                    write_file.write(' '.join(filtered_words) + '\n')
        with open(folder_loc + "final_stopwords\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'r', encoding="utf8") as content_file:
            current_file = content_file.read()
            with open(folder_loc + "final_stopwords_stem\\" + hashlib.md5(i.encode('utf8')).hexdigest(), 'w', encoding="utf8") as write_file:
                for x in current_file.splitlines():
                    singles = [stemmer.stem(word) for word in x.split(' ')]
                    write_file.write(' '.join(singles) + '\n')
                    #print('s')



#exit()

# Step 1
# create_dir(current_user)
# create_wget_batch_file_domain(current_user)
# gen_hashed_filename(current_user)#why do this?

"""
# Step 2
convert_to_text(current_user)

# Step 3
clean_expand(current_user)#what do you mean by expand?
remove_main_page_content(current_user)
apply_stemming_stopwords()
parse_to_paragraph()


exit()

"""
def generate_paragraphs():
    import gensim
    from nltk.tokenize import RegexpTokenizer
    from gensim import corpora, models

    with (open(folder_loc + 'topic_paragraphs.csv', 'w', encoding='utf8')) as output_file:

        for x in range(0,len(url_list)):
            x = url_list[x]
            #filenames.append(folder_name + "\\" + hashlib.md5(x.encode('utf8') + '_p\\').hexdigest())
            mypath = folder_loc + 'final_stopwords' + "\\" + hashlib.md5(x.encode('utf8')).hexdigest() + '_p\\';
            paths = [mypath + f for f in listdir(mypath) if isfile(join(mypath, f))]
            tokenizer = RegexpTokenizer(r'\w+')

            for ij in range(0, len(paths)):
                #paragraph_list.append({'url':x, 'filename' : hashlib.md5(x.encode('utf8')).hexdigest(), 'paragraph': ij })
                with open(paths[ij], 'r', encoding="utf8") as content:
                    raw = ' '.join(content.readlines())
                    if raw.strip() != '':
                        print('---')
                        #paragraph_list.append(' '.join(content.readlines()))

                        # turn our tokenized documents into a id <-> term dictionary
                        tokens = [tokenizer.tokenize(raw)]
                        dictionary = corpora.Dictionary(tokens)
                        # convert tokenized documents into a document-term matrix
                        corpus = [dictionary.doc2bow(text) for text in tokens]
                        # generate LDA model
                        ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=1, id2word = dictionary, passes=20)

                        output_file.write(x + ',' + str(ij)  + ',' + ldamodel.print_topics(num_topics=2, num_words=6)[0][1] + '\n')




def query_string(stop_words=1, stemming=1, ngrams=1, full_vocab = 0, paragraphs=0):
    tfidf = TfidfVectorizer(input='filename', ngram_range=(1,ngrams))
    filenames = []
    folder_name = ''
    if stop_words == 1 and stemming == 0:
        folder_name = 'final_stopwords'
    if stemming == 1 and stop_words == 0:
        folder_name = 'final_stem'

    if stemming == 1 and stop_words == 1:
        folder_name = 'final_stopwords_stem'

    if stemming == 0 and stop_words == 0:
        folder_name = 'final'

    if full_vocab == 1:
        folder_name = 'final'

    if paragraphs == 0:
        for x in url_list:
            filenames.append(".\\ics_5111_dataset\\user_" + str(current_user) + "\\" + folder_name + "\\" + hashlib.md5(x.encode('utf8')).hexdigest())
    else:
        for x in url_list:
            #filenames.append(folder_name + "\\" + hashlib.md5(x.encode('utf8') + '_p\\').hexdigest())
            mypath = folder_loc + folder_name + "\\" + hashlib.md5(x.encode('utf8')).hexdigest() + '_p\\';
            paths = [mypath + f for f in listdir(mypath) if isfile(join(mypath, f))]
            for ij in range(0, len(paths)):
                paragraph_list.append({'url':x, 'filename' : hashlib.md5(x.encode('utf8')).hexdigest(), 'paragraph': ij })
            filenames = filenames + paths


            #onlyfiles = [f for f in listdir(folder_name + "\\" + hashlib.md5(x.encode('utf8')) + '_p\\') if isfile(join(folder_name + "\\" + hashlib.md5(x.encode('utf8') + '_p\\', f))]

    sparseX = tfidf.fit_transform(filenames)
    return (sparseX, tfidf)

def get_matches(word_string, data, stop_words=1, stemming=1):

    stemmer = PorterStemmer()
    test = scipy.sparse.csc_matrix((1, data[0].shape[1]), dtype=numpy.float)

    word_list = re.sub(r'([^\s\w])+', ' ', word_string.lower()).strip();
    filtered_words = word_list.split(' ')

    if stop_words== 1:
        filtered_words = [word for word in word_list.split(' ') if word not in stopwords.words('english')]
    else:
        filtered_words = word_list.split(' ')

    if stemming == 1:
        word_list = [stemmer.stem(word) for word in filtered_words]
    else:
        word_list = filtered_words;

    filtered_words = word_list;
    for i in filtered_words:
        backup = i;
        i= i.replace('_', ' ')
        if i not in data[1].get_feature_names():
            filtered_words.remove(backup)

    for idx, word in enumerate(filtered_words):
        word = word.replace('_', ' ')
        if word in data[1].get_feature_names():
            test[0, data[1].get_feature_names().index(word)] +=  1 / len(word_list);
    cosine_similarities = linear_kernel(test, data[0])
    print(cosine_similarities)

    with (open('task_1_user_' + str(current_user) + '.csv', 'a+')) as output_file:
        for idx, cosx in enumerate(cosine_similarities[0]):
            output_file.write(url_list[idx] + ',' + hashlib.md5(url_list[idx].encode('utf8')).hexdigest() + ',' + str(cosx) + '\n')

    volume = sum(1 for y in cosine_similarities[0] if y > 0) * -1
    if volume == 0:
        return []

    return numpy.argsort(cosine_similarities)[0][volume:]
"""
    print(url_list[8:10])
    print(cosine_similarities[0][26:])
    print(numpy.argsort(cosine_similarities)[0])
    print(numpy.take(url_list, numpy.argsort(cosine_similarities)[0][-5:]))
    """
#    print(cosine_similarities.flatten())
#    print(filtered_words)


current_user = 4
url_list = get_list_of_url(current_user)

folder_loc = 'ics_5111_dataset\\user_' + str(current_user) + '\\'



# vacation maldives, human computation, music events
p_query = "human computation"
p_n_grams = 1
p_stop_words = 1
p_stemming = 1
p_paragraph = 0
print(url_list )
with (open('task_1_user_' + str(current_user) + '.csv', 'a+')) as output_file:
    output_file.write(p_query + '\n')

data = query_string(ngrams=p_n_grams, stemming=p_stemming, stop_words=p_stop_words, paragraphs=p_paragraph)
list_of_documents_ordered = get_matches(p_query, data, stop_words=p_stop_words, stemming=p_stemming)

url_list_returned = numpy.take(url_list, list_of_documents_ordered[-5:][::-1]);
to_return = []

with (open('task_1_user_' + str(current_user) + '.csv', 'a+')) as output_file:
    output_file.write('\n')
print(url_list_returned)
"""
for i in url_list_returned:
    if ( i )
    to_return.append((i, hashlib.md5(i.encode('utf8')).hexdigest()))
print(list_of_documents_ordered)
"""

"""
data = query_string(p_n_grams, stemming=p_stemming, stop_words=p_stop_words, paragraphs=p_paragraph)
with open(folder_loc + 'TaskClassExt.csv', 'r') as query_files:
    for i in query_files.readlines():
        list_of_documents_ordered = get_matches(i.split(',')[1].strip(), data)
        url_list_returned = numpy.take(url_list, list_of_documents_ordered[-5:][::-1]);
        to_return = []
        for i in url_list_returned:
            to_return.append((i, hashlib.md5(i.encode('utf8')).hexdigest()))
        print(to_return)
"""
exit()

# print(list_of_documents_ordered)


# print(numpy.take(paragraph_list, list_of_documents_ordered[-5:]))


exit()





exit()


#data = query_string(1, 1, 1)
#get_matches("maldives accomodation info information", data)

#get_matches("Human computation", data)
#get_matches("Maldives Vacation", data)

#filtered_words = ' '.join(word_list)


#print(url_list[numpy.argsort(cosine_similarities)[0]])



